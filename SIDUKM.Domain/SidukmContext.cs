﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SIDUKM.Domain
{
    public class SidukmContext : baza_zpiEntities
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
