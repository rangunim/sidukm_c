//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SIDUKM.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class Message
    {
        public int MessageID { get; set; }
        public int Sender_AccountID { get; set; }
        public int Recipient_AccountID { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public System.DateTime DateCreated { get; set; }
        public bool IsReaded { get; set; }
        public bool IsAnonymousRecipient { get; set; }
        public bool IsDeletedBySender { get; set; }
        public bool IsDeletedByRecipient { get; set; }
        public Nullable<System.DateTime> DateDeletedBySender { get; set; }
        public Nullable<System.DateTime> DateDeletedByRecipient { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual Account Account1 { get; set; }
    }
}
