﻿using System.Threading.Tasks;

namespace SIDUKM.Logic
{
    public interface IMailService
    {
        bool Send(string recipient, string subject, string body);
        Task SendAsync(string recipient, string subject, string body);
    }
}
