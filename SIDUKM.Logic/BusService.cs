﻿using System;
using SIDUKM.Domain;
using SIDUKM.Logic.Repository;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.IO.Compression;
using System.Xml;
using System.Net;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace SIDUKM.Logic
{
    public class BusService: IBusService
    {
        private readonly IRepository<Stop> StopRepository;
        private readonly IRepository<Line> LineRepository;
        private readonly IRepository<Timetable> TimetableRepository;
        private readonly IRepository<City> CityRepository;
        private readonly IRepository<Route> RouteRepository;

        public BusService(IRepository<Stop> StopRepository, IRepository<Line> LineRepository, IRepository<Timetable> TimetableRepository, IRepository<City> CityRepository, IRepository<Route> RouteRepository)
        {
            this.StopRepository = StopRepository;
            this.LineRepository = LineRepository;
            this.TimetableRepository = TimetableRepository;
            this.CityRepository = CityRepository;
            this.RouteRepository = RouteRepository;
        }

        public bool AddOrUpdateStop(Stop stop)
        {
            if(stop == null) return false;
            Stop s = StopRepository.GetAll().FirstOrDefault(x => x.CityId.Equals(stop.CityId) && x.StopNumber.Equals(stop.StopNumber));
            if(s == null)
            {
                StopRepository.Add(stop);
            }
            else
            {
               // stop.StopId = s.StopId;
                s.CoordinateX = stop.CoordinateX;
                s.CoordinateY = stop.CoordinateY;
                s.StopName = stop.StopName;
                
                StopRepository.Update(s);
            }
            return true;
        }
        public bool AddStopAll(Stop [] stops) //uzywac tylko wtedy gdy jestes pewien, że będa same Add.
        {
            if(stops == null) return false;
            var stopsDB = StopRepository.GetAll();
            if(stopsDB.Count(x => x.CityId == 1) > 0)
            {
                foreach(Stop stop in stops)
                {
                    if(stopsDB.FirstOrDefault(x => x.CityId.Equals(stop.CityId) && x.StopNumber.Equals(stop.StopNumber)) != null)
                    {
                        return false;
                    }
                }
            }        
            StopRepository.AddAll(stops);
            return true;
        }

        public bool AddOrUpdateLine(Line line)
        {
            if(line == null) return false;
            Line l = LineRepository.GetAll().FirstOrDefault(x => x.CityId == line.CityId && x.LineName.Equals(line.LineName, StringComparison.OrdinalIgnoreCase) && x.Direction.Equals(line.Direction, StringComparison.OrdinalIgnoreCase));
            if(l == null)
            {
                LineRepository.Add(line);
            }
            //else
            //{
               // line.LineId = l.LineId;             
                //LineRepository.Update(line);
            //}
            return true;
        }
        public bool AddLineAll(Line [] lines) //uzywac tylko wtedy gdy jestes pewien, że będa same Add.
        {
            if(lines == null) return false;
            var linesDB = LineRepository.GetAll();

            if(linesDB.Count() > 0)
            {
                foreach(Line line in lines)
                {
                    if(linesDB.FirstOrDefault(x => x.CityId == line.CityId && x.LineName.Equals(line.LineName, StringComparison.OrdinalIgnoreCase) && x.Direction.Equals(line.Direction, StringComparison.OrdinalIgnoreCase)) != null)
                    {
                        return false;
                    }
                }
            }         
            LineRepository.AddAll(lines);
            return true;      
        }
        public bool AddTimetable(int stopId, int lineId, Dictionary<string, List<string>> timetable)
        {
            Timetable result = TimetableRepository.GetAll().FirstOrDefault(x => x.StopId == stopId && x.LineId == lineId);
            bool newEntry = false;

            if (result == null)
            {
                result = new Timetable()
                {
                    StopId = stopId,
                    LineId = lineId,
                };
                newEntry = true;
            }

            result.Hash = null;
            XDocument xml = new XDocument();
            XElement root = new XElement("Timetable");
            xml.Add(root);
            if (timetable != null)
            {
                foreach (var entry in timetable)
                {
                    XElement column = new XElement("Column");
                    XAttribute name = new XAttribute("Name", entry.Key);
                    column.Add(name);

                    foreach (var time in entry.Value)
                    {
                        if (Regex.Match(time, @"[0-2]?[0-9]:[0-5][0-9][A-Za-z\*]?").Success || time == "")
                        {
                            XElement depart = new XElement("Departure", time);
                            column.Add(depart);
                        }
                        else
                            return false;
                    }

                    root.Add(column);
                }
            }
            result.Table = xml.ToString();

            try 
            {
                if (newEntry)
                    TimetableRepository.Add(result);
                else
                    TimetableRepository.Update(result);
                return true;
            }
            catch
            {
                return false;
            }
            
        }

        public bool AddOrUpdateTimetable(Timetable timetable)
        {
            if(timetable == null) return false;
            Timetable t = TimetableRepository.GetAll().FirstOrDefault(x => x.LineId.Equals(timetable.LineId) && x.StopId.Equals(timetable.StopId));
            if(t == null)
            {
                TimetableRepository.Add(timetable);
            }
            else
            {
                t.Table = timetable.Table;
                t.Hash = timetable.Hash;
                TimetableRepository.Update(t);
            }
            return true; 
        }

        
        public bool AddTimetableAll(Timetable [] timetables) //uzywac tylko gdy jestesmy pewni ze beda same ADD
        {
            if(timetables == null) return false;
            var timetablesDB = TimetableRepository.GetAll();

            if(timetablesDB.Count() > 0)
            {
                foreach(Timetable timetable in timetables)
                {
                    if(timetablesDB.FirstOrDefault(x => x.LineId.Equals(timetable.LineId) && x.StopId.Equals(timetable.StopId)) != null)
                    {
                        return false;
                    }
                }
            }
            TimetableRepository.AddAll(timetables);
            return true;
        }
        public bool AddRouteAll(Route [] routes) //uzywac tylko gdy jestesmy pewni ze beda same ADD
        {
            if(routes == null) return false;
            var routesDB = RouteRepository.GetAll();

            if(routesDB.Count() > 0)
            {
                foreach(Route route in routes)
                {
                    if(routesDB.FirstOrDefault(x => x.LineId.Equals(route.LineId) && x.StopId.Equals(route.StopId)) != null)
                    {
                        return false;
                    }
                }
            }
            RouteRepository.AddAll(routes);
            return true;
        }
        public bool AddOrUpdateRoute(Route route)
        {
            if(route == null) return false;
            Route r = RouteRepository.GetAll().FirstOrDefault(x => x.LineId.Equals(route.LineId) && x.StopId.Equals(route.StopId));
            if(r == null)
            {
                RouteRepository.Add(route);
            }
            else
            {
                r.Order = route.Order;
                RouteRepository.Update(r);
            }
            return true;
        }


        public List<string> GetListTimetable(int lineId, int stopId, int timetableId)
        {
            Timetable t = TimetableRepository.GetAll().FirstOrDefault(x => x.LineId.Equals(lineId) && x.StopId.Equals(stopId));
            List<string> result = new List<string>();
            for(int i=0; i<24; i++){
                result.Add("");
            }
            if (t == null)
            {
                return null;
            }
            string xml = t.Table;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            foreach(XmlNode xmlNode in doc.DocumentElement.ChildNodes[timetableId].ChildNodes)
            {
                string [] time = xmlNode.InnerText.ToString().Split(':');
                int id = Int32.Parse(time[0]);
                result[id]+=time[1]+" ";
            }
            return result;
        }

        public List<Stop> GetStops(int cityId)
        {
            List<Stop> result = new List<Stop>();

            foreach(Stop stop in StopRepository.GetAll())
            {
                if(stop.CityId == cityId && !stop.StopName.Contains("" + stop.StopNumber))
                {
                    stop.StopName = String.Format("{0} - {1}", stop.StopName, stop.StopNumber);
                }
                result.Add(stop);
            }
            return result;
        }
        public List<Stop> GetOryginalStops(int cityId)
        {
            return StopRepository.GetAll().ToList();
        }

        public List<Stop> GetStopsDistinct(int cityId)
        {
            return StopRepository.GetBy(x => x.CityId == cityId).Where(x => x.CoordinateX!="0").GroupBy(x => x.StopName).
                Select(x => x.FirstOrDefault()).ToList();
        }

        public List<Line> GetLines(int cityId)
        {
            List<Line> result = new List<Line>();

            foreach(Line line in LineRepository.GetAll())
            {
                if(line.CityId == cityId && !line.LineName.Contains(line.Direction))
                {
                    line.LineName = String.Format("{0} kierunek: {1}", line.LineName, line.Direction);
                }
                result.Add(line);
            }

            return result;
        }

        public string GetLineNameWithStopName(int lineId, int stopId)
        {
            var line = LineRepository.FindBy(x => x.LineId == lineId);
            var stop = StopRepository.FindBy(x => x.StopId == stopId);
            return String.Format("{0}: {1}, kierunek: {2}", line.LineName, stop.StopName, line.Direction);
        }

        public string GetLineName(int lineId)
        {
            return LineRepository.FindBy(x => x.LineId == lineId).LineName;
        }

        public DateTime GetLastUpdateDateInAllLines()
        {
            var timetableDB = TimetableRepository.GetAll();
            return timetableDB.Count() > 0 ? timetableDB.Max(x => x.LastUpdated.Value) : DateTime.Today.AddYears(-10);
        }
        public DateTime? GetLastUpdate(string lineName, int cityID)
        {
            Line line = LineRepository.FindBy(x => x.CityId == cityID && x.LineName.Equals(lineName, StringComparison.OrdinalIgnoreCase));
            if(line == null) return null;
            Timetable timetable = TimetableRepository.FindBy(x => x.LineId.Equals(line.LineId));
            if(timetable == null) return null;
            return timetable.LastUpdated;    
        }

        public string GetCoordinates(int stopId)
        {
            var stop = StopRepository.FindBy(x => x.StopId == stopId);
            var coordX = stop.CoordinateX.ToString(CultureInfo.InvariantCulture);
            var coordY = stop.CoordinateY.ToString(CultureInfo.InvariantCulture);
            return String.Format("{0}:{1}", coordX, coordY);
        }

        public ZipArchive GetZipArchiveFromWeb(String zipPath, ZipArchiveMode mode = ZipArchiveMode.Read) 
        {
            if(zipPath == null) return null;
            if(zipPath.Substring(0, 6).Contains("http"))
            {
                WebClient webClient = new WebClient();
                Stream data = webClient.OpenRead(zipPath);
                return  new ZipArchive(data, mode);
            }
            else return ZipFile.Open(zipPath, mode);
        }
       
        public XmlReader GetXmlFromZip(ZipArchive zip, string fileName)
        {   
            foreach(ZipArchiveEntry entry in zip.Entries)
            {
                if(entry.Name.Equals(fileName, StringComparison.OrdinalIgnoreCase))
                {
                    StreamReader sr = new StreamReader(entry.Open(), Encoding.GetEncoding("ISO-8859-2"));
                    XmlReader x = XmlReader.Create(sr);
                /*    XmlDocument contentxml = new XmlDocument();
                    try {  contentxml.Load(x);}
                    catch(XmlException exp)
                    {  Console.WriteLine(exp.Message); }*/
                       
                    /*StringWriter sw = new StringWriter();
                    XmlTextWriter xw = new XmlTextWriter(sw);
                    contentxml.WriteTo(xw);
                    Console.WriteLine("<BR>" + sw.ToString());*/
                    return x;
                }
            }
            return null;
        }

        public void UpdateDataFromXml(XmlReader xml, ref List<Line> resultLine, ref  List<Stop> resultStop, ref Tuple<List<Line>, List<Stop>, List<Timetable>> tuple, ref List<Route> resultRoute)
        {
            Line lineTemp = new Line();
            Stop stopTemp = new Stop();
            Timetable timetableTemp = new Timetable();

            XDocument doc = XDocument.Load(xml);
            string toGetHash = doc.ToString().Substring(doc.ToString().IndexOf("<wariant"));
            toGetHash = AccountService.GetMD5(toGetHash);
            XElement linia = doc.Descendants("linia").ElementAt(0);
            bool oldHash = true;

            int orderStop = 0; 
            foreach(XElement element1 in doc.Descendants("wariant"))
            {
                orderStop = 1;
                //lineTemp = new Line(linia.Attribute("nazwa").Value, element1.Attribute("nazwa").Value);
                // lineTemp = LineRepository.FindBy(x => x.LineName.Equals(linia.Attribute("nazwa").Value) && x.Direction.Equals(element1.Attribute("nazwa").Value)  && x.CityId == 1);
                lineTemp.LineName = linia.Attribute("nazwa").Value;
                lineTemp.Direction = element1.Attribute("nazwa").Value;
                lineTemp.CityId = 1; //TODO gdy beda inne miasta
                Line lineTemp2 = new Line
                {
                    //LineId = lineTemp.LineId,
                    LineName = lineTemp.LineName,
                    Direction = lineTemp.Direction,
                    CityId = lineTemp.CityId
                };
                resultLine.Add(lineTemp2);
                //AddOrUpdateLine(lineTemp2);//LineList.Add(lineTemp); // ========================ADD TO DATABASE
                foreach(XElement element2 in element1.Descendants("przystanek"))
                {
                    //stopTemp = new Stop(element2.Attribute("nazwa").Value, Convert.ToInt32(element2.Attribute("id").Value));
                    // stopTemp = StopRepository.FindBy(x => x.StopNumber==Convert.ToInt32(element2.Attribute("id").Value) && x.CityId==1);
                    stopTemp.StopName = element2.Attribute("nazwa").Value;
                    stopTemp.StopNumber = Convert.ToInt32(element2.Attribute("id").Value);
                    stopTemp.CoordinateX = "0";
                    stopTemp.CoordinateY = "0";
                    stopTemp.CityId = 1; //TODO gdy beda inne miasta

                    Stop stopTemp2 = new Stop
                    {
                        // StopId = stopTemp.StopId,
                        StopNumber = stopTemp.StopNumber,
                        StopName = stopTemp.StopName,
                        CoordinateX = stopTemp.CoordinateX,
                        CoordinateY = stopTemp.CoordinateY,
                        CityId = stopTemp.CityId
                    };
                    //AddOrUpdateStop(stopTemp2);// StopList.Add(stopTemp);  // ========================ADD TO DATABASE
                    resultStop.Add(stopTemp2);

                    //========CHECK HASH (FOR THIS XML FILE)==================================
                    if(oldHash)
                    {
                        Line l = LineRepository.GetAll().FirstOrDefault(x => x.CityId == lineTemp2.CityId && x.LineName.Contains(lineTemp2.LineName) && x.Direction.Contains(lineTemp2.Direction));
                        Stop s = StopRepository.GetAll().FirstOrDefault(x => x.StopNumber.Equals(stopTemp2.StopNumber) && x.StopName.Contains(stopTemp2.StopName) && x.CityId.Equals(stopTemp2.CityId));
                        if(l != null && s != null)
                        {
                            Timetable t = TimetableRepository.GetAll().FirstOrDefault(x => x.LineId.Equals(l.LineId) && x.StopId.Equals(s.StopId));
                            if(t != null && t.Hash != null)
                            {
                                if(t.Hash.Equals(toGetHash))
                                {	
                                    resultLine.RemoveAt(resultLine.Count - 1);
                                    resultStop.RemoveAt(resultStop.Count - 1);
                                    return; // we have actuall timetable for this line in database, so end this method.
                                }
                                else oldHash=false;
                            }
                            else oldHash = false;
                        }
                        else oldHash = false;
                    }
                    //=============END CHECK HASH (FOR THIS XML FILE)=========================

                    Route route = new Route
                    {
                        Order = orderStop++
                    };
                    resultRoute.Add(route);

                    //============ START GET TIMETABLE ======================================
                    //XmlDocument timeXML = new XmlDocument(new XElement("Timetable"));
                    XElement main = new XElement("Timetable");
                    foreach(XElement element3 in element2.Descendants("dzien"))
                    {
                        XElement col = new XElement("Column", new XAttribute("Name", element3.Attribute("nazwa").Value));
                        //col.SetAttributeValue("Name",);
                        main.Add(col);

                        foreach(XElement element4 in element3.Descendants("godz"))
                        {
                            XElement dep;
                            foreach(XElement element5 in element4.Descendants("min"))
                            {
                                dep = new XElement("Departure");
                                dep.SetValue(element4.Attribute("h").Value + ":" + element5.Attribute("m").Value);
                                col.Add(dep);
                            }
                        }
                        //timeXML.CreateElement(main);
                    }
                    //TimetableList.Add(new Timetable(stopTemp.ID, lineTemp.ID, main.ToString())); // ========================ADD TO DATABASE
                    //stopTemp = StopRepository.FindBy(x => x.StopNumber == stopTemp.StopNumber && x.CityId == stopTemp.CityId);
                    //lineTemp = LineRepository.FindBy(x => x.LineName.Equals(lineTemp.LineName,StringComparison.OrdinalIgnoreCase)  && x.Direction.Equals(lineTemp.Direction,StringComparison.OrdinalIgnoreCase) && x.CityId == lineTemp.CityId);
                    //timetableTemp = TimetableRepository.FindBy(x => x.StopId == stopTemp.StopId && x.LineId== lineTemp.LineId);

                    timetableTemp.StopId = stopTemp.StopId;
                    timetableTemp.LineId = lineTemp.LineId;
                    timetableTemp.Table = main.ToString();
                    Timetable timetableTemp2 = new Timetable
                    {
                        StopId = timetableTemp.StopId,
                        LineId = timetableTemp.LineId,
                        Table = timetableTemp.Table,
                        Hash = toGetHash
                    };
                    //AddOrUpdateTimetable(timetableTemp2);
                    tuple.Item1.Add(lineTemp2);
                    tuple.Item2.Add(stopTemp2);
                    tuple.Item3.Add(timetableTemp2);
                }
            }
        }

       

        public List<string> GetLineNames(int cityId)
        {
            int resultLenght = 0;
            List<string> result = new List<string>();
            List<Line> linesDB = LineRepository.GetAll().ToList();

            foreach(Line line in linesDB)
            {
                if (line.CityId == cityId)
                {
                    int counter = 0;
                    bool isAdded = false;
                    while (!isAdded && counter != resultLenght)
                    {
                        if (line.LineName.Equals(result[counter]))
                        {
                            isAdded = true;
                        }
                        else
                        {
                            counter++;
                        }
                    }
                    if (!isAdded) { result.Add(line.LineName); resultLenght++; }
                }
            }
            return result;
        }


        public List<Stop> GetStopsByLine(int lineId)
        {
            List<Stop> result = new List<Stop>();
            List<Stop> stops = StopRepository.GetAll().ToList();
            
            foreach(Timetable t in TimetableRepository.GetBy(x => x.LineId == lineId))
            {
                result.Add(stops.FirstOrDefault(x => x.StopId == t.StopId));
            }

            return result;
        }

        public Timetable GetTimetable(int lineId, int StopId)
        {
            return TimetableRepository.FindBy(x => x.LineId == lineId && x.StopId == StopId);
        }

        /*private void CorrectTypo()
        {
            foreach (var tt in TimetableRepository.GetAll())
            {
                tt.Table = tt.Table.Replace("Deparature", "Departure");
                TimetableRepository.Update(tt);
            }
        }*/

        public Dictionary<string, List<string>> GetTimetableParsed(int lineId, int stopId)
        {
            //CorrectTypo();
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>(3);
            string xmlText = TimetableRepository.FindBy(x => x.LineId == lineId && x.StopId == stopId).Table;
            XDocument xml = XDocument.Parse(xmlText);

            foreach (var column in xml.Element("Timetable").Elements("Column"))
            {
                string name = column.Attribute("Name").Value;
                List<string> departList = new List<string>(1000);
                foreach (var depart in column.Elements("Departure"))
                {
                    departList.Add(depart.Value);
                }
                /*foreach (var depart in column.Elements("Deparature"))
                {
                    departList.Add(depart.Value);
                }*/
                //departList.Sort();
                result.Add(name, departList);
            }        
            return result;
        }

        public List<Stop> GetSortedStops(int lineId)
        {
            List<Stop> newStops = new List<Stop>();
            List<Stop> stops = GetStops(1);
            int count = 0;
            List<Timetable> timetables= TimetableRepository.GetBy(x => x.LineId == lineId).ToList();
            HashSet<int> stops2 = new HashSet<int> (timetables.Select(x=>x.StopId));
            /*foreach (Stop s in stops)
            {
                bool added = false;
                int countTimetable =0;
                while (!added && countTimetable < timetables.Count)
                {
                    if (timetables[countTimetable].StopId == s.StopId)
                    {
                        newStops.Insert(count, s);
                        count++;
                        added = true;
                    }
                    countTimetable++;
                }
                if (!added)
                {
                    newStops.Add(s);
                }    
            }*/
            newStops = stops.OrderBy(x => stops2.Contains(x.StopId) ? 0 : 1).ThenBy(x=>x.StopName).ToList();
            return newStops;
        }

        public List<Line> GetDirectionByName(string name)
        {
            return LineRepository.GetBy(x => x.LineName == name).ToList();
        }

        public List<Stop> GetStopsOrder(int lineId)
        {
            List<Stop> result = new List<Stop>();
            List<Route> route = RouteRepository.GetBy(x => x.LineId==lineId).OrderBy(x => x.Order).ToList();
            foreach(var r in route)
            {
                var temp = StopRepository.FindBy(x => x.StopId == r.StopId);
                result.Add(temp);
            }

            return result;
        }

    }
}
