﻿using System;
using System.Collections.Generic;
using SIDUKM.Domain;
using System.Linq;

namespace SIDUKM.Logic
{
    public interface IAccountService
    {
        ICollection<City> GetCities();
        ICollection<Account> GetAccounts();
        ICollection<Gender> GetGenders();
        Account GetAccountByUsername(string username, string password);
        Account GetAccountByName(string username);
        bool RegisterAccount(Account account);
        bool ChangePassword(string password, int id);
        int GetAccountIdByGuid(Guid guid);
        Guid? CreateResetLink(string email);
        UserRole GetUserRole(string login);
        ICollection<UserRole> GetUserRoles();
        IQueryable<AccountData> GetAccountsData(string view);
        bool EditAccount(Account newDataAccount, AccountAvatar newAvatar);
        bool EditPassword(string username, string password, string newPassword);
       // bool EditDataAccount(Account newDataAccount);
        bool DeleteAccount(int id);
        Account GetAccountById(int id);
        Account GetAccountById(int id, bool ignoreDeleted);
        AccountAvatar GetAccountAvatar(int id);
        bool DeleteAvatar(int id);
        bool ChangeUserRole(int accountId, int userRoleId);
        List<int> GetModeratorsId();
        int GetAccountId(string username);
        string GetAccountName(int accountId, bool showLogin);
        bool IsUserBanned(int accountId);
        DateTime GetDateOfBanEnd(int accountId);
        AccountBan GetBan(int accountId);
        bool SetBan(int accountId, DateTime start, DateTime end);
        bool Unban(int accountId);
    }
}
