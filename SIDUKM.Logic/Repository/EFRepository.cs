﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using SIDUKM.Domain;

namespace SIDUKM.Logic.Repository
{
    public class EFRepository<T> : IRepository<T> where T : class
    {
        private baza_zpiEntities context;

        public EFRepository()
        {
            context = new SidukmContext();
            context.Configuration.LazyLoadingEnabled = false;
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
            context.Database.CommandTimeout = 300;
        }

        public virtual IQueryable<T> GetAll(params Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var property in includeProperties)
            {
                context.Set<T>().Include(property).Load();
            }

            return context.Set<T>();
        }

        public virtual IQueryable<T> GetBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var property in includeProperties)
            {
                context.Set<T>().Include(property).Load();
            }

            return context.Set<T>().Where(predicate);
        }

        public T FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            foreach (var property in includeProperties)
            {
                context.Set<T>().Include(property).Load();
            }

            return context.Set<T>().FirstOrDefault(predicate);
        }

        public virtual void Add(T entity)
        {
            var type = entity.GetType();
            var property = type.GetProperty("DateCreated");
            if (property != null)
            {
                property.SetValue(entity, DateTime.UtcNow, null);
            }

            property = type.GetProperty("LastUpdated");
            if (property != null)
            {
                property.SetValue(entity, DateTime.UtcNow, null);
            }

            context.Set<T>().Add(entity);
            context.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            context.Set<T>().Remove(entity);
            context.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            var type = entity.GetType();
            var property = type.GetProperty("LastUpdated");
            if (property != null)
            {
                property.SetValue(entity, DateTime.UtcNow, null);
            }

            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public int Count()
        {
            return context.Set<T>().Count();
        }

        public IQueryable<T> GetQueryable()
        {
            return context.Set<T>() as IQueryable<T>;
        }

        public virtual void AddAll(T [] entities)
        {
            foreach(var entity in entities)
            {
                var type = entity.GetType();
                var property = type.GetProperty("DateCreated");
                if(property != null)
                {
                    property.SetValue(entity, DateTime.UtcNow, null);
                }

                property = type.GetProperty("LastUpdated");
                if(property != null)
                {
                    property.SetValue(entity, DateTime.UtcNow, null);
                }
            }

            var count = entities.GetLength(0);
            int c = 0;
            while(c < count)
            {
                var records = entities.Skip(c).Take(500).ToList();
                context.Set<T>().AddRange(records);
                context.SaveChanges();
                c += 500;
            }
            CreateNewContext();
        }

        private void CreateNewContext()
        {
            context = new SidukmContext();
            context.Configuration.LazyLoadingEnabled = false;
            context.Configuration.AutoDetectChangesEnabled = false;
            context.Configuration.ValidateOnSaveEnabled = false;
        }
    }

}
