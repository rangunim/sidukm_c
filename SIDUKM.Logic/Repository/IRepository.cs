﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace SIDUKM.Logic.Repository
{
    public interface IRepository<T> where T : class
    {
        int Count();

        IQueryable<T> GetAll(params Expression<Func<T, object>>[] includeProperties);

        IQueryable<T> GetBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        T FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        void Add(T entity);

        void Delete(T entity);

        void Update(T entity);

        IQueryable<T> GetQueryable();
        void AddAll(T [] entities);
     }
}
