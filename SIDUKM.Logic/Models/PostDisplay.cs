﻿namespace SIDUKM.Logic
{
    public class PostDisplay
    {
        public long PostId { get; set; }
        public string Message { get; set; }
        public string DateCreated { get; set; }
        public bool IsEdited { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int AccountGender { get; set; }
        public int Upvotes { get; set; }
        public int Downvotes { get; set; }
    }
}
