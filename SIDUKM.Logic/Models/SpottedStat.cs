﻿namespace SIDUKM.Logic.Models
{
    public class SpottedStat
    {
        public string Author { get; set; }
        public string Date { get; set; }
        public string Message { get; set; }
        public int Count { get; set; }
    }
}
