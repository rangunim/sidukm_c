﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Linq;

namespace SIDUKM.Logic
{
    public class Rss
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string PubDate { get; set; }
    }

}