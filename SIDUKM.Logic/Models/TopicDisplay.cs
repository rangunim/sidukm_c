﻿using System;

namespace SIDUKM.Logic
{
    public class TopicDisplay
    {
        public int TopicId { get; set; }
        public string Message { get; set; }
        public bool IsAnonymous { get; set; }
        public string DateCreated { get; set; }
        public bool IsEdited { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public int AccountGender { get; set; }
        public string TopicCategory { get; set; }
        public string Line { get; set; }
        public int Upvotes { get; set; }
        public int Downvotes { get; set; }
        public int Comments { get; set; }
    }
}
