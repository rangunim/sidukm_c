﻿using System;

namespace SIDUKM.Logic
{
    public class MessageData
    {
        public string Subject { get; set; }
        public string Account { get; set; }
        public int AccountId { get; set; }
        public string Date { get; set; }
        public int MessageId { get; set; }
        public string Content { get; set; }
        public bool IsReaded { get; set; }
        public int SenderId { get; set; }
        public int RecipientId { get; set; }
        public bool IsDeleted { get; set; }
    }
}