﻿using System;
namespace SIDUKM.Logic
{
    public class AccountData
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CityName { get; set; }
        public string GenderName { get; set; }
        public string UserRoleName { get; set; }
        public string DateCreated { get; set; }
        public string DateDeleted { get; set; }
        public string Additional { get; set; }
       
    }
}
