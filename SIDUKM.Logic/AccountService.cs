﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using SIDUKM.Domain;
using SIDUKM.Logic.Repository;

namespace SIDUKM.Logic
{
    public class AccountService : IAccountService
    {
        // odwolania do tabel bazy danych
        private readonly IRepository<City> CityRepository;
        private readonly IRepository<Gender> GenderRepository;
        private readonly IRepository<UserRole> UserRoleRepository;
        private readonly IRepository<Account> AccountRepository;
        private readonly IRepository<PasswordReset> PasswordResetRepository;
        private readonly IRepository<AccountAvatar> AccountAvatarRepository;
        private readonly IRepository<AccountBan> AccountBanRepository; 

        public AccountService(IRepository<City> CityRepository, IRepository<Gender> GenderRepository,
            IRepository<UserRole> UserRoleRepository, IRepository<Account> AccountRepository,
            IRepository<PasswordReset> PasswordResetRepository, IRepository<AccountAvatar> AccountAvatarRepository, 
            IRepository<AccountBan> AccountBanRepository)
        {
            this.CityRepository = CityRepository;
            this.GenderRepository = GenderRepository;
            this.UserRoleRepository = UserRoleRepository;
            this.AccountRepository = AccountRepository;
            this.PasswordResetRepository = PasswordResetRepository;
            this.AccountAvatarRepository = AccountAvatarRepository;
            this.AccountBanRepository = AccountBanRepository;
        }

        public ICollection<City> GetCities()
        {
            return CityRepository.GetAll().ToList();
        }
        public ICollection<Gender> GetGenders()
        {
            return GenderRepository.GetAll().ToList();
        }

        public ICollection<Account> GetAccounts()
        {
            return AccountRepository.GetAll().ToList();
        }

        internal static string GetMD5(string text)
        {
            byte[] textByte = Encoding.UTF8.GetBytes(text);
            byte[] hashed = null;
            using (MD5 hasher = MD5.Create())
            {
                hashed = hasher.ComputeHash(textByte);
            }
            StringBuilder result = new StringBuilder(hashed.Length * 2);
            foreach (byte t in hashed)
            {
                result.Append(t.ToString("X2"));
            }
            return result.ToString();
        }

        public Account GetAccountByUsername(string username, string password)
        {
            string md5pass = GetMD5(password);
            Account result = AccountRepository.GetAll().FirstOrDefault(x => x.AccountLogin.Equals(username) && 
                x.AccountPassword.ToUpper().Equals(md5pass));
            if(result == null || result.IsDeleted)
                return null;
            return result;
        }
        public Account GetAccountByName(string username)
        {
            Account result = AccountRepository.GetAll().FirstOrDefault(x => x.AccountLogin.Equals(username));
            if(result == null || result.IsDeleted)
                return null;
            return result;
        }
        public Account GetAccountById(int id)
        {
            /*Account result = AccountRepository.GetAll().FirstOrDefault(x => x.AccountId == id);
            return result == null || result.IsDeleted  ? null : result;*/
            return GetAccountById(id, true);
        }

        public bool RegisterAccount(Account account)
        {
            var accountList = GetAccounts().ToList();
            
            // sprawdzenie, czy jest co najmniej jedna cyfra i co najmniej jedna litera
            Regex rgx = new Regex(@"(([0-9]).*([a-zA-z]).*)|(([a-zA-z]).*([0-9]).*)");
            if(!rgx.IsMatch(account.AccountPassword))
                return false;

            if(!ValidateAccount(account))
                return false;
            
            // czy login lub email istnieja w bazie
            if (accountList.FirstOrDefault(x => x.AccountLogin == account.AccountLogin || x.Email == account.Email) != null)
                return false;         
            if(account.AccountPassword.Length < 6)
                return false;     

            account.AccountPassword = GetMD5(account.AccountPassword);
            if (account.GenderId == 0)
                account.GenderId = null;
            AccountRepository.Add(account);
            return true;
        }

        public bool ChangePassword(string password, int id)
        {
            Account user = AccountRepository.GetAll().FirstOrDefault(x => x.AccountId == id);
            
            if(!ValidateAccount(user))
                return false;
            
            // sprawdzenie dlugosci hasla
            if(user.AccountPassword.Length < 6)
                return false;
            // sprawdzenie, czy jest co najmniej jedna cyfra i co najmniej jedna litera
            Regex rgx = new Regex(@"(([0-9]).*([a-zA-z]).*)|(([a-zA-z]).*([0-9]).*)");
            if(!rgx.IsMatch(user.AccountPassword))
                return false;

            password = GetMD5(password);
            user.AccountPassword = password;
            AccountRepository.Update(user);
            PasswordResetRepository.Delete(PasswordResetRepository.GetAll().FirstOrDefault(x => x.AccountId == id));

            return true;
        }

        private bool ValidateAccount(Account account)
        {
            if(account == null)
                return false;

            // sprawdzenie, czy imie i nazwisko zawieraja jedynie litery, spacje i myslnik
            if(account.FirstName != null)
                if(account.FirstName.Any(x => !Char.IsLetter(x) && x != ' ' && x != '-'))
                    return false;
            if(account.LastName != null)
                if(account.LastName.Any(x => !Char.IsLetter(x) && x != ' ' && x != '-'))
                    return false;
            // sprawdzenie poprawnosci maila
            if(!(new EmailAddressAttribute().IsValid(account.Email)))
                return false;
            return true;
        }

        public bool EditAccount (Account newDataAccount, AccountAvatar newAvatar)
        {
            var accountList = GetAccounts().ToList();

            if(!ValidateAccount(newDataAccount))
                return false;

            // czy login istnieje w bazie
            if(accountList.FirstOrDefault(x => x.AccountLogin == newDataAccount.AccountLogin) == null)
                return false;

            Account editedAccount = AccountRepository.FindBy(x => x.AccountId == newDataAccount.AccountId);

            if(!editedAccount.Email.Equals(newDataAccount.Email, StringComparison.OrdinalIgnoreCase))
            {
                if(accountList.FirstOrDefault(x => x.Email == newDataAccount.Email) != null)
                    return false;
            }

            if(newAvatar != null)
            {
                AccountAvatar editedAvatar = AccountAvatarRepository.FindBy(x => x.AccountId == newAvatar.AccountId);
                if(editedAvatar == null)
                {
                    AccountAvatarRepository.Add(newAvatar);
                }
                else
                {
                    editedAvatar.Avatar = newAvatar.Avatar;
                    editedAvatar.MimeType = newAvatar.MimeType;
                    AccountAvatarRepository.Update(editedAvatar);
                }
            }
            
            if (newDataAccount.AccountPassword != null)
            {
                string password = GetMD5(newDataAccount.AccountPassword);
                editedAccount.AccountPassword = password;
               // editedAccount.AccountPassword = newDataAccount.AccountPassword;
            }

            editedAccount.Email = newDataAccount.Email;
            editedAccount.CityId = newDataAccount.CityId;
            editedAccount.GenderId = newDataAccount.GenderId; //== null ? null : newDataAccount.GenderId;
            editedAccount.FirstName = newDataAccount.FirstName;
            editedAccount.LastName = newDataAccount.LastName;
            editedAccount.UserRoleId = newDataAccount.UserRoleId;
            AccountRepository.Update(editedAccount);
            return true;
        }

        public bool EditPassword(string username, string password, string newPassword)
        {
            string md5pass = GetMD5(password);
            Account result = AccountRepository.GetAll().FirstOrDefault(x => x.AccountLogin.Equals(username) &&
                x.AccountPassword.ToUpper().Equals(md5pass));
            if (result!=null)
            {
                string md5new = GetMD5(newPassword);
                //Account account = GetAccountByUsername(username, password);
                result.AccountPassword = md5new;
                return true;
            }
            return false;
        }

        /*public bool EditDataAccount(Account newDataAccount)
        {
            var accountList = GetAccounts().ToList();

            // czy login lub email istnieja w bazie
            if (accountList.FirstOrDefault(x => x.Email == newDataAccount.Email) == null)
                return false;

            Account editedAccount = AccountRepository.FindBy(x => x.AccountId == newDataAccount.AccountId);

            editedAccount.Email = newDataAccount.Email;
            editedAccount.CityId = newDataAccount.CityId;
            editedAccount.GenderId = newDataAccount.GenderId;
            editedAccount.FirstName = newDataAccount.FirstName;
            editedAccount.LastName = newDataAccount.LastName;
            AccountRepository.Update(editedAccount);
            return true;
        }
        */
        public bool DeleteAccount(int id)
        {
            var user = AccountRepository.FindBy(x => x.AccountId == id);
            if(user !=null)
            {
                user.IsDeleted = true;
                AccountRepository.Update(user);
                return true;
            }
            return false;
        }


        public int GetAccountIdByGuid(Guid guid)
        {
            var acc = PasswordResetRepository.GetAll().FirstOrDefault(x => x.Link.Equals(guid));
            return acc != null ? acc.AccountId : 0;
        }

        public Guid? CreateResetLink(string email)
        {
            Account user = AccountRepository.GetAll().FirstOrDefault(x => x.Email.Equals(email));

            if (user == null)
                return null;

            if(user.IsDeleted)
                return null;

            PasswordReset reset = PasswordResetRepository.GetAll().FirstOrDefault(x => x.AccountId == user.AccountId);
            if (reset == null)
            {
                reset = new PasswordReset
                {
                    AccountId = user.AccountId,
                    Link = Guid.NewGuid()
                };
                PasswordResetRepository.Add(reset);
            }

            return reset.Link;
        }

        public UserRole GetUserRole(string login)
        {
            var user = AccountRepository.FindBy(x => x.AccountLogin == login);
            if (user == null)
            {
                return null;
            }
            var role = UserRoleRepository.FindBy(x => x.UserRoleId == user.UserRoleId);
            return role;
        }

        public IQueryable<AccountData> GetAccountsData(string view)
        {
            //var accounts = GetAccounts();
            IEnumerable<Account> accountsFilter;         
            switch(view)
            {
                case "DefaultView": accountsFilter = AccountRepository.GetBy(x => !x.IsDeleted); break;
                case "ArchiveView": accountsFilter = AccountRepository.GetBy(x => x.IsDeleted); break;
                case "GuidView": accountsFilter = PasswordResetRepository.GetAll().Select(x => x.Account); break;
                case "BannedView": accountsFilter = AccountBanRepository.GetAll().Select(x => x.Account); break;
                default: accountsFilter = AccountRepository.GetBy(x => !x.IsDeleted); break;
            }
               
            var result = new List<AccountData>();

            foreach(var account in accountsFilter)
            {
                var city = CityRepository.FindBy(x => x.CityId == account.CityId).CityName;
                var gender = "";
                if (account.GenderId.HasValue)
                    gender = GenderRepository.FindBy(x => x.GenderId == account.GenderId).GenderName;
                //Thread.Sleep(1000);
                var userRole = UserRoleRepository.FindBy(x => x.UserRoleId == account.UserRoleId).RoleName;

                AccountData temp = new AccountData()
                {
                    Additional = IsUserBanned(account.AccountId) ? "zbanowany" : "",
                    Email = account.Email,
                    FirstName = account.FirstName,
                    Id = account.AccountId,
                    LastName = account.LastName,
                    Login = account.AccountLogin,
                    CityName = city,
                    GenderName = gender,
                    UserRoleName = userRole,
                    DateCreated = (account.DateCreated).Date.ToLongDateString(),
                };
                if(account.IsDeleted)
                {
                    temp.Additional = "usunięty";
                }
                result.Add(temp); 
            }

            return result.AsQueryable();
        }


        public ICollection<UserRole> GetUserRoles()
        {
            return UserRoleRepository.GetAll().ToList();
        }

        public AccountAvatar GetAccountAvatar(int id)
        {
            AccountAvatar accAvatar = AccountAvatarRepository.FindBy(m => m.AccountId == id);
            return accAvatar != null ? accAvatar : null;
        }

        public bool DeleteAvatar(int id)
        {
            AccountAvatar accAvatar = AccountAvatarRepository.FindBy(m => m.AccountId == id);
            if(accAvatar != null)
            {
                AccountAvatarRepository.Delete(accAvatar);
                return true;
            }           
            return false;
        }

        public bool ChangeUserRole(int accountId, int userRoleId)
        {
            var account = AccountRepository.FindBy(x => x.AccountId == accountId);
            if (account == null)
            {
                return false;
            }
            account.UserRoleId = (short) userRoleId;
            try
            {
                AccountRepository.Update(account);
                return account.UserRoleId == userRoleId;
            }
            catch
            {
                return false;
            }
        }

        public Account GetAccountById(int id, bool ignoreDeleted)
        {
            Account result = AccountRepository.GetAll().FirstOrDefault(x => x.AccountId == id);
            return result == null || (result.IsDeleted && ignoreDeleted) ? null : result;
        }

        public List<int> GetModeratorsId()
        {
            return AccountRepository.GetBy(x => x.UserRoleId < 3).Select(x => x.AccountId).ToList();
        }

        public int GetAccountId(string username)
        {
            var account = AccountRepository.FindBy(x => x.AccountLogin == username);
            return account != null ? account.AccountId : 0;
        }

        public string GetAccountName(int accountId, bool showLogin)
        {
            var account = AccountRepository.FindBy(x => x.AccountId == accountId);
            string display = account.AccountLogin;
            if (account.FirstName == null)
            {
                return display;
            }
            display = account.FirstName;
            if (account.LastName != null)
            {
                display += " " + account.LastName;
            }
            if (showLogin)
            {
                display += " (" + account.AccountLogin + ")";
            }
            return display;
        }

        public bool IsUserBanned(int accountId)
        {
            var ban = AccountBanRepository.FindBy(x => x.AccountId == accountId);
            if (ban == null)
            {
                return false;
            }
            if (ban.EndDate > DateTime.Now && ban.StartDate < DateTime.Now)
            {
                return true;
            }
            AccountBanRepository.Delete(ban);
            return false;
        }

        public DateTime GetDateOfBanEnd(int accountId)
        {
            var ban = AccountBanRepository.FindBy(x => x.AccountId == accountId);
            return ban == null ? DateTime.MinValue : ban.EndDate;
        }

        public AccountBan GetBan(int accountId)
        {
            return AccountBanRepository.FindBy(x => x.AccountId == accountId);
        }

        public bool SetBan(int accountId, DateTime start, DateTime end)
        {
            if (start > end)
            {
                return false;
            }
            var ban = AccountBanRepository.FindBy(x => x.AccountId == accountId);
            try
            {
                if (ban == null)
                {
                    var newBan = new AccountBan()
                    {
                        AccountId = accountId,
                        StartDate = start,
                        EndDate = end
                    };
                    AccountBanRepository.Add(newBan);
                    return true;
                }
                else
                {
                    ban.StartDate = start;
                    ban.EndDate = end;
                    AccountBanRepository.Update(ban);
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Unban(int accountId)
        {
            var ban = AccountBanRepository.FindBy(x => x.AccountId == accountId);
            if (ban == null)
            {
                return false;
            }
            AccountBanRepository.Delete(ban);
            return true;
        }
    }
}
