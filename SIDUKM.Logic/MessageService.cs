﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIDUKM.Domain;
using SIDUKM.Logic.Repository;
using System.Data.Entity.SqlServer;

namespace SIDUKM.Logic
{
    public class MessageService : IMessageService
    {
        private readonly IRepository<Message> MessageRepository;
        private readonly IRepository<Account> AccountRepository;

        public MessageService(IRepository<Message> messageRepository, IRepository<Account> accountRepository)
        {
            MessageRepository = messageRepository;
            AccountRepository = accountRepository;
        }

        public bool SendMessage(Message message)
        {
            message.DateCreated = DateTime.Now;
            message.IsDeletedByRecipient = false;
            message.IsDeletedBySender = false;
            message.IsReaded = false;
            try
            {
                MessageRepository.Add(message);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private string GetAccountName(int accountId, bool showLogin)
        {
            var account = AccountRepository.FindBy(x => x.AccountId == accountId);
            string display = account.AccountLogin;
            if (account.FirstName == null)
            {
                return display;
            }
            display = account.FirstName;
            if (account.LastName != null)
            {
                display += " " + account.LastName;
            }
            if (showLogin)
            {
                display += " (" + account.AccountLogin + ")";
            }
            return display;
        }

        public IQueryable<MessageData> GetInbox(int accountId)
        {
            var got = MessageRepository.GetBy(x => x.Recipient_AccountID == accountId && !x.IsDeletedByRecipient).ToList();
            return got.Select(x => new MessageData()
            {
                MessageId = x.MessageID,
                AccountId = x.Sender_AccountID,
                Subject = x.Subject,
                Date = String.Format("{0:yyyy-MM-dd HH:mm}", x.DateCreated),
                Account = //x.IsAnonymousRecipient ? "<< anonimowy >>" : 
                            GetAccountName(x.Sender_AccountID, false),
                IsReaded = x.IsReaded,
                IsDeleted = false
            }).AsQueryable();
        }

        public IQueryable<MessageData> GetSent(int accountId)
        {
            var got = MessageRepository.GetBy(x => x.Sender_AccountID == accountId && !x.IsDeletedBySender).ToList();
            return got.Select(x => new MessageData()
            {
                MessageId = x.MessageID,
                AccountId = x.IsAnonymousRecipient ? -1 : x.Recipient_AccountID,
                Subject = x.Subject,
                Date = String.Format("{0:yyyy-MM-dd HH:mm}", x.DateCreated),
                Account = x.IsAnonymousRecipient ? "<< anonimowy >>" : GetAccountName(x.Recipient_AccountID, false),
                IsReaded = true,
                IsDeleted = false
            }).AsQueryable();
        }

        public IQueryable<MessageData> GetDeleted(int accountId)
        {
            var got =
                MessageRepository.GetBy(
                    x =>
                        (x.Sender_AccountID == accountId && x.IsDeletedBySender && 
                        x.DateDeletedBySender > SqlFunctions.DateAdd("dd", -14, DateTime.Now)) ||
                        (x.Recipient_AccountID == accountId && x.IsDeletedByRecipient &&
                        x.DateDeletedByRecipient > SqlFunctions.DateAdd("dd", -14, DateTime.Now))
                        ).ToList();
            return got.Select(x => new MessageData()
            {
                MessageId = x.MessageID,
                AccountId = x.Sender_AccountID == accountId ? (x.IsAnonymousRecipient ? -1 : x.Recipient_AccountID) : x.Sender_AccountID,
                Subject = x.Subject,
                Date = String.Format("{0:yyyy-MM-dd HH:mm}", x.DateCreated),
                Account = x.IsAnonymousRecipient ? "<< anonimowy >>" : GetAccountName(x.Recipient_AccountID, false),
                IsDeleted = true
            }).AsQueryable();
        }

        public bool RemoveMessage(int messageId, int accountId)
        {
            Message msg = MessageRepository.FindBy(x => x.MessageID == messageId);
            if (msg == null)
            {
                return false;
            }
            try
            {
                if (msg.Recipient_AccountID == accountId)
                {
                    msg.IsDeletedByRecipient = true;
                    msg.DateDeletedByRecipient = DateTime.UtcNow;
                    MessageRepository.Update(msg);
                }
                else if (msg.Sender_AccountID == accountId)
                {
                    msg.IsDeletedBySender = true;
                    msg.DateDeletedBySender = DateTime.UtcNow;
                    MessageRepository.Update(msg);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool RemoveMessage(int messageId, bool isSender)
        {
            Message msg = MessageRepository.FindBy(x => x.MessageID == messageId);
            if (msg == null)
            {
                return false;
            }
            try
            {
                if (isSender)
                {
                    msg.IsDeletedBySender = true;
                    msg.DateDeletedBySender = DateTime.UtcNow;
                }
                else
                {
                    msg.IsDeletedByRecipient = true;
                    msg.DateDeletedByRecipient = DateTime.UtcNow;
                }
                MessageRepository.Update(msg);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool UnremoveMessage(int messageId, int accountId)
        {
            Message msg = MessageRepository.FindBy(x => x.MessageID == messageId);
            if (msg == null)
            {
                return false;
            }
            try
            {
                if (msg.Recipient_AccountID == accountId)
                {
                    msg.IsDeletedByRecipient = false;
                    MessageRepository.Update(msg);
                }
                else if (msg.Sender_AccountID == accountId)
                {
                    msg.IsDeletedBySender = false;
                    MessageRepository.Update(msg);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public List<Tuple<int, string>> GetAccountsList()
        {
            return
                AccountRepository.GetBy(x => !x.IsDeleted).AsEnumerable()
                    .Select(x => new Tuple<int, string>(x.AccountId, GetAccountName(x.AccountId, true)))
                    .ToList();
        }

        public bool CheckAsRead(int messageId)
        {
            Message msg = MessageRepository.FindBy(x => x.MessageID == messageId);
            if (msg == null)
            {
                return false;
            }
            try
            {
                msg.IsReaded = true;
                MessageRepository.Update(msg);
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public MessageData GetMessage(int messageId, int user)
        {
            Message msg = MessageRepository.FindBy(x => x.MessageID == messageId);
            return msg == null
                ? null
                : new MessageData()
                {
                    MessageId = msg.MessageID,
                    AccountId = msg.IsAnonymousRecipient ? -1 
                        : msg.Sender_AccountID==user
                            ? msg.Recipient_AccountID : msg.Sender_AccountID,
                    Subject = msg.Subject,
                    Date = String.Format("{0:yyyy-MM-dd HH:mm}", msg.DateCreated),
                    Account =
                        msg.IsAnonymousRecipient && msg.Sender_AccountID == user
                            ? "<< anonimowy >>"
                            : msg.Sender_AccountID == user
                                ? GetAccountName(msg.Recipient_AccountID, false)
                                : GetAccountName(msg.Sender_AccountID, false),
                    Content = msg.Content,
                    IsReaded = msg.IsReaded,
                    RecipientId = msg.Recipient_AccountID,
                    SenderId = msg.Sender_AccountID,
                    IsDeleted = msg.IsDeletedByRecipient,
                    
                };
        }

        public Message GetRawMessage(int messageId)
        {
            return MessageRepository.FindBy(x => x.MessageID == messageId);
        }
        
        public int GetUnreadCount(int accountId)
        {
            int count = 0;
            var messages = MessageRepository.GetBy(x => x.Recipient_AccountID == accountId && !x.IsReaded && !x.IsDeletedByRecipient);
            if (messages != null)
            {
                count = messages.Count();
            }
            return count;
        }


        public bool RemoveFromTrash(int messageId, int accountId)
        {
            var message = MessageRepository.FindBy(x => x.MessageID == messageId);

            if (message.Sender_AccountID == accountId && message.IsDeletedBySender)
            {
                message.DateDeletedBySender = DateTime.UtcNow.AddYears(-1);

                if (message.Recipient_AccountID == accountId && message.IsDeletedByRecipient)
                    message.DateDeletedByRecipient = DateTime.UtcNow.AddYears(-1);

                MessageRepository.Update(message);

                return true;
            }
            else if (message.Recipient_AccountID == accountId && message.IsDeletedByRecipient)
            {
                message.DateDeletedByRecipient = DateTime.UtcNow.AddYears(-1);
                MessageRepository.Update(message);

                return true;
            }

            return false;
        }
    }
}