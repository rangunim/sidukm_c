﻿using System.Collections.Generic;
using SIDUKM.Domain;
using System.IO.Compression;
using System.Xml;
using System;

namespace SIDUKM.Logic
{
    public interface IBusService
    {
        bool AddOrUpdateStop(Stop stop);
        bool AddOrUpdateLine(Line line);
        bool AddTimetable(int stopId, int lineId, Dictionary<string, List<string>> timetable);
        bool AddOrUpdateTimetable(Timetable timetable);
        bool AddOrUpdateRoute(Route route);
        bool AddRouteAll(Route [] routes);
        List<Stop> GetStops(int cityId);
        List<Stop> GetStopsDistinct(int cityId);
        List<Line> GetLines(int cityId);
        string GetLineNameWithStopName(int lineId, int stopId);
        string GetLineName(int lineId);
        string GetCoordinates(int stopId);
        ZipArchive GetZipArchiveFromWeb(string zipPath, ZipArchiveMode mode);
        XmlReader GetXmlFromZip(ZipArchive zip, string fileName);
        void UpdateDataFromXml(XmlReader xml, ref List<Line> resultLine, ref  List<Stop> resultStop, ref Tuple<List<Line>, List<Stop>, List<Timetable>> tuple, ref List<Route> resultRoute);
        bool AddLineAll(Line [] lines);
        bool AddStopAll(Stop [] stops);
        bool AddTimetableAll(Timetable [] timetables);
        List<string> GetLineNames(int cityId);
        List<Stop> GetStopsByLine(int lineId);
        Timetable GetTimetable(int lineId, int StopId);
        Dictionary<string, List<string>> GetTimetableParsed(int lineId, int stopId);
        List<string> GetListTimetable(int lineId, int stopId, int timetableId);
        List<Stop> GetOryginalStops(int cityId);
        DateTime GetLastUpdateDateInAllLines();
        DateTime? GetLastUpdate(string lineName, int cityID);
        List<Stop> GetSortedStops(int lineId);
        List<Line> GetDirectionByName(string name);
        List<Stop> GetStopsOrder(int lineId);
    }
}
