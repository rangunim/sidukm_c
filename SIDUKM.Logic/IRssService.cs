﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SIDUKM.Logic
{
    public interface IRssService
    {
        IEnumerable<Rss> GetNews(String link);
        string GetRssLink(String login);
    }
}
