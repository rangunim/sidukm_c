﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SIDUKM.Logic
{
    public class MailService : IMailService
    {
        private readonly SmtpClient smtp;

        public MailService()
        {
            smtp = new SmtpClient("mailtrap.io", 2525)
            {
                EnableSsl = true,
                Credentials = new NetworkCredential("3246220827a0f20a3", "d188a8262728a1")
            };
        }

        public bool Send(string recipient, string subject, string body)
        {

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("sidukm@sidukm.sidukm");
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = subject;
            mailMessage.To.Add(recipient);           
            smtp.Send(mailMessage);
            return true;
            
           // throw new NotImplementedException();
        }

        public async Task SendAsync(string recipient, string subject, string body)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Body = body;
            mailMessage.Subject = subject;
            mailMessage.To.Add(recipient);
            mailMessage.IsBodyHtml = true;
            await smtp.SendMailAsync(mailMessage);
        }
    }
}
