﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIDUKM.Domain;
using SIDUKM.Logic.Models;
using SIDUKM.Logic.Repository;

namespace SIDUKM.Logic
{
    public class StatService : IStatService
    {
        private IRepository<Account> AccountRepository;
        private IRepository<Topic> TopicRepository;
        private IRepository<Post> PostRepository;
        private IRepository<TopicVote> TopicVoteRepository;
        private IRepository<PostVote> PostVoteRepository;

        public StatService(IRepository<Account> accountRepository, IRepository<Topic> topicRepository,
            IRepository<Post> postRepository, IRepository<TopicVote> topicVoteRepository,
            IRepository<PostVote> postVoteRepository)
        {
            AccountRepository = accountRepository;
            TopicRepository = topicRepository;
            PostRepository = postRepository;
            TopicVoteRepository = topicVoteRepository;
            PostVoteRepository = postVoteRepository;
        }

        private string AddZero(int number)
        {
            return number < 10 ? String.Format("0{0}", number) : number.ToString();
        }

        public Dictionary<string, int> RegistrationsPerDay()
        {
            return AccountRepository.GetBy(x => !x.IsDeleted).GroupBy(x => new
            {
                Year = x.DateCreated.Year,
                Month = x.DateCreated.Month,
                Day = x.DateCreated.Day
            }).Select(g => new
            {
                Year = g.Key.Year,
                Month = g.Key.Month,
                Day = g.Key.Day,
                Accounts = g.Count()
            }).AsEnumerable().ToDictionary(x => x.Year + "-" + AddZero(x.Month) + "-" + AddZero(x.Day), x => x.Accounts);
        }

        public Dictionary<string, int> SpottedTopicsPerDay()
        {
            return TopicRepository.GetBy(x => !x.IsDeleted).OrderBy(x => x.DateCreated).GroupBy(x => new
            {
                Year = x.DateCreated.Year,
                Month = x.DateCreated.Month,
                Day = x.DateCreated.Day
            }).Select(g => new
            {
                Year = g.Key.Year,
                Month = g.Key.Month,
                Day = g.Key.Day,
                Topics = g.Count()
            }).AsEnumerable().ToDictionary(x => x.Year + "-" + AddZero(x.Month) + "-" + AddZero(x.Day), x => x.Topics);
        }

        public Dictionary<string, int> SpottedPostsPerDay()
        {
            return PostRepository.GetBy(x => !x.IsDeleted).OrderBy(x => x.DateCreated).GroupBy(x => new
            {
                Year = x.DateCreated.Year,
                Month = x.DateCreated.Month,
                Day = x.DateCreated.Day
            }).Select(g => new
            {
                Year = g.Key.Year,
                Month = g.Key.Month,
                Day = g.Key.Day,
                Posts = g.Count()
            }).AsEnumerable().ToDictionary(x => x.Year + "-" + AddZero(x.Month) + "-" + AddZero(x.Day), x => x.Posts);
        }

        public Dictionary<string, int> StatAccumulated(Func<Dictionary<string, int>> statFunc)
        {
            int sum = 0;
            var source = statFunc();
            var result = new Dictionary<string, int>(source.Count);
            foreach (var i in source.OrderBy(x => x.Key))
            {
                result.Add(i.Key, i.Value + sum);
                sum += i.Value;
            }
            return result;
        }

        public SpottedStat MostCommentedTopic()
        {
            var result =
                (PostRepository.GetBy(x => !x.IsDeleted)
                    .GroupBy(p => new {Topic = p.TopicId})
                    .OrderByDescending(g => g.Count())
                    .Select(g => new {Topic = g.Key.Topic, Count = g.Count()})).FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            var topic = TopicRepository.FindBy(x => x.TopicId == result.Topic);
            return new SpottedStat()
            {
                Author = AccountRepository.FindBy(x => x.AccountId == topic.AccountId).AccountLogin,
                Count = result.Count,
                Date = String.Format("{0:F}", topic.DateCreated),
                Message = topic.Message
            };
        }

        public SpottedStat HighestVotesTopic()
        {
            var result =
                (TopicVoteRepository.GetAll()
                    .Where(tv => tv.VoteValue > 0)
                    .GroupBy(tv => new {Topic = tv.TopicId})
                    .OrderByDescending(g => g.Sum(x => x.VoteValue))
                    .Select(g => new {Topic = g.Key.Topic, Votes = g.Sum(x => x.VoteValue)})).FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            var topic = TopicRepository.FindBy(x => x.TopicId == result.Topic);
            return new SpottedStat()
            {
                Author = AccountRepository.FindBy(x => x.AccountId == topic.AccountId).AccountLogin,
                Count = result.Votes,
                Date = String.Format("{0:F}", topic.DateCreated),
                Message = topic.Message
            };
        }

        public SpottedStat LowestVotesTopic()
        {
            var result =
                (TopicVoteRepository.GetAll()
                    .Where(tv => tv.VoteValue < 0)
                    .GroupBy(tv => new { Topic = tv.TopicId })
                    .OrderByDescending(g => g.Sum(x => x.VoteValue))
                    .Select(g => new { Topic = g.Key.Topic, Votes = g.Sum(x => x.VoteValue) })).FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            var topic = TopicRepository.FindBy(x => x.TopicId == result.Topic);
            return new SpottedStat()
            {
                Author = AccountRepository.FindBy(x => x.AccountId == topic.AccountId).AccountLogin,
                Count = result.Votes,
                Date = String.Format("{0:F}", topic.DateCreated),
                Message = topic.Message
            };
        }

        public SpottedStat HighestVotesPost()
        {
            var result =
                (PostVoteRepository.GetAll()
                    .Where(pv => pv.VoteValue > 0)
                    .GroupBy(pv => new { Post = pv.PostId })
                    .OrderByDescending(g => g.Sum(x => x.VoteValue))
                    .Select(g => new { Post = g.Key.Post, Votes = g.Sum(x => x.VoteValue) })).FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            var post = PostRepository.FindBy(x => x.PostId == result.Post);
            return new SpottedStat()
            {
                Author = AccountRepository.FindBy(x => x.AccountId == post.AccountId).AccountLogin,
                Count = result.Votes,
                Date = String.Format("{0:F}", post.DateCreated),
                Message = post.Message
            };
        }

        public SpottedStat LowestVotesPost()
        {
            var result =
                (PostVoteRepository.GetAll()
                    .Where(pv => pv.VoteValue < 0)
                    .GroupBy(pv => new { Post = pv.PostId })
                    .OrderByDescending(g => g.Sum(x => x.VoteValue))
                    .Select(g => new { Post = g.Key.Post, Votes = g.Sum(x => x.VoteValue) })).FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            var post = PostRepository.FindBy(x => x.PostId == result.Post);
            return new SpottedStat()
            {
                Author = AccountRepository.FindBy(x => x.AccountId == post.AccountId).AccountLogin,
                Count = result.Votes,
                Date = String.Format("{0:F}", post.DateCreated),
                Message = post.Message
            };
        }

        public Dictionary<string, int> SpottedActivityByTopics(int topCount)
        {
            var result = (from t in TopicRepository.GetBy(x => !x.IsDeleted)
                group t by new
                {
                    Account = t.AccountId
                }
                into g
                orderby g.Count() descending
                select new
                {
                    Account = g.Key.Account,
                    Topics = g.Count()
                }).Take(topCount).AsEnumerable();
            return result.ToDictionary(x => AccountRepository.FindBy(y => y.AccountId == x.Account).AccountLogin,
                x => x.Topics);
        }

        public Dictionary<string, int> SpottedActivityByPosts(int topCount)
        {
            var result = (from t in PostRepository.GetBy(x => !x.IsDeleted)
                group t by new
                {
                    Account = t.AccountId
                }
                into g
                orderby g.Count() descending
                select new
                {
                    Account = g.Key.Account,
                    Posts = g.Count()
                }).Take(topCount).AsEnumerable();
            return result.ToDictionary(x => AccountRepository.FindBy(y => y.AccountId == x.Account).AccountLogin,
                x => x.Posts);
        }

    }
}