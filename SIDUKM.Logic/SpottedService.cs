﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIDUKM.Domain;
using SIDUKM.Logic.Repository;

namespace SIDUKM.Logic
{
    public class SpottedService : ISpottedService
    {
        private readonly IRepository<Topic> TopicRepository;
        private readonly IRepository<Account> AccountRepository;
        private readonly IRepository<City> CityRepository;
        private readonly IRepository<Line> LineRepository;
        private readonly IRepository<TopicCategory> TopicCategoryRepository;
        private readonly IRepository<TopicVote> TopicVoteRepository;
        private readonly IRepository<Post> PostRepository;
        private readonly IRepository<PostVote> PostVoteRepository;
        private readonly IRepository<Profanity> ProfanityRepository;

        public SpottedService(IRepository<Topic> topicRepository, IRepository<Account> accountRepository,
            IRepository<City> cityRepository, IRepository<Line> lineRepository, IRepository<TopicCategory> topicCategoryRepository,
            IRepository<TopicVote> topicVoteRepository, IRepository<Post> postRepository, IRepository<PostVote> postVoteRepository, IRepository<Profanity> profanityRepository)
        {
            TopicRepository = topicRepository;
            AccountRepository = accountRepository;
            CityRepository = cityRepository;
            LineRepository = lineRepository;
            TopicCategoryRepository = topicCategoryRepository;
            TopicVoteRepository = topicVoteRepository;
            PostRepository = postRepository;
            PostVoteRepository = postVoteRepository;
            ProfanityRepository = profanityRepository;
        }

        public int AddTopic(Topic topic)
        {
            try
            {
                //if (topic.TopicCategoryId
                topic.Message = CensorMessage(topic.Message);
                TopicRepository.Add(topic);
                return topic.TopicId;
            }
            catch (Exception e)
            {
                // TODO logowanie wyjątku
                return 0;
            }
        }

        public List<TopicCategory> GetTopicCategories()
        {
            return TopicCategoryRepository.GetAll().ToList();
        }

        private string CensorMessage(string message)
        {
            System.Text.StringBuilder temp = new System.Text.StringBuilder();
            foreach(string word in ProfanityRepository.GetAll().Select(x => x.Word))
            {
               temp.Clear();
               temp.Append(word [0]);
               for(int i=1; i < word.Length -1; i++)
               {
                   temp.Append('*');
               }
               temp.Append(word [word.Length - 1]);
               message = System.Text.RegularExpressions.Regex.Replace(message, word, temp.ToString(), System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            }
            return message;     
        }

        public List<Topic> GetTopics(int count, int lastId, int categoryId, int cityId)
        {
            // categoryId == 0 => wszystkie kategorie
            // lastId == 0 => od początku posty
            if (categoryId == 0)
            {
                return lastId == 0 ? TopicRepository.GetBy(x => !x.IsDeleted && x.CityId == cityId)
                    .OrderByDescending(x => x.TopicId).Take(count).ToList() :
                    TopicRepository.GetBy(x => x.TopicId < lastId && !x.IsDeleted && x.CityId == cityId)
                    .OrderByDescending(x => x.TopicId).Take(count).ToList();
            }
            return lastId == 0 ? TopicRepository.GetBy(x => x.TopicCategoryId == categoryId && !x.IsDeleted && x.CityId == cityId)
                .OrderByDescending(x => x.TopicId).Take(count).ToList() :
                TopicRepository.GetBy(x => x.TopicId < lastId && x.TopicCategoryId == categoryId && !x.IsDeleted && x.CityId == cityId)
                .OrderByDescending(x => x.TopicId).Take(count).ToList();
        }

        public static string TimeToText(DateTime dateTime)
        {
            var difference = DateTime.UtcNow - dateTime;
            if (difference.TotalHours < 1)
            {
                return String.Format("{0} min. temu", difference.Minutes);
            }
            if (difference.TotalHours < 24)
            {
                return String.Format("{0} godz. temu", difference.Hours);
            }
            if (difference.TotalDays < 2)
            {
                return String.Format("Wczoraj o {0}:{1}", difference.Hours, difference.Minutes);
            }
            if (difference.TotalDays < 3)
            {
                return String.Format("Przedwczoraj o {0}:{1}", difference.Hours, difference.Minutes);
            }
            return String.Format("{0:d MMMM yyyy, HH:mm}", dateTime);
        }

        public int CountVotes(long messageId, bool isTopic, bool countUpvotes)
        {
            int result = 0;
            if (isTopic)
            {
                if (TopicVoteRepository.Count() == 0)
                {
                    return 0;
                }
                foreach (var vote in TopicVoteRepository.GetBy(x => x.TopicId == messageId))
                {
                    if (countUpvotes && vote.VoteValue > 0)
                        result += vote.VoteValue;
                    else if (!countUpvotes && vote.VoteValue < 0)
                        result += vote.VoteValue;
                }
            }
            else
            {
                if (PostVoteRepository.Count() == 0)
                {
                    return 0;
                }
                foreach (var vote in PostVoteRepository.GetBy(x => x.PostId == messageId))
                {
                    if (countUpvotes && vote.VoteValue > 0)
                        result += vote.VoteValue;
                    else if (!countUpvotes && vote.VoteValue < 0)
                        result += vote.VoteValue;
                }
            }
            return result;
        }

        private string GetAccountName(int accountId)
        {
            var account = AccountRepository.FindBy(x => x.AccountId == accountId);
            string display = account.AccountLogin;
            if (account.FirstName != null)
            {
                display = account.FirstName;
                if (account.LastName != null)
                {
                    display += " " + account.LastName;
                }
            }
            return display;
        }

        public List<TopicDisplay> GetTopicsToDisplay(int count, int lastId, int categoryId, int cityId)
        {
            var result = new List<TopicDisplay>();
            var topics = GetTopics(count, lastId, categoryId, cityId);
            if (!topics.Any())
            {
                return result;
            }
            result.AddRange(topics.Select(topic => new TopicDisplay()
            {
                AccountId = topic.AccountId,
                //DateCreated = String.Format("{0:d MMMM yyyy, HH:mm}", topic.DateCreated),
                DateCreated = TimeToText(topic.DateCreated),
                IsAnonymous = topic.IsAnonymous,
                Message = topic.Message,
                TopicId = topic.TopicId,
                IsEdited = topic.LastUpdated != null && !(topic.LastUpdated == topic.DateCreated),
                TopicCategory = TopicCategoryRepository.FindBy(x => x.TopicCategoryId == topic.TopicCategoryId).Name,
                Line = topic.LineId == null
                    ? ""
                    : LineRepository.GetBy(x => x.LineId == topic.LineId)
                        .Select(x => x.LineName + " (kierunek:" + x.Direction + ")").FirstOrDefault(),
                Comments = PostRepository.GetBy(x => x.TopicId == topic.TopicId && !x.IsDeleted).Count(),
                Upvotes = CountVotes(topic.TopicId, true, true),
                Downvotes = CountVotes(topic.TopicId, true, false),
                AccountName = topic.IsAnonymous ? "<< anonimowe >>" : GetAccountName(topic.AccountId),
                AccountGender = AccountRepository.FindBy(x => x.AccountId == topic.AccountId).GenderId ?? 0
            }));
            /*
            foreach (var topic in topics)
            {
                TopicDisplay toAdd = new TopicDisplay();
                toAdd.AccountId = topic.AccountId;
                toAdd.DateCreated = String.Format("{0:d MMMM yyyy, HH:mm}", topic.DateCreated);
                toAdd.IsAnonymous = topic.IsAnonymous;
                toAdd.Message = topic.Message;
                toAdd.TopicId = topic.TopicId;
                toAdd.IsEdited = topic.LastUpdated != null && !(topic.LastUpdated == topic.DateCreated);
                toAdd.TopicCategory = TopicCategoryRepository.FindBy(x => x.TopicCategoryId == topic.TopicCategoryId).Name;
                toAdd.Line = topic.LineId == null
                    ? ""
                    : LineRepository.GetBy(x => x.LineId == topic.LineId)
                        .Select(x => x.LineName + " (kierunek:" + x.Direction + ")").FirstOrDefault();
                toAdd.Comments = PostRepository.GetBy(x => x.TopicId == topic.TopicId).Count();
                toAdd.Upvotes = CountVotes(topic.TopicId, true, true);
                toAdd.Downvotes = CountVotes(topic.TopicId, true, false);
                result.Add(toAdd);
            }*/
            return result;
        }

        public bool AreNewTopics(int firstId, int categoryId, int cityId)
        {
            return categoryId == 0 ? TopicRepository.GetBy(x => x.TopicId > firstId && !x.IsDeleted && x.CityId == cityId).Any() :
                TopicRepository.GetBy(x => x.TopicId > firstId && x.TopicCategoryId == categoryId && !x.IsDeleted && x.CityId == cityId)
                .Any();
        }

        public int GetNewTopicsCount(int firstId, int categoryId, int cityId)
        {
            return categoryId == 0
                ? TopicRepository.GetBy(x => x.TopicId > firstId && !x.IsDeleted && x.CityId == cityId).Count()
                : TopicRepository.GetBy(
                    x => x.TopicId > firstId && x.TopicCategoryId == categoryId && !x.IsDeleted && x.CityId == cityId)
                    .Count();
        }

        public List<Post> GetPosts(int topicId)
        {
            // topic nie istnieje
            if (!TopicRepository.GetBy(x => x.TopicId == topicId && !x.IsDeleted).Any())
            {
                return null;
            }

            var posts = PostRepository.GetBy(x => x.TopicId == topicId && !x.IsDeleted);
            if (posts == null || !posts.Any())
            {
                return new List<Post>();
            }

            //return posts.OrderBy(x => x.DateCreated).ToList();
            return posts.ToList();
        }

        public List<PostDisplay> GetPostsToDisplay(int topicId)
        {
            var result = new List<PostDisplay>();
            var posts = GetPosts(topicId);
            if (!posts.Any())
            {
                return result;
            }
            result.AddRange(posts.Select(post => new PostDisplay()
            {
                AccountId = post.AccountId,
                DateCreated = TimeToText(post.DateCreated),
                PostId = post.PostId,
                Message = post.Message,
                IsEdited = post.LastUpdated != null && !(post.LastUpdated == post.DateCreated),
                Upvotes = CountVotes(post.PostId, false, true),
                Downvotes = CountVotes(post.PostId, false, false),
                AccountName = GetAccountName(post.AccountId),
                AccountGender = AccountRepository.FindBy(x => x.AccountId == post.AccountId).GenderId ?? 0
            }));
            return result;
        }

        public long AddPost(Post post)
        {
            try
            {
                post.Message = CensorMessage(post.Message);
                PostRepository.Add(post);

                return post.PostId;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public int AddTopicVote(int accountId, int topicId, bool isPlus)
        {
            TopicVote vote = TopicVoteRepository.FindBy(x => x.AccountId == accountId && x.TopicId == topicId);

            if (vote == null)
            {
                if (isPlus)
                    TopicVoteRepository.Add(new TopicVote()
                    {
                        VoteValue = 1,
                        AccountId = accountId,
                        TopicId = topicId
                    });
                else
                    TopicVoteRepository.Add(new TopicVote()
                    {
                        VoteValue = -1,
                        AccountId = accountId,
                        TopicId = topicId
                    });
            }
            else
            {
                if (isPlus)
                    vote.VoteValue = 1;
                else
                    vote.VoteValue = -1;

                TopicVoteRepository.Update(vote);
            }

            return CountVotes(topicId, true, isPlus);
        }

        public int AddPostVote(int accountId, long postId, bool isPlus)
        {
            PostVote vote = PostVoteRepository.FindBy(x => x.AccountId == accountId && x.PostId == postId);

            if (vote == null)
            {
                if (isPlus)
                    PostVoteRepository.Add(new PostVote()
                    {
                        VoteValue = 1,
                        PostId = postId,
                        AccountId = accountId
                    });
                else
                    PostVoteRepository.Add(new PostVote()
                    {
                        VoteValue = -1,
                        PostId = postId,
                        AccountId = accountId
                    });
            }
            else
            {
                if (isPlus)
                    vote.VoteValue = 1;
                else
                    vote.VoteValue = -1;

                PostVoteRepository.Update(vote);
            }

            return CountVotes(postId, false, isPlus);
        }

        public Topic GetRawTopic(int topicId)
        {
            return TopicRepository.FindBy(x => x.TopicId == topicId);
        }

        public void DeleteTopic(int topicId)
        {
            Topic top = TopicRepository.FindBy(x => x.TopicId == topicId);
            if (top != null)
            {
                top.IsDeleted = true;
                TopicRepository.Update(top);
            }
        }

        public void DeletePost(int postId)
        {
            Post post = PostRepository.FindBy(x=>x.PostId == postId);
            if (post != null)
            {
                post.IsDeleted = true;
                PostRepository.Update(post);
            }
        }
        public Topic GetTopicById(int topicId)
        {
            return TopicRepository.FindBy(x => x.TopicId == topicId);
        }

        public void EditTopic(Topic topic) 
        {
            if(topic == null) return;
            try
            {
                Topic t = GetTopicById(topic.TopicId);
                t.Message = CensorMessage(topic.Message);
                TopicRepository.Update(t);
            }
            catch(Exception)
            {
                
            }
        }

        public Post GetPostById(int postId)
        {
            return PostRepository.FindBy(x => x.PostId == postId);
        }

        public void EditPost(Post post)
        {
            if (post == null) return;
            try
            {
                Post p = GetPostById((int)post.PostId);
                p.Message = CensorMessage(post.Message);
                PostRepository.Update(p);

            }
            catch (Exception) { }
        }
    }
}
