﻿using System.Collections.Generic;
using SIDUKM.Domain;

namespace SIDUKM.Logic
{
    public interface ISpottedService
    {
        int AddTopic(Topic topic);
        List<TopicCategory> GetTopicCategories();
        List<Topic> GetTopics(int count, int lastId, int categoryId, int cityId);
        int CountVotes(long messageId, bool isTopic, bool countUpvotes);
        List<TopicDisplay> GetTopicsToDisplay(int count, int lastId, int categoryId, int cityId);
        bool AreNewTopics(int firstId, int categoryId, int cityId);
        int GetNewTopicsCount(int firstId, int categoryId, int cityId);
        List<Post> GetPosts(int topicId);
        List<PostDisplay> GetPostsToDisplay(int topicId);
        long AddPost(Post post);
        int AddTopicVote(int accountId, int topicId, bool isPlus);
        int AddPostVote(int accountId, long postId, bool isPlus);
        Topic GetRawTopic(int topicId);
        void DeleteTopic(int topicId);
        Topic GetTopicById(int topicId);
        void EditTopic(Topic topic);
        void DeletePost(int postId);
        Post GetPostById(int postId);
        void EditPost(Post post);
    }
}
