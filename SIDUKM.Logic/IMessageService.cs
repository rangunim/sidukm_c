﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIDUKM.Domain;

namespace SIDUKM.Logic
{
    public interface IMessageService
    {
        bool SendMessage(Message message);
        IQueryable<MessageData> GetInbox(int accountId);
        IQueryable<MessageData> GetSent(int accountId);
        IQueryable<MessageData> GetDeleted(int accountId);
        bool RemoveMessage(int messageId, int accountId);
        bool RemoveMessage(int messageId, bool isSender);
        bool UnremoveMessage(int messageId, int accountId);
        bool RemoveFromTrash(int messageId, int accountId);
        List<Tuple<int, string>> GetAccountsList();
        bool CheckAsRead(int messageId);
        MessageData GetMessage(int messageId,int user);
        Message GetRawMessage(int messageId);
        int GetUnreadCount(int accountId);
    }
}