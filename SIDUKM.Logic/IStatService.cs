﻿using System;
using System.Collections.Generic;
using SIDUKM.Domain;
using SIDUKM.Logic.Models;

namespace SIDUKM.Logic
{
    public interface IStatService
    {
        Dictionary<string, int> RegistrationsPerDay();
        Dictionary<string, int> SpottedTopicsPerDay();
        Dictionary<string, int> SpottedPostsPerDay();
        Dictionary<string, int> StatAccumulated(Func<Dictionary<string, int>> statFunc);
        SpottedStat MostCommentedTopic();
        SpottedStat HighestVotesTopic();
        SpottedStat LowestVotesTopic();
        SpottedStat HighestVotesPost();
        SpottedStat LowestVotesPost();
        Dictionary<string, int> SpottedActivityByTopics(int topCount);
        Dictionary<string, int> SpottedActivityByPosts(int topCount);
    }
}