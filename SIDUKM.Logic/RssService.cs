﻿using SIDUKM.Domain;
using SIDUKM.Logic.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SIDUKM.Logic
{
    public class RssService : IRssService
    {
        IRepository<City> CityRepository;
        IRepository<Account> AccountRepository;

        public RssService(IRepository<City> cityRepository, IRepository<Account> accountRepository)
        {
            CityRepository = cityRepository;
            AccountRepository = accountRepository;
        }
        public  string GetRssLink(string login)
        {
            var account = AccountRepository.FindBy(x => x.AccountLogin == login);
            if(account!=null)
            {
                City city = CityRepository.FindBy(x => x.CityId == account.CityId);
                return city.RssLink;
            }
            return CityRepository.FindBy(x => x.CityId == 1).RssLink;
        }

        public IEnumerable<Rss> GetNews(String link)
        {
            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            try
            {
                var xmlData = client.DownloadString(link);  //remember to update this URL

                XDocument xml = XDocument.Parse(xmlData);

                var News = (from story in xml.Descendants("item")
                            select new Rss
                            {
                                Title = ((string)story.Element("title")),
                                Link = ((string)story.Element("link")),
                                Description = ((string)story.Element("description")),
                                PubDate = ((string)story.Element("pubDate"))
                            }).Take(5);
                return News;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
