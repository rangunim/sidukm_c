﻿using System.Collections.Generic;
using Moq;
using SIDUKM.Logic;

namespace SIDUKM.UnitTests
{
    class ServicesMocks
    {
        public Mock<IMailService> MockMailService { get; set; }
        public List<string[]> MockMailServer { get; set; }

        public ServicesMocks()
        {
            MockMailServer = new List<string []>();
            MockMailService = new Mock<IMailService>();
            MockMailService.Setup(m => m.Send(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).
                Returns<string,string,string>((recipient, subject, body) => {
                        var add = new[]{ recipient,subject,body };
                        MockMailServer.Add(add);
                        return MockMailServer[MockMailServer.Count - 1] == add;
                    });
        }
    }
}
