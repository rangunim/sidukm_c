﻿using System;
using System.Collections.Generic;
using SIDUKM.Domain;

namespace SIDUKM.UnitTests
{
    class DomainMocks
    {
        // listy z danymi
        public List<City> CityList { get; set; }
        public List<Gender> GenderList { get; set; }
        public List<UserRole> UserRoleList { get; set; }
        public List<Account> AccountList { get; set; }
        public List<AccountAvatar> AccountAvatarList { get; set; }
        public List<PasswordReset> PasswordResetList { get; set; }
        public List<TopicCategory> TopicCategoryList { get; set; }
        public List<Topic> TopicList { get; set; }
        public List<Line> LineList { get; set; }
        public List<TopicVote> TopicVoteList { get; set; }
        public List<Post> PostList { get; set; }
        public List<PostVote> PostVoteList { get; set; }
        public List<Profanity> ProfanityList { get; set; }
        public List<AccountBan> AccountBanList { get; set; }

        // singleton
        public static DomainMocks Instance
        {
            get { return _instance ?? (_instance = new DomainMocks()); }
        }
        private static DomainMocks _instance;

        private DomainMocks()
        {
            CityList = new List<City>
            {
                new City { CityId = 1, CityName = "Testowe", RssLink="http://www.gazetawroclawska.pl/rss/gazetawroclawska.xml" },
                new City { CityId = 2, CityName = "Wrocław", RssLink="http://pasazer.mpk.wroc.pl/rss-aktualnosci-i-zmiany" }
            };
            GenderList = new List<Gender> 
            {
                new Gender { GenderId=1, GenderName="Test" },
                new Gender { GenderId=2, GenderName="Mezczyzna" }
            };
            UserRoleList = new List<UserRole>
            {
                new UserRole { UserRoleId=1, RoleName="Admin" },
                new UserRole {UserRoleId=2, RoleName = "Uzytkownik"},
                new UserRole {UserRoleId=3, RoleName = "Moderator"}
            };
            AccountList = new List<Account>
            {
                new Account
                { 
                    AccountId=1, 
                    AccountLogin="login", 
                    AccountPassword="098f6bcd4621d373cade4e832627b4f6",
                    CityId=2,
                    City=CityList[1],
                    Email="e@e-mail.com",
                    FirstName="Imie",
                    GenderId=1,
                    Gender=GenderList[0],
                    LastName="Nazwisko",
                    UserRoleId=1,
                    UserRole=UserRoleList[0],
                    DateCreated = DateTime.MinValue,
                    IsDeleted = false
                },
                new Account
                {
                    AccountId=2, 
                    AccountLogin="user", 
                    AccountPassword="a0905cde5632e484dbcf5f943738c507",
                    CityId=2,
                    City=CityList[1],
                    Email="user@mail.com",
                    FirstName="User",
                    GenderId=1,
                    Gender=GenderList[0],
                    LastName="User LastName",
                    UserRoleId=3,
                    UserRole=UserRoleList[0],
                    DateCreated = DateTime.Now,
                    IsDeleted = false
                },
                new Account
                {
                    AccountId=3, 
                    AccountLogin="dziwak-ze-hej", 
                    AccountPassword="a0905cde5632e484dbcf5f943738c507",
                    CityId=1,
                    City=CityList[0],
                    Email="us@il.com",
                    FirstName="Us",
                    GenderId=null,
                    LastName="Las",
                    UserRoleId=2,
                    UserRole=UserRoleList[0],
                    DateCreated = new DateTime(2012,03,14,13,37,09),
                    IsDeleted = true
                }
            };

            AccountAvatarList = new List<AccountAvatar>
            {
                new AccountAvatar 
                {
                    AccountId = 1,
                    Avatar = new byte [] { },
                    MimeType = "image/png"
                },
                new AccountAvatar 
                {
                    AccountId = 2,
                    Avatar = new byte [] { },
                    MimeType = "image/png"
                }
            };
                
            PasswordResetList = new List<PasswordReset>();

            TopicCategoryList = new List<TopicCategory>
            {
                new TopicCategory
                {
                    TopicCategoryId=1,
                    Name = "Linie"
                },
                new TopicCategory
                {
                    TopicCategoryId=2,
                    Name="Ciekawostki"
                },
                new TopicCategory
                {
                    TopicCategoryId=3,
                    Name="Inne"
                }
            };

            TopicList = new List<Topic>
            {
                new Topic
                {
                    Account= AccountList[0],
                    AccountId=1,
                    City = CityList[0],
                    DateCreated = new DateTime(2015,05,15),
                    IsAnonymous = false,
                    IsDeleted = false,
                    LastUpdated = new DateTime(201,05,15),
                    Line = new Line(),
                    LineId = 1,
                    Message= "Siema ludzie. To ja, tester.",
                    TopicCategory = TopicCategoryList[0],
                    TopicId = 0,
                    
                    

                },
                new Topic
                {
                    Account= AccountList[0],
                    AccountId=1,
                    City = CityList[0],
                    DateCreated = new DateTime(2015,05,15),
                    IsAnonymous = false,
                    IsDeleted = false,
                    LastUpdated = new DateTime(201,05,15),
                    Line = new Line(),
                    LineId = 1,
                    Message= "Siema ludzie. To ja, tester.",
                    TopicCategory = TopicCategoryList[0],
                    TopicId = 0
                    

                }
            };

            LineList = new List<Line>()
            {
                new Line
                {
                    CityId = 1,
                    City = CityList[0],
                    Direction = "Kaszuby",
                    LineId =1,
                    LineName = "Jedynka"
                },
                new Line
                {
                    CityId = 2,
                    City = CityList[1],
                    Direction = "Haven-Hell",
                    LineId =2,
                    LineName = "Hell"
                }, 
            };

            TopicVoteList = new List<TopicVote>()
            {
                new TopicVote
                {
                    Account=AccountList[0],
                    AccountId=1,
                    Topic = TopicList[0],
                    TopicId =1,
                    VoteValue =5
                },
                new TopicVote
                {
                    Account=AccountList[1],
                    AccountId=2,
                    Topic = TopicList[1],
                    TopicId =2,
                    VoteValue =2
                }
            };

            PostList = new List<Post>()
            {
                new Post
                {
                    Account = AccountList[0],
                    AccountId =1,
                    DateCreated = new DateTime(2015,05,15),
                    IsDeleted=false,
                    LastUpdated = new DateTime(2015,05,16),
                    Message = "Siema tester",
                    PostId=1,
                    PostVote = null,// TODO zmienić to
                    Topic = TopicList[0],
                    TopicId=1 
                },
                new Post
                {
                    Account = AccountList[1],
                    AccountId =2,
                    DateCreated = new DateTime(2015,05,15),
                    IsDeleted=true,
                    LastUpdated = new DateTime(2015,05,16),
                    Message = "Jest czwarta w nocy, a ja tu siedze i pisze",
                    PostId=2,
                    PostVote = null,// TODO zmienić to
                    Topic = TopicList[0],
                    TopicId=1 
                }
            };

            PostVoteList = new List<PostVote>()
            {
                new PostVote
                {
                    Account = AccountList[0],
                    AccountId=1,
                    Post = PostList[0],
                    PostId =1,
                    VoteValue = 1
                },
                new PostVote
                {
                    Account = AccountList[0],
                    AccountId=1,
                    Post = PostList[1],
                    PostId =2,
                    VoteValue = 5
                }
            };

            ProfanityList = new List<Profanity>()
            {
                new Profanity
                {                
                    ProfanityId =1,
                    Word = "kurwa"
                },
                
                new Profanity
                {                
                    ProfanityId =2,
                    Word = "pierdole"
                },

                new Profanity
                {                
                    ProfanityId =1,
                    Word = "jebie"
                }
            };

            AccountBanList = new List<AccountBan>()
             {
                 new AccountBan
                 {
                     AccountId = 1,
                     Account = AccountList[0],
                     StartDate = DateTime.Parse("2015-06-07 00:00:00.000"),
                     EndDate = DateTime.Parse("2015-06-09 00:00:00.000"),     
                 }, 
                 new AccountBan
                 {
                     AccountId = 3,
                     Account = AccountList[2],
                     StartDate = DateTime.Parse("2015-06-08 12:00:00.000"),
                     EndDate = DateTime.Parse("2015-06-10 15:00:00.000"),     
                 }
             };
        }
    }
}
