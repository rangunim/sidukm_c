﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SIDUKM.Domain;
using SIDUKM.Logic;

namespace SIDUKM.UnitTests.Logic
{
    [TestClass]
    public class SpottedServiceTests
    {
        public SpottedService Target { get; set; }
        DomainMocks Domain { get; set; }
        RepositoriesMocks Repository { get; set; }
        ServicesMocks Service { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            Domain = DomainMocks.Instance;
            Repository = new RepositoriesMocks();
            Service = new ServicesMocks();
            Target = new SpottedService(Repository.MockTopicRepository.Object, Repository.MockAccountRepository.Object, Repository.MockCityRepository.Object,
                Repository.MockLineRepository.Object, Repository.MockTopicCategoryRepository.Object, Repository.MockTopicVoteRepository.Object,
                Repository.MockPostRepository.Object, Repository.MockPostVoteRepository.Object, Repository.MockProfanityRepository.Object);
        }

        [TestMethod]
        public void CanGetTopicCategory()
        {
            var result = Target.GetTopicCategories();
            Assert.IsNotNull(result);
            Assert.AreEqual(result[0].Name, "Linie");
            Assert.AreEqual(result[result.Count-1].Name,"Inne");
            Assert.AreEqual(result[0].TopicCategoryId, 1);
        }

        [TestMethod]
        public void CanAddTopic()
        {
            Account account = Domain.AccountList[0];
            var topic = new Topic()
            {
                TopicId = 1,
                AccountId = account.AccountId,
                CityId = account.CityId,
                DateCreated = new DateTime(2015,05,15),
                IsAnonymous=false,
                IsDeleted=false,
                LineId = 1,
                Message ="To jest tak testowa wiadomość, nie zwracaj na nią uwagi",
                TopicCategoryId =1    
            };

            Target.AddTopic(topic);
            int listSize = Domain.TopicList.Count;
            Assert.IsNotNull(Domain.TopicList[listSize-1]);
            Assert.AreEqual(Domain.TopicList[listSize - 1].CityId, topic.CityId);
            Assert.AreEqual(Domain.TopicList[listSize - 1].DateCreated, topic.DateCreated);
            Assert.IsNull(Domain.TopicList[listSize - 1].LastUpdated);
            Assert.AreEqual(Domain.TopicList[listSize - 1].Message, topic.Message);
            Assert.AreEqual(Domain.TopicList[listSize - 1].Message, "To jest tak testowa wiadomość, nie zwracaj na nią uwagi");
            Assert.AreEqual(Domain.TopicList[listSize - 1].Account, topic.Account);
            Assert.AreEqual(Domain.TopicList[listSize - 1], topic);
        }
    }
}
