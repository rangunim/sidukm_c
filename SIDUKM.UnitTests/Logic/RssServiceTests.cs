﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIDUKM.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIDUKM.UnitTests.Logic
{
    [TestClass]
    public class RssServiceTests
    {
        public IRssService Target { get; set; }
        DomainMocks Domain { get; set; }
        RepositoriesMocks Repository { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            Domain = DomainMocks.Instance;
            Repository = new RepositoriesMocks();
            Target = new RssService(Repository.MockCityRepository.Object, Repository.MockAccountRepository.Object);
        }

        [TestMethod]
        public void CanGetRssLink()
        {
            var result = Target.GetRssLink("login");
            var user = Domain.AccountList.FirstOrDefault(x => x.AccountLogin == "login");
            Assert.IsNotNull(result);
            Assert.AreEqual(Domain.CityList.FirstOrDefault(x => x.CityId == user.CityId).RssLink, result);

            var defaultRss = Domain.CityList.FirstOrDefault(x => x.CityId == 1).RssLink;
            result = Target.GetRssLink("");
            Assert.IsNotNull(result);
            Assert.AreEqual(defaultRss, result);

            result = Target.GetRssLink(null);
            Assert.IsNotNull(result);
            Assert.AreEqual(defaultRss, result);
        }

        [TestMethod]
        public void CanGetNews()
        {
            var defaultRss = Domain.CityList.FirstOrDefault(x => x.CityId == 1).RssLink;
            var result = Target.GetNews(defaultRss);
            Assert.IsNotNull(result);
            Assert.AreNotEqual(0, result.Count());

            result = Target.GetNews("");
            Assert.IsNull(result);

            result = Target.GetNews(null);
            Assert.IsNull(result);
        }
    }
}
