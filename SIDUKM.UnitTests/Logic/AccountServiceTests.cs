﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIDUKM.Domain;
using SIDUKM.Logic;
using System.Web.Mvc;

namespace SIDUKM.UnitTests
{
    [TestClass]
    public class AccountServiceTests
    {
        public IAccountService Target { get; set; }
        DomainMocks Domain { get; set; }
        RepositoriesMocks Repository { get; set; }
        ServicesMocks Service { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            Domain = DomainMocks.Instance;
            Repository = new RepositoriesMocks();
            Service = new ServicesMocks();
            Target = new AccountService(Repository.MockCityRepository.Object, Repository.MockGenderRepository.Object, 
                Repository.MockUserRoleRepository.Object, Repository.MockAccountRepository.Object,
                Repository.MockPasswordResetRepository.Object, Repository.MockAccountAvatarRepository.Object,
                Repository.MockAccountBanRepository.Object);      
        }

        [TestMethod]
        public void CanGetCities()
        {
            var result = Target.GetCities();
            Assert.IsNotNull(result);
            Assert.AreEqual(Domain.CityList.Count, result.Count);
            Assert.AreEqual("Testowe",result.ToList()[0].CityName);
        }

        [TestMethod]
        public void CanGetAccounts()
        {
            var result = Target.GetAccounts();
            Assert.IsNotNull(result);
            Assert.AreEqual(Domain.AccountList.Count, result.Count);
            Assert.AreEqual("login", result.ToList()[0].AccountLogin);
        }

        [TestMethod]
        public void CanGetAccount()
        {
            var result = Target.GetAccountByUsername("login", "test");
            Assert.IsNotNull(result);
            Assert.AreEqual(Domain.AccountList[0], result);
        }

        [TestMethod]
        public void CanRegisterAccount()
        {
            var input = new Account
            {
                AccountLogin = "Aga",
                AccountPassword = "aga_Cieslak_04.",
                Email = "a@a.pl",
                FirstName = "Agni eszka",
                LastName = "Cieślak-Cieślak",
                CityId = 1,
                UserRoleId = 1
            };
            string pass = "aga_Cieslak_04.";

            var result = Target.RegisterAccount(input);
            Assert.IsTrue(result);
            Assert.AreEqual(Domain.AccountList[Domain.AccountList.Count - 1].AccountLogin, input.AccountLogin);
            Assert.AreEqual(Domain.AccountList[Domain.AccountList.Count - 1].AccountPassword, input.AccountPassword);

            var loginResult = Target.GetAccountByUsername(input.AccountLogin, pass);
            Assert.IsNotNull(loginResult);
            Assert.AreEqual(input.AccountLogin, loginResult.AccountLogin);

            // bledny email - niepoprawny format
            input = new Account
            {
                AccountLogin = "Aga2",
                AccountPassword = "aga_Cieslak_04.",
                Email = "a@",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledne haslo - mniej niz 6 znakow
            input = new Account
            {
                AccountLogin = "Aga3",
                AccountPassword = "ag4Ci",
                Email = "a@a2.pl",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledne haslo - brak co najmniej jednej cyfry
            input = new Account
            {
                AccountLogin = "Aga5",
                AccountPassword = "agaCieslak",
                Email = "a@a4.pl",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledne haslo - brak co najmniej jednej litery
            input = new Account
            {
                AccountLogin = "Aga6",
                AccountPassword = "45.39-36",
                Email = "a@a5.pl",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledny login - powtarzajacy sie
            input = new Account
            {
                AccountLogin = "login",
                AccountPassword = "aga_Cieslak_04.",
                Email = "a@a3.pl",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledny email - powtarzajacy sie
            input = new Account
            {
                AccountLogin = "Aga4",
                AccountPassword = "aga_Cieslak_04.",
                Email = "e@e-mail.com",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledne imie
            input = new Account
            {
                AccountLogin = "Aga7",
                AccountPassword = "aga_Cieslak_04.",
                Email = "e@e2-mail.com",
                FirstName = "Agnieszk@",
                LastName = "Cieślak",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
            // bledne nazwisko
            input = new Account
            {
                AccountLogin = "Aga8",
                AccountPassword = "aga_Cieslak_04.",
                Email = "e@e3-mail.com",
                FirstName = "Agnieszka",
                LastName = "Cieslak-Cie5slak",
                CityId = 1,
                UserRoleId = 1
            };
            result = Target.RegisterAccount(input);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void CanPasswordReset()
        {
            //Create Test account
            var input = new Account
            {
                AccountLogin = "michal",
                AccountPassword = "qwerty12345",
                Email = "aa@wp.pl",
                FirstName = "Michal",
                LastName = "Ste",
                CityId = 1,
                UserRoleId = 3
            };
            var result = Target.RegisterAccount(input);

            //Log in to crreated account (input) - check to created in database
            Account loginResult = Target.GetAccountByUsername("michal", "qwerty12345");
            Assert.IsNotNull(loginResult);
            Assert.AreEqual(input.AccountLogin, loginResult.AccountLogin);

            //===== Normal test (the simplest case) - made in accordane with procedure
            //create GUID 
            Guid? result2 = Target.CreateResetLink(loginResult.Email);
            Assert.IsNotNull(result2);
            Assert.AreEqual(Target.GetAccountIdByGuid(result2.Value), loginResult.AccountId); 
            var entry = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == loginResult.AccountId);
            Assert.AreEqual(entry.Link, result2.Value);
            
            //change password
            string oldPass = loginResult.AccountPassword;
            Target.ChangePassword("qazwsx12345", Target.GetAccountIdByGuid(result2.Value));
            Assert.AreNotEqual(loginResult.AccountPassword, oldPass);

            //===== Test - Change again password(before succesfully changed password) with old GUID. =====
            oldPass = loginResult.AccountPassword;
            Target.ChangePassword("nowehaslo12345", Target.GetAccountIdByGuid(result2.Value)); //tu mamy nowe haslo dla loginResult,
            Assert.AreEqual(oldPass, loginResult.AccountPassword);
           
            //=== Test - Change password when user created guid and didn't finished this process and maked again new GUID. ===
            //create  2x GUID for the same account
            result2 = Target.CreateResetLink(loginResult.Email);
            Guid? result3 = Target.CreateResetLink(loginResult.Email);

            Assert.IsNotNull(result2);
            Assert.IsNotNull(result3);

            entry = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == loginResult.AccountId);
            Assert.AreEqual(entry.Link, result3.Value);
            Assert.AreEqual(entry.Link, result2.Value);
            Assert.AreEqual(Target.GetAccountIdByGuid(result2.Value), loginResult.AccountId); 
            
            //change password - for complete the process
            oldPass = loginResult.AccountPassword; //tu mamy "qazwsx12345"
            Target.ChangePassword("qazwsx12345lalalla", Target.GetAccountIdByGuid(result3.Value));
            Assert.AreNotEqual(loginResult.AccountPassword, oldPass);

            //=== Test - When we have in database much GUID for much accounts. ===
            Account acc1 = loginResult; 
            Account acc2 = Domain.AccountList[0];
            Account acc3 = Domain.AccountList[1];

            //create GUID's for accounts
            Guid? acc1Guid = Target.CreateResetLink(acc1.Email);
            Guid? acc2Guid = Target.CreateResetLink(acc2.Email);
            Guid? acc3Guid = Target.CreateResetLink(acc3.Email);

            Assert.IsNotNull(acc1Guid);
            Assert.IsNotNull(acc2Guid);
            Assert.IsNotNull(acc3Guid);

            var entry1 = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == acc1.AccountId);
            var entry2 = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == acc2.AccountId);
            var entry3 = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == acc3.AccountId);
            Assert.AreEqual(acc1Guid.Value, entry1.Link);
            Assert.AreEqual(acc2Guid.Value, entry2.Link);
            Assert.AreEqual(acc3Guid.Value, entry3.Link);

            Assert.AreNotEqual(acc1Guid.Value, entry2.Link);
            Assert.AreNotEqual(acc1Guid.Value, entry3.Link);
            Assert.AreNotEqual(acc2Guid.Value, entry3.Link); 

            //change password's
            string oldPass1 = acc1.AccountPassword;
            string oldPass2 = acc2.AccountPassword;
            string oldPass3 = acc3.AccountPassword;

            Target.ChangePassword("password1", Target.GetAccountIdByGuid(acc1Guid.Value));
            Target.ChangePassword("password2", Target.GetAccountIdByGuid(acc2Guid.Value));
            Target.ChangePassword("password3", Target.GetAccountIdByGuid(acc3Guid.Value));
            
            Assert.AreNotEqual(acc1.AccountPassword, oldPass1);
            Assert.AreNotEqual(acc2.AccountPassword, oldPass2);
            Assert.AreNotEqual(acc3.AccountPassword, oldPass3);
            Assert.AreNotEqual(acc1.AccountPassword, oldPass2);
            Assert.AreNotEqual(acc1.AccountPassword, oldPass3);
            Assert.AreNotEqual(acc2.AccountPassword, oldPass3);

            //=== Test - Change password when many user's created guid and didn't finished this process and maked again GUID. ===

            //create 3x GUID's for accounts
            acc1Guid = Target.CreateResetLink(acc1.Email);
            acc2Guid = Target.CreateResetLink(acc2.Email);
            acc3Guid = Target.CreateResetLink(acc3.Email);
            Guid? acc1Guid1 = Target.CreateResetLink(acc1.Email);
            Guid? acc2Guid1 = Target.CreateResetLink(acc2.Email);
            Guid? acc3Guid1 = Target.CreateResetLink(acc3.Email);
            Guid? acc1Guid2 = Target.CreateResetLink(acc1.Email);
            Guid? acc2Guid2 = Target.CreateResetLink(acc2.Email);
            Guid? acc3Guid2 = Target.CreateResetLink(acc3.Email);

            entry1 = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == acc1.AccountId);
            entry2 = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == acc2.AccountId);
            entry3 = Domain.PasswordResetList.FirstOrDefault(x => x.AccountId == acc3.AccountId);

            Assert.IsNotNull(acc1Guid);
            Assert.IsNotNull(acc2Guid);
            Assert.IsNotNull(acc3Guid);

            Assert.AreEqual(entry1.Link, acc1Guid.Value); //wszystkie  guid'y danego accounta się liczą (bo są takie same).
            Assert.AreEqual(entry1.Link, acc1Guid1.Value);        
            Assert.AreEqual(entry1.Link, acc1Guid2.Value);
            Assert.AreEqual(entry2.Link, acc2Guid.Value);
            Assert.AreEqual(entry2.Link, acc2Guid1.Value);
            Assert.AreEqual(entry2.Link, acc2Guid2.Value);
            Assert.AreEqual(entry3.Link, acc3Guid.Value);
            Assert.AreEqual(entry3.Link, acc3Guid1.Value);
            Assert.AreEqual(entry3.Link, acc3Guid2.Value);

            Assert.AreNotEqual(acc1Guid.Value, entry2.Link); //mieszanka (aktywnych guid) by guid z acc1 nie byl taki sam jak dla acc2.
            Assert.AreNotEqual(acc1Guid.Value, entry3.Link);
            Assert.AreNotEqual(acc2Guid.Value, entry3.Link);
      
            //mieszanek starych guid nie trzeba,stad pominalem...
        
            Assert.AreEqual(Target.GetAccountIdByGuid(acc1Guid2.Value), acc1.AccountId); // czy wlasciwe pobral konto wzgledem guid
            Assert.AreEqual(Target.GetAccountIdByGuid(acc2Guid2.Value), acc2.AccountId);
            Assert.AreEqual(Target.GetAccountIdByGuid(acc3Guid2.Value), acc3.AccountId);

            //change password's - for complete the process
            oldPass1 = acc1.AccountPassword;
            oldPass2 = acc2.AccountPassword;
            oldPass3 = acc3.AccountPassword;

            Target.ChangePassword("qazwsx123451", Target.GetAccountIdByGuid(acc1Guid2.Value));
            Target.ChangePassword("qazwsx123452", Target.GetAccountIdByGuid(acc2Guid2.Value));
            Target.ChangePassword("qazwsx123453", Target.GetAccountIdByGuid(acc3Guid2.Value));

            Assert.AreNotEqual(acc1.AccountPassword, oldPass1);
            Assert.AreNotEqual(acc2.AccountPassword, oldPass2);
            Assert.AreNotEqual(acc3.AccountPassword, oldPass3);

            Assert.AreNotEqual(acc1.AccountPassword, oldPass2); //mieszanka nowych hasel innego konta z innym kontem
            Assert.AreNotEqual(acc1.AccountPassword, oldPass3);
            Assert.AreNotEqual(acc2.AccountPassword, oldPass3);

            // ===Test when not exists account (user) want create new guid ===
            acc1Guid = Target.CreateResetLink("adresEmail@wp.pl");
            Assert.IsNull(acc1Guid);

             // ===Test when we try change password with not exists guid ===
            oldPass = acc1.AccountPassword;
            Target.ChangePassword("newPassword1", Target.GetAccountIdByGuid(acc1Guid1.Value));
          
            Assert.AreEqual(oldPass, acc1.AccountPassword);
        }

       /* [TestMethod]
        public void CanGetAccountData()
        {
            var result = Target.GetAccountsData("DefaultView"); //TODO po dodaniu widoku zbanowanych zwraca null
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count(), Domain.AccountList.Where(x=>!x.IsDeleted).Count()); 
            Assert.AreEqual(Domain.AccountList[0].AccountLogin, result.ToList()[0].Login);
            Assert.AreEqual(Domain.AccountList[1].AccountId, result.ToList()[1].Id);
        }*/

        [TestMethod]
        public void CanEditAccount()
        {
            var account = Domain.AccountList[0];
            var toEdit = new Account()
            {
                AccountLogin = account.AccountLogin,
                AccountPassword = account.AccountPassword,
                AccountId = account.AccountId,
                City = account.City,
                CityId = account.CityId,
                DateCreated = account.DateCreated,
                Email = account.Email,
                FirstName = account.FirstName,
                Gender = account.Gender,
                GenderId = account.GenderId,
                IsDeleted = account.IsDeleted,
                LastName = account.LastName,
                UserRole = account.UserRole,
                UserRoleId = account.UserRoleId
            };
            toEdit.Email = "test_edycji@a.pl";
            toEdit.FirstName = "testowyEdit";
            var result = Target.EditAccount(toEdit, null);
            Assert.IsTrue(result);
            Assert.AreEqual(Domain.AccountList[0].Email, toEdit.Email);
            Assert.AreEqual(Domain.AccountList[0].FirstName, toEdit.FirstName);

            var toEdit2 = new Account()
            {
                AccountId = 666,
                AccountLogin = "huahua",
                AccountPassword = "a0905cde5632e484dbcf5f943738c507",
                CityId = 2,
                Email = "huahua@hua.com",
                FirstName = "User",
                GenderId = 1,
                LastName = "User LastName",
                UserRoleId = 3,
                DateCreated = DateTime.Now,
                IsDeleted = false
            };
            var result2 = Target.EditAccount(toEdit2, null);
            Assert.IsFalse(result2);
            Assert.IsNull(Domain.AccountList.FirstOrDefault(x => x.AccountId == toEdit2.AccountId));
        }

        [TestMethod]
        public void CanDeleteAccount()
        {
            var toDelete = Domain.AccountList[1];
            var result = Target.DeleteAccount(toDelete.AccountId);
            Assert.IsTrue(result);
            Assert.IsTrue(toDelete.IsDeleted);

            result = Target.DeleteAccount(666);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void CanRetriveAvatar()
        {
            AccountAvatar avatar = new AccountAvatar //stworzony dla testów
            {
                AccountId = 1,
                Avatar = new byte [] { },
                MimeType = "image/png"
            };

           
            var result1 = Target.GetAccountAvatar(avatar.AccountId);
            Assert.IsNotNull(result1);
            Assert.IsInstanceOfType(result1, typeof(AccountAvatar));
            Assert.AreEqual(avatar.MimeType, result1.MimeType);
            
            //======for InvalidID======
            result1 = Target.GetAccountAvatar(10000);
            Assert.IsNull(result1);
            //Assert.IsInstanceOfType(result1, typeof(FileResult));
            //Assert.AreEqual("image/png", result1.MimeType);
        }

        [TestMethod]
        public void CanGetAccountBan()
        {
            //Account are banned
            var result1 = Target.GetBan(1); 
            Assert.IsNotNull(result1);
            Assert.IsInstanceOfType(result1, typeof(AccountBan));
            Assert.IsTrue(Target.IsUserBanned(result1.AccountId));

            //Account are not banned
            result1 = Target.GetBan(2);
            Assert.IsNull(result1);
        }
        [TestMethod]
        public void CanGetUserBanned()
        {
            //Account are banned
            var result1 = Target.GetBan(1);
            Assert.IsNotNull(result1);
            Assert.IsTrue(Target.IsUserBanned(result1.AccountId));

            //Account are not banned
            result1 = Target.GetBan(2);
            Assert.IsNull(result1);
            Assert.IsFalse(Target.IsUserBanned(2));
        }

        [TestMethod] 
        public void CanSetAccountBan()
        {
            var result1 = Target.GetBan(2);
            Assert.IsNull(result1);
            Assert.IsFalse(Target.IsUserBanned(2));
           
            Target.SetBan(2, DateTime.Parse("2015-06-07 00:00:00.000"), DateTime.Parse("2015-07-09 00:00:00.000"));

            result1 = Target.GetBan(2);
            Assert.IsNotNull(result1);
            Assert.IsInstanceOfType(result1, typeof(AccountBan));
            Assert.IsTrue(Target.IsUserBanned(2));
        }

        [TestMethod] 
        public void CanGetDateOfBanEnd()
        {
            var result1 = Target.GetBan(2);
            Assert.IsNotNull(result1);
            Assert.IsInstanceOfType(result1, typeof(AccountBan));

            DateTime end = Target.GetDateOfBanEnd(result1.AccountId);
            Assert.IsInstanceOfType(end, typeof(DateTime));
            Assert.AreEqual(result1.EndDate, end);
        }

    }

   
}
