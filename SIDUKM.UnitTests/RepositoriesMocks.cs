﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Moq;
using SIDUKM.Domain;
using SIDUKM.Logic.Repository;

namespace SIDUKM.UnitTests
{
    class RepositoriesMocks
    {
        public Mock<IRepository<City>> MockCityRepository { get; set; }
        public Mock<IRepository<Gender>> MockGenderRepository { get; set; }
        public Mock<IRepository<UserRole>> MockUserRoleRepository { get; set; }
        public Mock<IRepository<Account>> MockAccountRepository { get; set; }
        public Mock<IRepository<AccountAvatar>> MockAccountAvatarRepository { get; set; }
        public Mock<IRepository<PasswordReset>> MockPasswordResetRepository { get; set; }
        public Mock<IRepository<Topic>> MockTopicRepository { get; set; }
        public Mock<IRepository<Line>> MockLineRepository {get;set;}
        public Mock<IRepository<TopicCategory>> MockTopicCategoryRepository { get; set; }
        public Mock<IRepository<TopicVote>> MockTopicVoteRepository { get; set; }
        public Mock<IRepository<Post>> MockPostRepository { get; set; }
        public Mock<IRepository<PostVote>> MockPostVoteRepository { get; set; }
        public Mock<IRepository<Profanity>> MockProfanityRepository { get; set; } 
        public Mock<IRepository<AccountBan>> MockAccountBanRepository { get; set; }

        public RepositoriesMocks()
        {
            var dm = DomainMocks.Instance;
            MockCityRepository = new Mock<IRepository<City>>();
            MockCityRepository.Setup(m => m.GetAll()).Returns(dm.CityList.AsQueryable());
            MockCityRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<City, bool>>>(), It.IsAny<Expression<Func<City, object>>[]>()))
                .Returns<Expression<Func<City, bool>>, Expression<Func<City, object>>[]>((predicate, includeProperties)
                    => dm.CityList.AsQueryable().FirstOrDefault(predicate));
            MockCityRepository.Setup(m => m.Update(It.IsAny<City>())).Callback((City city) =>
            {
                int i = dm.CityList.FindIndex(x => x.CityId == city.CityId);
                dm.CityList[i] = city;
            });
            MockCityRepository.Setup(m => m.Delete(It.IsAny<City>())).Callback((City city) =>
            {
                dm.CityList.Remove(city);
            });
            MockCityRepository.Setup(m => m.Add(It.IsAny<City>())).Callback((City city) =>
            {
                city.CityId = (short)(dm.CityList.Last().CityId + 1);
                dm.CityList.Add(city);
            });
            
            MockGenderRepository = new Mock<IRepository<Gender>>();
            MockGenderRepository.Setup(m => m.GetAll()).Returns(dm.GenderList.AsQueryable());
            MockGenderRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<Gender, bool>>>(), It.IsAny<Expression<Func<Gender, object>>[]>()))
                .Returns<Expression<Func<Gender, bool>>, Expression<Func<Gender, object>>[]>((predicate, includeProperties)
                    => dm.GenderList.AsQueryable().FirstOrDefault(predicate));
            
            MockUserRoleRepository = new Mock<IRepository<UserRole>>();
            MockUserRoleRepository.Setup(m => m.GetAll()).Returns(dm.UserRoleList.AsQueryable());
            MockUserRoleRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<UserRole, bool>>>(), It.IsAny<Expression<Func<UserRole, object>>[]>()))
                .Returns<Expression<Func<UserRole, bool>>, Expression<Func<UserRole, object>>[]>((predicate, includeProperties)
                    => dm.UserRoleList.AsQueryable().FirstOrDefault(predicate));
            
            MockAccountRepository = new Mock<IRepository<Account>>();
            MockAccountRepository.Setup(m => m.GetAll()).Returns(dm.AccountList.AsQueryable());
            MockAccountRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<Account, bool>>>(), It.IsAny<Expression<Func<Account, object>>[]>()))
                .Returns<Expression<Func<Account, bool>>, Expression<Func<Account, object>>[]>((predicate, includeProperties)
                    => dm.AccountList.AsQueryable().FirstOrDefault(predicate));
            MockAccountRepository.Setup(m => m.Update(It.IsAny<Account>())).Callback((Account account) =>
            {
                int i = dm.AccountList.FindIndex(x => x.AccountId == account.AccountId);
                dm.AccountList[i] = account;
            });
            MockAccountRepository.Setup(m => m.Delete(It.IsAny<Account>())).Callback((Account account) =>
            {
                dm.AccountList.Remove(account);
            });
            MockAccountRepository.Setup(m => m.Add(It.IsAny<Account>())).Callback((Account account) =>
            {
                account.AccountId = (dm.AccountList.Last().AccountId + 1);
                dm.AccountList.Add(account);
            });

            MockAccountAvatarRepository = new Mock<IRepository<AccountAvatar>>();
            MockAccountAvatarRepository.Setup(m => m.GetAll()).Returns(dm.AccountAvatarList.AsQueryable());
            MockAccountAvatarRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<AccountAvatar, bool>>>(), It.IsAny<Expression<Func<AccountAvatar, object>>[]>()))
                .Returns<Expression<Func<AccountAvatar, bool>>, Expression<Func<AccountAvatar, object>>[]>((predicate, includeProperties)
                    => dm.AccountAvatarList.AsQueryable().FirstOrDefault(predicate));
            MockAccountAvatarRepository.Setup(m => m.Update(It.IsAny<AccountAvatar>())).Callback((AccountAvatar AccountAvatar) =>
            {
                int i = dm.AccountAvatarList.FindIndex(x => x.AccountId == AccountAvatar.AccountId);
                dm.AccountAvatarList[i] = AccountAvatar;
            });
            MockAccountAvatarRepository.Setup(m => m.Delete(It.IsAny<AccountAvatar>())).Callback((AccountAvatar AccountAvatar) =>
            {
                dm.AccountAvatarList.Remove(AccountAvatar);
            });
            MockAccountAvatarRepository.Setup(m => m.Add(It.IsAny<AccountAvatar>())).Callback((AccountAvatar AccountAvatar) =>
            {
                dm.AccountAvatarList.Add(AccountAvatar);
            });

            MockPasswordResetRepository = new Mock<IRepository<PasswordReset>>();
            MockPasswordResetRepository.Setup(m => m.GetAll()).Returns(dm.PasswordResetList.AsQueryable());
            MockPasswordResetRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<PasswordReset, bool>>>(), It.IsAny<Expression<Func<PasswordReset, object>>[]>()))
                .Returns<Expression<Func<PasswordReset, bool>>, Expression<Func<PasswordReset, object>>[]>((predicate, includeProperties)
                    => dm.PasswordResetList.AsQueryable().FirstOrDefault(predicate));
            MockPasswordResetRepository.Setup(m => m.Update(It.IsAny<PasswordReset>())).Callback((PasswordReset PasswordReset) =>
            {
                int i = dm.PasswordResetList.FindIndex(x => x.AccountId == PasswordReset.AccountId);
                dm.PasswordResetList[i] = PasswordReset;
            });
            MockPasswordResetRepository.Setup(m => m.Delete(It.IsAny<PasswordReset>())).Callback((PasswordReset PasswordReset) =>
            {
                dm.PasswordResetList.Remove(PasswordReset);
            });
            MockPasswordResetRepository.Setup(m => m.Add(It.IsAny<PasswordReset>())).Callback((PasswordReset PasswordReset) =>
            {
                dm.PasswordResetList.Add(PasswordReset);
            });

            MockTopicRepository = new Mock<IRepository<Topic>>();
            MockTopicRepository.Setup(m => m.GetAll()).Returns(dm.TopicList.AsQueryable());
            MockTopicRepository.Setup(m=>m.FindBy(It.IsAny<Expression<Func<Topic,bool>>>(), It.IsAny<Expression<Func<Topic, object>>[]>()))
                .Returns<Expression<Func<Topic,bool>>, Expression<Func<Topic, object>>[]>((predicate, includeProperties)=>dm.TopicList.AsQueryable().FirstOrDefault(predicate));
            MockTopicRepository.Setup(m => m.Update(It.IsAny<Topic>())).Callback((Topic Topic) =>
                {
                    int i = dm.TopicList.FindIndex(x => x.TopicId == Topic.TopicId);
                    dm.TopicList[i] = Topic;
                });
            MockTopicRepository.Setup(m => m.Delete(It.IsAny<Topic>())).Callback((Topic Topic) =>
                {
                    dm.TopicList.Remove(Topic);
                });
            MockTopicRepository.Setup(m => m.Add(It.IsAny<Topic>())).Callback((Topic Topic) =>
            {
                dm.TopicList.Add(Topic);
            });

            MockTopicCategoryRepository = new Mock<IRepository<TopicCategory>>();
            MockTopicCategoryRepository.Setup(m => m.GetAll()).Returns(dm.TopicCategoryList.AsQueryable());
            MockTopicCategoryRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<TopicCategory, bool>>>(), It.IsAny<Expression<Func<TopicCategory, object>>[]>()))
                .Returns<Expression<Func<TopicCategory, bool>>, Expression<Func<TopicCategory, object>>[]>((predicate, includeProperties) => dm.TopicCategoryList.AsQueryable().FirstOrDefault(predicate));
            MockTopicCategoryRepository.Setup(m => m.Update(It.IsAny<TopicCategory>())).Callback((TopicCategory TopicCategory) =>
            {
                int i = dm.TopicCategoryList.FindIndex(x => x.TopicCategoryId == TopicCategory.TopicCategoryId);
                dm.TopicCategoryList[i] = TopicCategory;
            });
            MockTopicCategoryRepository.Setup(m => m.Delete(It.IsAny<TopicCategory>())).Callback((TopicCategory TopicCategory) =>
            {
                dm.TopicCategoryList.Remove(TopicCategory);
            });
            MockTopicCategoryRepository.Setup(m => m.Add(It.IsAny<TopicCategory>())).Callback((TopicCategory TopicCategory) =>
            {
                dm.TopicCategoryList.Add(TopicCategory);
            });

            MockLineRepository = new Mock<IRepository<Line>>();
            MockLineRepository.Setup(m => m.GetAll()).Returns(dm.LineList.AsQueryable());
            MockLineRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<Line, bool>>>(), It.IsAny<Expression<Func<Line, object>>[]>()))
                .Returns<Expression<Func<Line, bool>>, Expression<Func<Line, object>>[]>((predicate, includeProperties) => dm.LineList.AsQueryable().FirstOrDefault(predicate));
            MockLineRepository.Setup(m => m.Update(It.IsAny<Line>())).Callback((Line Line) =>
            {
                int i = dm.LineList.FindIndex(x => x.LineId == Line.LineId);
                dm.LineList[i] = Line;
            });
            MockLineRepository.Setup(m => m.Delete(It.IsAny<Line>())).Callback((Line Line) =>
            {
                dm.LineList.Remove(Line);
            });
            MockLineRepository.Setup(m => m.Add(It.IsAny<Line>())).Callback((Line Line) =>
            {
                dm.LineList.Add(Line);
            });

            MockTopicVoteRepository = new Mock<IRepository<TopicVote>>();
            MockTopicVoteRepository.Setup(m => m.GetAll()).Returns(dm.TopicVoteList.AsQueryable());
            MockTopicVoteRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<TopicVote, bool>>>(), It.IsAny<Expression<Func<TopicVote, object>>[]>()))
                .Returns<Expression<Func<TopicVote, bool>>, Expression<Func<TopicVote, object>>[]>((predicate, includeProperties) => dm.TopicVoteList.AsQueryable().FirstOrDefault(predicate));
            MockTopicVoteRepository.Setup(m => m.Update(It.IsAny<TopicVote>())).Callback((TopicVote TopicVote) =>
            {
                int i = dm.TopicVoteList.FindIndex(x => x.AccountId == TopicVote.AccountId && x.TopicId == TopicVote.TopicId);
                dm.TopicVoteList[i] = TopicVote;
            });
            MockTopicVoteRepository.Setup(m => m.Delete(It.IsAny<TopicVote>())).Callback((TopicVote TopicVote) =>
            {
                dm.TopicVoteList.Remove(TopicVote);
            });
            MockTopicVoteRepository.Setup(m => m.Add(It.IsAny<TopicVote>())).Callback((TopicVote TopicVote) =>
            {
                dm.TopicVoteList.Add(TopicVote);
            });

            MockPostRepository = new Mock<IRepository<Post>>();
            MockPostRepository.Setup(m => m.GetAll()).Returns(dm.PostList.AsQueryable());
            MockPostRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<Post, bool>>>(), It.IsAny<Expression<Func<Post, object>>[]>()))
                .Returns<Expression<Func<Post, bool>>, Expression<Func<Post, object>>[]>((predicate, includeProperties) => dm.PostList.AsQueryable().FirstOrDefault(predicate));
            MockPostRepository.Setup(m => m.Update(It.IsAny<Post>())).Callback((Post Post) =>
            {
                int i = dm.PostList.FindIndex(x => x.PostId == Post.PostId);
                dm.PostList[i] = Post;
            });
            MockPostRepository.Setup(m => m.Delete(It.IsAny<Post>())).Callback((Post Post) =>
            {
                dm.PostList.Remove(Post);
            });
            MockPostRepository.Setup(m => m.Add(It.IsAny<Post>())).Callback((Post Post) =>
            {
                dm.PostList.Add(Post);
            });

            MockPostVoteRepository = new Mock<IRepository<PostVote>>();
            MockPostVoteRepository.Setup(m => m.GetAll()).Returns(dm.PostVoteList.AsQueryable());
            MockPostVoteRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<PostVote, bool>>>(), It.IsAny<Expression<Func<PostVote, object>>[]>()))
                .Returns<Expression<Func<PostVote, bool>>, Expression<Func<PostVote, object>>[]>((predicate, includeProperties) => dm.PostVoteList.AsQueryable().FirstOrDefault(predicate));
            MockPostVoteRepository.Setup(m => m.Update(It.IsAny<PostVote>())).Callback((PostVote PostVote) =>
            {
                int i = dm.PostVoteList.FindIndex(x => x.AccountId == PostVote.AccountId && x.PostId == PostVote.PostId);
                dm.PostVoteList[i] = PostVote;
            });
            MockPostVoteRepository.Setup(m => m.Delete(It.IsAny<PostVote>())).Callback((PostVote PostVote) =>
            {
                dm.PostVoteList.Remove(PostVote);
            });
            MockPostVoteRepository.Setup(m => m.Add(It.IsAny<PostVote>())).Callback((PostVote PostVote) =>
            {
                dm.PostVoteList.Add(PostVote);
            });

            MockProfanityRepository = new Mock<IRepository<Profanity>>();
            MockProfanityRepository.Setup(m => m.GetAll()).Returns(dm.ProfanityList.AsQueryable());
            MockProfanityRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<Profanity, bool>>>(), It.IsAny<Expression<Func<Profanity, object>> []>()))
                .Returns<Expression<Func<Profanity, bool>>, Expression<Func<Profanity, object>> []>((predicate, includeProperties) => dm.ProfanityList.AsQueryable().FirstOrDefault(predicate));
            MockProfanityRepository.Setup(m => m.Update(It.IsAny<Profanity>())).Callback((Profanity Profanity) =>
            {
                int i = dm.ProfanityList.FindIndex(x => x.Word.Equals(Profanity.Word, StringComparison.OrdinalIgnoreCase));
                dm.ProfanityList [i] = Profanity;
            });
            MockProfanityRepository.Setup(m => m.Delete(It.IsAny<Profanity>())).Callback((Profanity Profanity) =>
            {
                dm.ProfanityList.Remove(Profanity);
            });
            MockProfanityRepository.Setup(m => m.Add(It.IsAny<Profanity>())).Callback((Profanity Profanity) =>
            {
                dm.ProfanityList.Add(Profanity);
            });

            MockAccountBanRepository = new Mock<IRepository<AccountBan>>();
            MockAccountBanRepository.Setup(m => m.GetAll()).Returns(dm.AccountBanList.AsQueryable());
            MockAccountBanRepository.Setup(m => m.FindBy(It.IsAny<Expression<Func<AccountBan, bool>>>(), It.IsAny<Expression<Func<AccountBan, object>> []>()))
                .Returns<Expression<Func<AccountBan, bool>>, Expression<Func<AccountBan, object>> []>((predicate, includeProperties) => dm.AccountBanList.AsQueryable().FirstOrDefault(predicate));
            MockAccountBanRepository.Setup(m => m.Update(It.IsAny<AccountBan>())).Callback((AccountBan AccountBan) =>
            {
                int i = dm.AccountBanList.FindIndex(x => x.AccountId == AccountBan.AccountId);
                dm.AccountBanList [i] = AccountBan;
            });
            MockAccountBanRepository.Setup(m => m.Delete(It.IsAny<AccountBan>())).Callback((AccountBan AccountBan) =>
            {
                dm.AccountBanList.Remove(AccountBan);
            });
            MockAccountBanRepository.Setup(m => m.Add(It.IsAny<AccountBan>())).Callback((AccountBan AccountBan) =>
            {
                dm.AccountBanList.Add(AccountBan);
            });
        }
    }
}
