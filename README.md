Projekt zaimplementowano w ramach kursu studenckiego. Projekt wykonywało 4-rech studentów, w którym każdy członek zespołu wykonywał każdą z częśći (implementacja logiki, widoku, podpinanie widoku do kontrolera, wykonywanie testów, itp.).

Projekt wykonano w technologi ASP.NET MVC 4, HTML(Razor), css( bootstrap), javascript, jquery. Dane przechowywane były w bazie danych MS SQL Server 2014. Do komunikacji z bazą danych został wykorzystany ORM Entity Framework. Jakość implementacji została zwiększona dzięki napisaniu testów jednostkowych oraz funkcjonalnych.
Praca odbywała się w czteroosobowym zespole w metodyce SCRUM.
Serwis ten umożliwia:
- Wyszukiwanie połączeń komunikacyjnych
- Przesyłanie wiadomości prywatnych w celu odnajdowania innych użytkowników 
- Dodawanie, komentowanie postów na ogólnodostępnej (dla zalogowanych użytkowników) tablicy, a także dodawanie "like" lub "not like" do danego postu lub komentarza
- Przeglądanie rozkładów jazdy
- Zarządzanie/Aktualizacja danych (wraz z kontami) przez administratora poprzez serwis
- Zarządzanie kontem przez właściciela konta w serwisie