﻿using System.Collections.Generic;
using System.Web.Mvc;
using SIDUKM.Domain;

namespace SIDUKM.WebUI.Models.Administration
{
    public class EditAccountViewModel
    {
        [HiddenInput(DisplayValue = false)]
        public int AccountId { get; set; }
        public List<UserRole> UserRole { get; set; }
        public int UserRoleId { get; set; }
        public string ReturnLink { get; set; }
    }
}