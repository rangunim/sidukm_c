﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIDUKM.WebUI.Models.Administration
{
    public class BanViewModel
    {
        public int AccountId { get; set; }

        [Required(ErrorMessage = "Należy podać początkową datę!")]
        [DisplayName("Początkowa data bana")]
        [RegularExpression(@"\d\d\d\d\-\d\d\-\d\d", ErrorMessage = "Prawidłowy format daty: YYYY-MM-DD")]
        public string StartDate { get; set; }

        [Required(ErrorMessage = "Należy podać końcową datę!")]
        [DisplayName("Końcowa data bana")]
        [RegularExpression(@"\d\d\d\d\-\d\d\-\d\d", ErrorMessage = "Prawidłowy format daty: YYYY-MM-DD")]
        public string StopDate { get; set; }

        public string ReturnLink { get; set; }
        public string Username { get; set; }
    }
}