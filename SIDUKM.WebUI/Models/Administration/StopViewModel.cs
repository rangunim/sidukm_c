﻿using SIDUKM.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIDUKM.WebUI.Models.Administration
{
    public class StopViewModel
    {
        [Required(ErrorMessage="To pole jest wymagane!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [Range(0, int.MaxValue, ErrorMessage = "Pole musi zawierać liczbę!")]
        public int Number { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [Range(0, float.MaxValue, ErrorMessage = "Pole musi zawierać liczbę!")]
        public float CoordinateX { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [Range(0, float.MaxValue, ErrorMessage = "Pole musi zawierać liczbę!")]
        public float CoordinateY { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane!")]
        public int CityID { get; set; }
        public List<City> City { get; set; }
    }
}