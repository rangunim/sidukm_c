﻿using SIDUKM.Domain;
using System.Collections.Generic;

namespace SIDUKM.WebUI.Models.Administration
{
    public class AddTimetableViewModel
    {
        public int LineNumberID { get;set;}
        //public List<LineNumber> LineNuber{get;}
        public int StopID { get; set; }
        //public List<Stop> Stop{get;}
        //[RegularExpression(@"([0-5][0-9][A-Za-z\*]?\s?){0,}",ErrorMessage=@"Błąd! Prawidłowy wpis jednej danej: [0-5][0-9][A-Za-z\*]")]
        public List<string> TimetableNormal { get; set; }
        //[RegularExpression(@"([0-5][0-9][A-Za-z\*]?\s?){0,}", ErrorMessage = @"Błąd! Prawidłowy wpis jednej danej: [0-5][0-9][A-Za-z\*]")]
        public List<string> TimetableSaturday { get; set; }
        //[RegularExpression(@"([0-5][0-9][A-Za-z\*]?\s?){0,}", ErrorMessage = @"Błąd! Prawidłowy wpis jednej danej: [0-5][0-9][A-Za-z\*]")]
        public List<string> TimetableSunday { get; set; }
        public List<Stop> Stops { get; set; }
        public List<Line> Lines { get; set; }
    }
}