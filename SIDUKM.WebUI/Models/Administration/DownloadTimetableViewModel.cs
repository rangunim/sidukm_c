﻿
using System;
using System.Collections.Generic;

namespace SIDUKM.WebUI.Models.Administration
{
    public class DownloadTimetableViewModel
    {
         public List<string> Lines { get; set; }
         public List<bool> ChoosedStop { get; set; }
         public DateTime LastUpdatedInAllLines { get; set; }
         public List<string> LastUpdated{ get; set; }
    }
}
