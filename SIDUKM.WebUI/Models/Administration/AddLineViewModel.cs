﻿using SIDUKM.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIDUKM.WebUI.Models.Administration
{
    public class AddLineViewModel
    {
        [Required(ErrorMessage = "To pole jest wymagane!")]
        public string Name { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane!")]
        public string Direction { get; set; }
        //public int LineTypeID { get; set; }
        //public List<LineType> LineType { get; set; }
        [Required(ErrorMessage = "To pole jest wymagane!")]
        public int CityID { get; set; }
        //public List<string> Route { get; set; }
        public List<City> City { get; set; }
        /*public List<string> Stops { get; set; }
        public List<int> StopsID{get;set;}*/
    }
}