﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SIDUKM.Domain;
using SIDUKM.WebUI.Infrastructure;

namespace SIDUKM.WebUI.Models.Account
{
    public class RegisterViewModel
    {

        [Display(Name = "Login")]
        [Required(ErrorMessage="To pole jest wymagane!")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "Login musi się składać z  minimum 4 znaków i nie może być dłuższe niż 30 znaków.")]
        public string Login { get; set; }
        
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Hasło musi się składać z  minimum 6 znaków w tym jednej cyfry oraz jednej litery i nie może być dłuższe niż 30 znaków.")]
        [RegularExpression(@"(([0-9]).*([a-zA-z]).*)|(([a-zA-z]).*([0-9]).*)", ErrorMessage = "Hasło musi się składać z  minimum 6 znaków w tym jednej cyfry oraz jednej litery. i nie może być dłuższe niż 30 znaków")]      
        public string Password { get; set; }

        [Display(Name = "Powtórz hasło")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "To pole jest wymagane!")]     
        [Compare("Password",ErrorMessage="Hasła nie są zgodne")]
        public string RepeatPassword { get; set; }

        [EmailAddress]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [StringLength(30, MinimumLength = 4, ErrorMessage = "Maksymalna dlugość maila nie może być dłuższa niż 30 znaków.")]
        
        public string Email { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public List<Gender> Gender { get; set; }

        public int GenderId { get; set; }
        
        public List<City> City { get; set; }
        
        public int CityId { get; set; }

       
        //[Range(typeof(bool),"true","true", ErrorMessage="Aby zarejestrować się w serwisie, musisz zaakcpetować regulamin.")]
        [IsTrue(ErrorMessage = "Aby zarejestrować się w serwisie, musisz zaakcpetować regulamin.")]
        public bool IsAcceptRule { get; set; }
        
        public Boolean IsAcceptMarketing { get; set; }
    }
}