﻿using SIDUKM.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SIDUKM.WebUI.Models.Account
{
    public class EditAccountViewModel
    {
        [System.Web.Mvc.HiddenInput(DisplayValue = false)]
        public int AccountId { get; set; }
        [EmailAddress]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        //[Required(ErrorMessage = "To pole jest wymagane!")]
        public string Email { get; set; }

        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Nowe hasło")]
        [DataType(DataType.Password)]
        [RegularExpression(@"(([0-9]).*([a-zA-z]).*)|(([a-zA-z]).*([0-9]).*)", ErrorMessage = "Hasło musi się składać z  minimum 6 znaków w tym jednej cyfry oraz jednej litery. i nie może być dłuższe niż 30 znaków")]
        public string NewPassword { get; set; }

        [Display(Name = "Powtórz hasło")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Hasła nie są zgodne")]
        public string RepeatPassword { get; set; }

        

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public List<Gender> Gender { get; set; }

        public short? GenderId { get; set; }

        public List<City> City { get; set; }

        public int CityId { get; set; }

        public List<UserRole> UserRole { get; set; }
        public int UserRoleId { get; set; }

        public string PreviousPage { get; set; }
      /*  public byte [] Avatar { get; set;}

       // [HiddenInput(DisplayValue=false)]
        public string MimeType { get; set;}
        */
      
    }
}