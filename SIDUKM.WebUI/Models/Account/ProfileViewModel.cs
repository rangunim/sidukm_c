﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIDUKM.WebUI.Models.Account
{
    public class ProfileViewModel
    {
        public string AccountLogin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string UserRole { get; set; }
        public int AccountId { get; set; }
    }
}