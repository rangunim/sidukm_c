﻿using System.ComponentModel.DataAnnotations;

namespace SIDUKM.WebUI.Models.Account
{
    public class ForgotPasswordViewModel
    {
        [EmailAddress(ErrorMessage = "Błędny adres e-mail!")]
        [Display(Name = "Email")]
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}