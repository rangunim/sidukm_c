﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SIDUKM.WebUI.Models.Account
{
    public class ResetPasswordViewModel
    {
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Hasło musi się składać z  minimum 6 znaków w tym jednej cyfry oraz jednej litery i nie może być dłuższe niż 30 znaków.")]
        [RegularExpression(@"(([0-9]).*([a-zA-z]).*)|(([a-zA-z]).*([0-9]).*)", ErrorMessage = "Hasło musi się składać z  minimum 6 znaków w tym jednej cyfry oraz jednej litery. i nie może być dłuższe niż 30 znaków")] 
        public String Password { get; set; }
       
        [Display(Name = "Powtórz hasło")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [Compare("Password", ErrorMessage = "Hasła są różne!")] // errormessage nie działa - błąd asp.net
        public String RepeatPassword { get; set; }
    } 
}