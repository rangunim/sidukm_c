﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIDUKM.WebUI.Models.Message
{
    public class SendViewModel
    {
        public List<Tuple<int, string>> AccountList { get; set; }

        [Required(ErrorMessage = "Należy podać odbiorcę wiadomości!")]
        [DisplayName("Do:")]
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public int AnonymousSourceId { get; set; }

        [Required(ErrorMessage = "Wiadomość musi posiadać temat!")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Wiadomość musi zawierać treść!")]
        public string Content { get; set; }
        public string ReturnLink { get; set; }
    }
}