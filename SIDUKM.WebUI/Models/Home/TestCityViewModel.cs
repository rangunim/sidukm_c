﻿using System.Collections.Generic;
using SIDUKM.Domain;

namespace SIDUKM.WebUI.Models.Home
{
    public class TestCityViewModel
    {
        public List<City> Cities { get; set; }
        public string City { get; set; }
    }
}