﻿using SIDUKM.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIDUKM.WebUI.Models.Transport
{
    public class DirectionsViewModel
    {
        public List<Line> DirectionsName { get; set; }
        public string LineName { get; set; }
    }
}