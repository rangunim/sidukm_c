﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SIDUKM.Domain;

namespace SIDUKM.WebUI.Models.Transport
{
    public class FindRouteViewModel
    {
        [Required(ErrorMessage = "To pole jest wymagane!")]
        [Range(0,int.MaxValue,ErrorMessage = "Musisz wpisać prawidłową nazwę przystanku!")]
        public int Start { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane!")]
        [Range(0, int.MaxValue, ErrorMessage = "Musisz wpisać prawidłową nazwę przystanku!")]
        public int Stop { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane!")]
        [RegularExpression(@"\d?\d:\d\d", ErrorMessage = "Prawidłowy format czasu: HH:MM")]
        public string Time { get; set; }

        [Required(ErrorMessage = "To pole jest wymagane!")]
        [RegularExpression(@"\d\d\.\d\d\.\d\d", ErrorMessage = "Prawidłowy format daty: DD.MM.YY")]
        public string Date { get; set; }

        public List<Stop> Stops { get; set; } 
    }
}