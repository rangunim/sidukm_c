﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SIDUKM.WebUI.Models.Spotted
{
    public class NewPostViewModel
    {
        public int TopicId { get; set; }
        public string Content { get; set; }
    }
}