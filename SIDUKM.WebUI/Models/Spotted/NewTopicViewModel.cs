﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SIDUKM.Domain;

namespace SIDUKM.WebUI.Models.Spotted
{
    public class NewTopicViewModel
    {
        [Display(Name = "Id tematu")]
        public int TopicId { get; set; }
        [Required(ErrorMessage = "Wybierz typ!")]
        [Display(Name="Typ tematu")]
        public int CategoryId { get; set; }

        [Display(Name = "Linia")]
        public int? LineId { get; set; }

        [Display(Name = "Temat anonimowy")]
        public bool IsAnonymous { get; set; }
        
        [Required(ErrorMessage = "Pole wiadomości jest wymagane!")]
        [DataType(DataType.MultilineText)]
        [MaxLength(1024, ErrorMessage = "Wiadomość może mieć maksymalnie 1024 znaki!")]
        public string Message { get; set; }

        public List<Line> Line { get; set; }
        public List<TopicCategory> Category { get; set; }
    }
}