﻿$(document).ready(function () {
    // rejestracje na dzień
    $.getJSON('/Stats/GetRegistrationsPerDay', function (data) {
        $('#registrationsPerDay').html('');
        new Morris.Area({
            element: 'registrationsPerDay',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Liczba rejestracji']
        });
    });
    // rejestracje na dzień kumulowane
    $.getJSON('/Stats/GetRegistrationsPerDay?acc=1', function (data) {
        $('#registrationsPerDayAccumulated').html('');
        new Morris.Area({
            element: 'registrationsPerDayAccumulated',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Łącznie kont']
        });
    });
    // topici na dzień
    $.getJSON('/Stats/GetTopicsPerDay', function (data) {
        $('#topicsPerDay').html('');
        new Morris.Area({
            element: 'topicsPerDay',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Liczba tematów']
        });
    });
    // topici na dzień kumulowane
    $.getJSON('/Stats/GetTopicsPerDay?acc=1', function (data) {
        $('#topicsPerDayAccumulated').html('');
        new Morris.Area({
            element: 'topicsPerDayAccumulated',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Łącznie tematów']
        });
    });
    // posty na dzień
    $.getJSON('/Stats/GetPostsPerDay', function (data) {
        $('#postsPerDay').html('');
        new Morris.Area({
            element: 'postsPerDay',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Liczba postów']
        });
    });
    // posty na dzień kumulowane
    $.getJSON('/Stats/GetPostsPerDay?acc=1', function (data) {
        $('#postsPerDayAccumulated').html('');
        new Morris.Area({
            element: 'postsPerDayAccumulated',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Łącznie postów']
        });
    });
    // najczęściej komentowany
    $.getJSON('/Stats/GetMostCommented', function(data) {
        $('#mostCommented p').html(data.Message);
        $('#mostCommented footer').html(data.Author + ' <cite>' + data.Date + '</cite>');
        $('#mostCommentedCount').html(data.Count);
    });
    // najlepszy temat
    $.getJSON('/Stats/GetTopicVotes?type=1', function (data) {
        $('#highestTopic p').html(data.Message);
        $('#highestTopic footer').html(data.Author + ' <cite>' + data.Date + '</cite>');
        $('#highestTopicCount').html(data.Count);
    });
    // najgorszy temat
    $.getJSON('/Stats/GetTopicVotes?type=-1', function (data) {
        $('#lowestTopic p').html(data.Message);
        $('#lowestTopic footer').html(data.Author + ' <cite>' + data.Date + '</cite>');
        $('#lowestTopicCount').html(data.Count);
    });
    // najlepszy post
    $.getJSON('/Stats/GetPostVotes?type=1', function (data) {
        $('#highestPost p').html(data.Message);
        $('#highestPost footer').html(data.Author + ' <cite>' + data.Date + '</cite>');
        $('#highestPostCount').html(data.Count);
    });
    // najgorszy post
    $.getJSON('/Stats/GetPostVotes?type=-1', function (data) {
        $('#lowestPost p').html(data.Message);
        $('#lowestPost footer').html(data.Author + ' <cite>' + data.Date + '</cite>');
        $('#lowestPostCount').html(data.Count);
    });
    // najbardziej aktywny topicowiec
    $.getJSON('/Stats/GetActivity?posts=0', function(data) {
        $('#activeTopics').html('');
        Morris.Bar({
            element: 'activeTopics',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Tematy']
        });
    });
    // najbardziej aktywny poster
    $.getJSON('/Stats/GetActivity?posts=1', function (data) {
        $('#activePosts').html('');
        Morris.Bar({
            element: 'activePosts',
            data: data,
            xkey: 'Key',
            ykeys: ['Value'],
            labels: ['Posty']
        });
    });
});