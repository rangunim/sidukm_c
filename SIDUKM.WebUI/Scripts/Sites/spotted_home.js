﻿// pobranie wiadomości
getTopicLink = '/Spotted/GetTopics?count=';

// sprawdzenie ile wiadomości pobrać
checkPostLink = '/Spotted/GetNewTopicsCount?firstId=';

// link do pobierania avataru
getAvatarLink = '/Account/GetAvatar?id=';

// najwyżej znajdująca się wiadomość
firstId = 0;

// załadowanie spotted
$(document).ready(function () {
    // poprawa tooltipów
    $("a").tooltip({
        'selector': '',
        'placement': 'top',
        'container': 'body'
    });
    // pobranie tematów
    $.getJSON(getTopicLink + '10', function (data) {
        $('#spotted-bar').html('');
        firstId = data[0].TopicId;
        addTopics(data);
    });
});

// odświeżenie spotted co 5 sekund
window.setInterval(function() {
    $.get(checkPostLink + firstId, function(data) {
        var content = parseInt(data);
        if (content > 0) {
            $.getJSON(getTopicLink + content, function (dataTopics) {
                firstId = dataTopics[0].TopicId;
                console.log(content);
                addTopics(dataTopics);
                deleteLast(content);
            });
        }
    });
}, 5000);

// usuwanie n ostatnich wiadomości
function deleteLast(count) {
    for (var i = 0; i < count; i++) {
        $('#spotted-bar').children().last().remove();
    }
}

// dodanie tematów
function addTopics(data) {
    if (data.length > 0) {
        var temp = $('<div></div>');
        $.each(data, function (id, value) {
            $(generateTopic(value)).appendTo(temp);
        });
        temp.children().prependTo('#spotted-bar');
    } else {
        $('#spotted-bar').html('<p>Brak wiadomości!</p>');
    }
}

// wygenerowanie htmla z tematem
function generateTopic(data) {
    var content = $('#spotted-source').clone();
    return generateMessage(content, data);
}

// uzupełnienie htmla
function generateMessage(content, data) {
    var avatar = getAvatarLink + '0';
    var profile = '#';
    if (!data.IsAnonymous) {
        avatar = getAvatarLink + data.AccountId;
        profile = '/Account/UserProfile/' + data.AccountId;
    }
    content.find('.spotted-topic').attr('data-topic', data.TopicId);
    content.find('.thumb span').html('<img src="' + avatar + '" width="20" height="20" class="img-circle">');
    if (data.Message.length > 120) {
        content.find('.details p').html('<muted>' + data.DateCreated + '</muted><br />' +
            '<a href="' + profile + '">' + data.AccountName + '</a>&nbsp;' + data.Message.slice(0, 120) +
            '<a href="#" data-toggle="tooltip" title="' + data.Message + '">...</a>');
    } else {
        content.find('.details p').html('<muted>' + data.DateCreated + '</muted><br />' +
            '<a href="' + profile + '">' + data.AccountName + '</a>&nbsp;' + data.Message);
    }
    return content.html();
}

// przekierowanie na spotted
$(document).on('click', '.spotted-topic', function (evt) {
    //evt.preventDefault();
    evt.stopPropagation();
    var topic = $(this).data('topic');
    window.location.assign("/Spotted/#topic" + topic);
});