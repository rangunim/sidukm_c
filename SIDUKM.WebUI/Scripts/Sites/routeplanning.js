﻿// initialize components
$(document).ready(function () {
    $('#Date').val(getDate());
    $('#Start').combobox();
    $('#Stop').combobox();
    $('#Date').datepicker({
        format: "dd.mm.yy",
        todayBtn: "linked",
        language: "pl"
    });
    $('#Time').timepicker({
        minuteStep: 1,
        showMeridian: false
    });
});

// get current date in dd.mm.yy
function getDate() {
    var date = new Date();
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yy = date.getFullYear() % 100;
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    if (yy < 10) {
        yy = '0' + yy;
    }
    return dd + '.' + mm + '.' + yy;
}

// resize iframe to remaining documents width and height
function resize(iframe) {
    var iframePos = $(iframe).position();
    var height = document.body.scrollHeight - iframePos.top - 200;
    var del = 0;
    if (parseInt($('#sidebar').css('margin-left').replace("px", "")) == 0) {
        del = 50;
    }
    var width = document.body.scrollWidth - iframePos.left - del;

    $(iframe).height(height);
    $(iframe).width(width);
}

// resize iframe on window resize
$(window).resize(function () {
    resize($('#jdframe'));
});

// action for button click
// TODO możliwość zmiany miasta
$('#submit').click(function() {
    var form = $('form');
    form.validate();
    if (form.valid()) {
        var start = $('#Start').val();
        var startname = $('#Start option[value=\'' + start + '\']').text();
        var stop = $('#Stop').val();
        var stopname = $('#Stop option[value=\'' + stop + '\']').text();
        var date = $('#Date').val();
        var time = $('#Time').val();
        var jsonlink = "./GetCoords/?start=" + start + "&stop=" + stop;
        var coordstart = "";
        var coordstop = "";
        $.getJSON(jsonlink, function(data) {
            coordstart = data.start;
            coordstop = data.stop;
            var link = "fc=" + coordstart + "&fn=" + startname + "&tc=" + coordstop + "&tn=" + stopname + "&h=" + time + "&d=" + date + "&as=true";
            var iframe = $('#jdframe');
            resize(iframe);
            iframe.css('visibility', 'visible');
            iframe.attr('src', 'http://wroclaw.jakdojade.pl/?' + link);
        }).fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Błąd: " + err);
        });    
    }
});