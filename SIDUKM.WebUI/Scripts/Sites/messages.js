﻿$(document).ready(function() {
    if (siteId === 0) {
        $('#inbox-tab').addClass('active');
        $('#inbox-link').attr('href', '#');
    } else if (siteId === 1) {
        $('#sent-tab').addClass('active');
        $('#sent-link').attr('href', '#');
    } else {
        $('#trash-tab').addClass('active');
        $('#trash-link').attr('href', '#');
    }
    var table = initDatatable();
    $('#messageList').hide();
    $('#messageList').show();
    /*table.fnSetColumnVis(5, false);
    table.fnSetColumnVis(6, false);*/
    /*table.column(5).visible(false);
    table.column(6).visible(false);*/
    table.on('processing.dt', function(e, settings, processing) {
       if (!processing) {
           table.column(5).visible(false);
           table.column(6).visible(false);
       }
    });
});

function getSubjectAndId(data, type, dataToSet) {
    return [data.MessageId, data.Subject, data.IsReaded];
}

function getAccountAndId(data, type, dataToSet) {
    return [data.AccountId, data.Account];
}

function deleteMessage(data) {
    
    var d = confirm("Czy przenieść wiadomość do kosza?");
    if (d) {
        location.reload();
        $.get("Delete", $.param({ 'id': data }, true), function () {
            });
    }
}

function deleteTrashMessage(data) {
    var d = confirm("Czy na pewno chcesz nieodwracalnie usunąć tą wiadomość?");
    if (d) {
        location.reload();
        $.get("DeleteFromTrash", $.param({ 'id': data }, true), function () {
        });
    }
}

function initDatatable() {
    var table = $("#tableContract").DataTable(
        {
            "bServerSide": true,
            "bProcessing": true,
            //stateSave: true,
            "scrollCollapse": true,
            "sAjaxSource": siteId === 0 ? "/Message/GetInbox" : (siteId === 1 ? "/Message/GetSent" : "/Message/GetTrash"),
            "sServerMethod": "POST",
            "aoColumns":
            [
                {
                    mData: getSubjectAndId,
                    sTitle: "Temat",
                    "bSearchable": false,
                    "width": "100%",
                    "bSearchtable": true,
                    "render": function (data, type, full, meta) {
                        var link = "<a href=\"/Message/View/" + data[0] + "\">" + data[1] + "</a>";
                        return data[2] ? link : "<b>" + link + "</b>";
                    }
                },
                {
                    mData: getAccountAndId,
                    sTitle: siteId === 0 ? "Od" : "Do",
                    "bSearchable": false,
                    "render": function(data, type, full, meta) {
                        return "<a href=\"/Account/UserProfile?id=" + data[0] + "\">" + data[1] + "</a>";
                    }
                },
                { mData: "Date", sTitle: "Data" },
                {
                    "bSortable": false,
                    "bSearchable": false,
                    "sClass": "center",
                    sTitle: "",
                    mData: "MessageId",
                    "render": function (data, type, full, meta) {
                        var replyLink = siteId === 0 ? "<a href=\"/Message/Send?original=" + data + "\">Odpowiedz</a>" :
                            siteId === 2 ? "<a href=\"javascript:void(0)\" onclick=\"deleteTrashMessage(" + data + ");\">Usuń</a>" : "";
                        return replyLink;
                    }
                },
                {
                    "bSortable": false,
                    "bSearchable": false,
                    "sClass": "center",
                    sTitle: "",
                    mData: "MessageId",
                    "render": function (data, type, full, meta) {
                        var deleteLink = siteId === 2 ? "<a href=\"/Message/Undelete/" + data + "\">Przywróć</a>" :
                            "<a href=\"javascript:void(0)\" onclick=\"deleteMessage(" + data + ");\">Usuń</a>";
                        return deleteLink;
                    }
                },
                {
                    mData: "Account",
                    "bVisible": false
                },
                {
                    mData: "Subject",
                    "bVisible": false
                },
            ],

            "language":
            {
                "sProcessing": "<p class=\"text-center\"><img src=\"../Content/img/loader-pacman.gif\"><br />Przetwarzanie...</p>",
                "sLengthMenu": "Pokaż _MENU_ pozycji",
                "sZeroRecords": "Nie znaleziono pasujących pozycji",
                "sInfoThousands": " ",
                "sInfo": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                "sInfoEmpty": "Pozycji 0 z 0 dostępnych",
                "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                "sInfoPostFix": "",
                "sSearch": "Szukaj:",
                "sLoadingRecords": "<p class=\"text-center\"><img src=\"../Content/img/loader-pacman.gif\"><br />Trwa ładowanie...</p>",
                "sUrl": "",
                "oPaginate":
                    {
                        "sFirst": "Pierwsza",
                        "sPrevious": "Poprzednia",
                        "sNext": "Następna",
                        "sLast": "Ostatnia"
                    }
            },
            "order": [[2, "desc"]],
        });
    return table;

}