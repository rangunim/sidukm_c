﻿// link do pobierania tematów
getTopicLink = '/Spotted/GetTopics';

// link do pobierania postów
getPostLink = '/Spotted/GetPosts';

// link do sprawdzenia czy są nowe posty
checkPostLink = '/Spotted/AreNewTopics';

// link do pobierania avataru
getAvatarLink = '/Account/GetAvatar?id=';

// link do wysyłania wiadomości
sendMessageLink = '/Message/Send?topic=';
sendMessageLinkUser = '/Message/Send?user=';

// link do edycji tematu
editTopicLink = '/Spotted/EditTopic?topic=';

// link do edycji postu
editPostLink = '/Spotted/EditPost?post=';

// linki do głosowania na posty
postUpvoteLink = '/Spotted/PostVote?value=1&postId=';
postDownvoteLink = '/Spotted/PostVote?value=-1&postId=';
getPostVotesLink = '/Spotted/GetPostVotes?id=';

// linki do głosowania na tematy
topicUpvoteLink = '/Spotted/TopicVote?value=1&topicId=';
topicDownvoteLink = '/Spotted/TopicVote?value=-1&topicId=';
getTopicVotesLink = '/Spotted/GetTopicVotes?id=';

// lista moderatorów
moderators = [];

// obecny użytkownik
currentUser = 0;

// id ostatniego posta
lastId = 0;

// id pierwszego posta
firstId = 0;

// kategoria
categoryId = 0;

// akcje wywoływane po uruchomieniu strony
$(document).ready(function () {
    $('#CategoryId').combobox();
    $('#LineId').combobox();
    // pobranie listy moderatorów
    $.getJSON('/Spotted/GetModeratorsList', function(data) {
        moderators = data;
        // pobranie zalogowanego użytkownika
        $.get('/Spotted/CurrentUser', function (data) {
            currentUser = parseInt(data);
            // pobranie tematów
            $.getJSON(getTopicLink, function (data) {
                firstId = data[0].TopicId;
                addTopics(data);
                $('#loadMore').removeAttr('hidden');
                $('#filterCategory').removeAttr('hidden');
                $('#filterCategoryLabel').removeAttr('hidden');
                // TODO naprawić scrollowanie
                var anchor = window.location.hash.substring(1);
                var target = $('#' + anchor);
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            });
        });
    });  
});

//funkcja wywołana po kliknięciu usunięcia tematu
function deleteTopic(data) {
    var remove = confirm("Czy usunąć ten temat?");
    if (remove) {
        $.post("/Spotted/DeleteTopic", $.param({ 'id': data }, true), function () {
            
        });
        $('#topic' + data).addClass('hidden');
 
    }
}


// usuwanie komentarza do tematu
function deletePost(data, object) {
    var remove = confirm("Czy na pewno usunąć komentarz?");
    if (remove) {
        $.post("Spotted/DeletePost", $.param({ 'id': data }, true), function () {
            
        });
        var value = $('#post' + data).parent().parent().parent().find(".comments").html() - 1;
        $('#post' + data).parent().parent().parent().find(".comments").html(value);

        
       $('#post' + data).addClass('hidden');
    }
}
$('#loadMore').click(function() {
    //GetTopics(int lastId = 0, int categoryId = 0, int count = 10, int cityId = 1)
    $('#loadMore').addClass('hidden');
    $('#loading').removeClass('hidden');
    var link = getTopicLink + '?lastId=' + lastId + '&categoryId=' + categoryId;
    $.getJSON(link, function (data) {
        if (data.length === 0) {
            $('#giveMeNoMore').removeClass('hidden');
            $('#loadMore').attr('disabled', 'disabled');
            $('#loading').addClass('hidden');
           
        } else {
            addTopics(data);
            $('#loadMore').removeClass('hidden');
        }
        $("#filterCategory").show();
        $("#filterCategoryLabel").show();
    });
});

//wybranie elememntu z listy kategorii (filter)
$("#filterCategory").on('change', function (evt) {
    /* var selectedText = $(this).find(':selected').text(); // zwroci: Towarzyskie,itd.    
     var $formPanelElements = $("div.form-panel");
     var posts = $("#spottedContent").find($formPanelElements); //[] pobiera wszystkie elementy z spottedcontent posiadajce <div class ="form-panel">
     if (selectedText == 'Wszystkie') {
         posts.show();
     }
     else {
         posts.hide();
         posts.each(function (index) {
             //console.log(index + ": " + $(this));
             var topicPosts = $(this).find('h4');
             if (topicPosts.text().indexOf(selectedText) >= 0) {
                 $(this).show();
             }
         });
     }
     $('#loadMore').removeClass('hidden');*/
    $("#filterCategory").hide();
    $("#filterCategoryLabel").hide();
    $('#loadMore').removeAttr('disabled');
    $('#giveMeNoMore').addClass('hidden');
    var selected = $(this).val();
    categoryId = selected;
    lastId = 0;
    $('#spottedContent').empty();
    $('#loadMore').click();
});

// kliknięcie rozwinięcia komentarzy
$(document).on('click', '.comment-link', function (evt) {
    evt.preventDefault();
    var topic = $(this).data('post');
    var topicanswers = $(this).parent().parent().parent().parent().parent().find('.topic-answers'); // todo jakoś ładniej ;d ?
    var answerslist = topicanswers.find('.answers-list');
    if (topicanswers.hasClass('hidden')) {
        // pobranie listy wiadomości
        var link = getPostLink + '?postId=' + topic;
        topicanswers.removeClass('hidden');
        topicanswers.find('.loading').removeClass('hidden');
        $.getJSON(link, function(data) {
            addPosts(data, answerslist);
            
            topicanswers.find('.loading').addClass('hidden');

        });
    } else {
        // schowanie listy
        topicanswers.addClass('hidden');
        answerslist.html('');
    }
});

// kliknięcie głosowania na post +1
$(document).on('click', '.post-upvote-link', function (evt) {
    evt.preventDefault();
    var id = $(this).attr('href');
    var parent = $(this).parent().parent();
    if (currentUser === 0) {
        window.alert("Zaloguj się, żeby móc zagłosować!");
    } else {
        if (id != '') {
            var link = postUpvoteLink + id;
            $.get(link, function (data) {
                $.getJSON(getPostVotesLink + id, function (data) {
                    refreshVotes(data, parent);
                });
            });
        }
    }
});

// kliknięcie głosowania na post -1
$(document).on('click', '.post-downvote-link', function (evt) {
    evt.preventDefault();
    var id = $(this).attr('href');
    var parent = $(this).parent().parent();
    if (currentUser === 0) {
        window.alert("Zaloguj się, żeby móc zagłosować!");
    } else {
        if (id != '') {
            var link = postDownvoteLink + id;
            $.get(link, function (data) {
                $.getJSON(getPostVotesLink + id, function (data) {
                    refreshVotes(data, parent);
                });
            });
        }
    }
});

// kliknięcie głosowania na temat +1
$(document).on('click', '.topic-upvote-link', function (evt) {
    evt.preventDefault();
    var id = $(this).attr('href');
    var parent = $(this).parent().parent();
    if (currentUser === 0) {
        window.alert("Zaloguj się, żeby móc zagłosować!");
    } else {
        if (id != '') {
            var link = topicUpvoteLink + id;
            $.get(link, function (data) {
                $.getJSON(getTopicVotesLink + id, function (data) {
                    refreshVotes(data, parent);
                });
            });
        }
    }
});

// kliknięcie głosowania na temat -1
$(document).on('click', '.topic-downvote-link', function (evt) {
    evt.preventDefault();
    var id = $(this).attr('href');
    var parent = $(this).parent().parent();
    if (currentUser === 0) {
        window.alert("Zaloguj się, żeby móc zagłosować!");
    } else {
        if (id != '') {
            var link = topicDownvoteLink + id;
            $.get(link, function (data) {
                $.getJSON(getTopicVotesLink + id, function (data) {
                    refreshVotes(data, parent);
                });
            });
        }
    }
});

// wysyłanie odpowiedzi
$(document).on('keydown', '.post-form-textarea', function (evt) {
    if (evt.keyCode == 13) {
        var handler = $(this);
        var value = $(this).val();
        var topicid = $(this).data('post');
        var toSend = {
            "TopicId": topicid,
            "Content": value
        };
        handler.attr('disabled', true);
        $.ajax({
            url: '/Spotted/AddPost',
            type: 'POST',
            data: JSON.stringify(toSend),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            error: function (data) {
                console.log("error: " + data);
                handler.removeAttr('disabled');
            },
            success: function (data) {
                if (data.success) {
                    console.log("udało się:" + data);
                    handler.val("");
                    handler.parent().parent().parent().parent().find('.comment-link').click();
                    handler.parent().parent().parent().parent().find('.comment-link').click();
                } else {
                    console.log("error: " + data);
                }
                handler.removeAttr('disabled');
            }
        })
    }
});


// aktualizacja głosów
function refreshVotes(data, where) {
    where.find('.downvotes').html(data.down);
    console.log(data.down);
    console.log(where.find('.downvotes'));
    where.find('.upvotes').html("+"+data.up);
    console.log(data.up);
    console.log(where.find('.upvotes'));
}

// wrzucenie tematów na stronę
function addTopics(data) {
    if (data.length > 0) {
        $.each(data, function (id, value) {
            $(generateTopic(value)).appendTo('#spottedContent');          
        });
        $('#loading').addClass('hidden');
        lastId = data[data.length - 1].TopicId;
    } else {
        $('#spottedContent').html('<p>Brak wiadomości!</p>');
    }
    $('#loading').addClass('hidden');
}

// wrzucenie postów do tematu
function addPosts(data, target) {
    if (data.length > 0) {
        $.each(data, function (id, value) {
            $(generatePost(value)).appendTo(target);
        });
        //alert(data.length);
        var value = target.parent().parent().parent().find(".comments").html(data.length);
    } else {
        target.html('<p class="text-center">Brak odpowiedzi!</p>');
    }
}

// wygenerowanie htmla z tematem
function generateTopic(data) {
    var content = $('#topic-source').clone();
    return generateMessage(content, data, false);
}

// wygenerowanie htmla z postem
function generatePost(data) {
    var content = $('#post-source').clone();
    return generateMessage(content, data, true);
}

// wygenerowanie treści htmla z wiadomością
function generateMessage(content, data, isPost) {
    // wpisanie treści
    content.find('.message-content').html(data.Message); 
    // wpisanie daty i czy edytowany
    if (data.IsEdited) {
        content.find('.message-date').html(data.DateCreated + ' (edytowany)');
    } else {
        content.find('.message-date').html(data.DateCreated);
    }
    // wpisanie głosów +
    content.find('.upvotes').html('+' + data.Upvotes);
    // wpisanie głosów -
    if (data.Downvotes !== 0) {
        content.find('.downvotes').html(data.Downvotes);
    } else {
        content.find('.downvotes').html('-0');
    } 
    // wpisanie nazwy użytkownika
    content.find('.user-nick').html('<b>' + data.AccountName + '</b>' + getGenderIcon(data.AccountGender));
    //To sobie zmieniono, ale chyba nie w tym miejscu
    //content.find('.topicPanel').attr('id', 'topic' + data.TopicId);
    //content.find('.post-panel').attr('id', 'post' + data.PostId);
    if (!isPost) {
        // wpisanie kategorii i linii
        if (data.Line !== '') {
            content.find('.topic-header').html(data.TopicCategory + ', ' + data.Line);
        } else {
            content.find('.topic-header').html(data.TopicCategory);
        }
        // wpisanie ilości komentarzy
        content.find('.comments').html(data.Comments);
        content.find('.comment-link').attr('data-post', data.TopicId);
        content.find('.post-form-textarea').attr('data-post', data.TopicId);
        content.find('.topicPanel').attr('id', 'topic' + data.TopicId);
        //content.find('.comment-link').removeAttr('href');
        // wpisanie avataru
        if (data.IsAnonymous) {
            content.find('.user-avatar').attr('src', getAvatarLink + '0');
            content.find('.user-profile').addClass('hidden');
            
        } else {
            content.find('.user-avatar').attr('src', getAvatarLink + data.AccountId);
            // wpisanie linka do obejrzenia profilu
            content.find('.user-profile').attr('href', '/Account/UserProfile?id=' + data.AccountId);
            
        }
    } else {
        // wpisanie anchora
        content.find('user-header').attr('id', data.postId);
        // wpisanie avataru
        content.find('.user-avatar').attr('src', getAvatarLink + data.AccountId);
        content.find('.user-profile').attr('href', '/Account/UserProfile?id=' + data.AccountId);
        content.find('.post-panel').attr('id', 'post' + data.PostId);
    }
    if (isPost) {
        if (currentUser > 0) {
            content.find('.post-upvote-link').attr('href', data.PostId);
            content.find('.post-downvote-link').attr('href', data.PostId);
        }
        content.find('.user-message').attr('href', sendMessageLinkUser + data.AccountId);
    } else {
        if (currentUser > 0) {
            //console.log(data.TopicId);
            content.find('.topic-upvote-link').attr('href', data.TopicId);
            content.find('.topic-downvote-link').attr('href', data.TopicId);
        }
        content.find('.user-message').attr('href', sendMessageLink + data.TopicId);
    } 
    // sprawdzenie czy użytkownik jest moderatorem lub autorem tematu
    if (currentUser === data.AccountId || moderators.indexOf(currentUser) >= 0) {
        // TODO wpisanie linka do edycji
        if (isPost) {
            // TODO !!!!!!!!!!!!!! EDIT POST, NIE TOPIC !!!!!!!!!!!!!!!!!
            content.find('.user-edit').attr('href', editPostLink + data.PostId);
        } else {
            content.find('.user-edit').attr('href', editTopicLink + data.TopicId);//tu odpowiedz
        }
        // TODO wpisanie linka do kasowania
        if (isPost) {
            content.find('.user-delete').attr('onclick', 'deletePost(' + data.PostId + ')');
            content.find('.user-delete').attr('href', 'javascript:void(0)');
        } else {
            content.find('.user-delete').attr('onclick', 'deleteTopic(' + data.TopicId + ')');
            content.find('.user-delete').attr('href', 'javascript:void(0)');
        }     
    } else {
        content.find('.user-admin').addClass('hidden');
    }
    return content.html();
}

// zwrócenie kodu html z ikoną płci
function getGenderIcon(gender) {
    switch(gender) {
        case 0:
            return '';
        case 2:
            return '&nbsp;<i class="fa fa-mars" style="color: blue;"></i>';
        case 1:
            return '&nbsp;<i class="fa fa-venus" style="color: hotpink;"></i>';
    }
    return '';
}