﻿var table;

$.urlParam = function (name) // save to varilble parameters from URL. $.urlParam('view');
{ 
    var results = new RegExp("[\?&]" + name + "=([^&#]*)").exec(window.location.href);
    return results[1] || 0;
}

MVCDataTable =
{    
    init: function ()
    {
        this.initDataTable()
    },

    initDataTable: function ()
    {
        if ($.urlParam('view') != "ArchiveView") {
            table = $("#tableContract").DataTable(
            {

                "bServerSide": true,
                "bProcessing": true,
                "stateSave": false,
                "scrollCollapse": true,
                "sAjaxSource": "/Administration/UserListData",
                "sServerMethod": "POST",
                "bAutoWidth": false,
                //"bLengthChange": false,
                "aoColumns":
                [
                    { mData: "Id", sTitle: "Id" , "sWidth": "5%"},
                    { mData: "Login", sTitle: "Login", "sWidth": "10%",},
                    { mData: "Email", sTitle: "Email", "sWidth": "10%" },
                    { mData: "FirstName", sTitle: "Imię", "sWidth": "10%" },
                    { mData: "LastName", sTitle: "Nazwisko", "sWidth": "10%" },
                    { mData: "CityName", sTitle: "Miasto", "sWidth": "10%" },
                    { mData: "GenderName", sTitle: "Płeć", "sWidth": "10%" },
                    { mData: "UserRoleName", sTitle: "Typ", "sWidth": "10%" },
                    { mData: "DateCreated", sTitle: "Utworzono", "sWidth": "10%" },
                    { mData: "Additional", sTitle: "Dodatkowe", "sWidth": "10%" },
                    {
                        "sWidth": "5%",
                        "bSortable": false,
                        "bSearchable": false,
                        "sClass": "center",
                        sTitle: "Akcje",
                        /*mData: "Id", "render": function (data, type, full, meta)
                        {
                            return "<a class=\"btn btn-xs btn-info\" href=\"EditAccount?id=" + data + "\">Edytuj </a>" +
                                    "    " +
                                "<a href=\"#\" id=\"btnRemove\" class=\"btn btn-xs btn-info\" " +
                                "onclick=\"openModal(" + data + ")\">Usuń</a>";
                        }*/
                        mData: getIdAndAdditional,
                        "render": function (data, type, full, meta) {
                            var isBanned = data[1] == "zbanowany";
                            var editLink = "<a class=\"btn btn-xs btn-info\" href=\"EditAccount?id=" + data[0] + "\">Edytuj </a>";
                            var deleteLink = "<a href=\"#\" id=\"btnRemove\" class=\"btn btn-xs btn-info\" " +
                                "onclick=\"removeAccount(" + data[0] + ")\">Usuń</a>";
                            var sendLink = "<a href=\"/Message/Send/?user=" + data[0] + "\" class=\"btn btn-xs btn-info\"" +
                                ">Wiadomość</a>";
                            var banLink = isBanned ? "<a href=\"#\" id=\"btnRemove\" class=\"btn btn-xs btn-info\" " +
                                "onclick=\"unban(" + data[0] + ")\">Odbanuj</a>" :
                                "<a class=\"btn btn-xs btn-info\" href=\"Ban?id=" + data[0] + "\">Banuj</a>";

                            return editLink + " " + banLink + " " + sendLink + " " + deleteLink;
                        }
                    }
                ],

                "language":
                {
                    "sProcessing": "<p class=\"text-center\"><img src=\"../Content/img/loader-pacman.gif\"><br />Przetwarzanie...</p>",
                    "sLengthMenu": "Pokaż _MENU_ pozycji",
                    "sZeroRecords": "Nie znaleziono pasujących pozycji",
                    "sInfoThousands": " ",
                    "sInfo": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                    "sInfoEmpty": "Pozycji 0 z 0 dostępnych",
                    "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                    "sInfoPostFix": "",
                    "sSearch": "Szukaj:",
                    "sLoadingRecords": "<p class=\"text-center\"><img src=\"../Content/img/loader-pacman.gif\"><br />Trwa ładowanie</p>",
                    "sUrl": "",
                    "oPaginate":
                        {
                            "sFirst": "Pierwsza",
                            "sPrevious": "Poprzednia",
                            "sNext": "Następna",
                            "sLast": "Ostatnia"
                        }
                }
            });
        }
        else
        {
            table = $("#tableContract").DataTable(
            {

                "bServerSide": true,
                "bProcessing": true,
                "stateSave": false,
                "scrollCollapse": true,
                "sAjaxSource": "/Administration/UserListData",
                "sServerMethod": "POST",
                "bAutoWidth": false,
                //"bLengthChange": false,
                "aoColumns":
                [
                    { mData: "Id", sTitle: "Id", "sWidth": "5%" },
                    { mData: "Login", sTitle: "Login", "sWidth": "10%" },
                    { mData: "Email", sTitle: "Email", "sWidth": "10%" },
                    { mData: "FirstName", sTitle: "Imię", "sWidth": "10%" },
                    { mData: "LastName", sTitle: "Nazwisko", "sWidth": "10%" },
                    { mData: "CityName", sTitle: "Miasto", "sWidth": "10%" },
                    { mData: "GenderName", sTitle: "Płeć", "sWidth": "10%" },
                    { mData: "UserRoleName", sTitle: "Typ", "sWidth": "10%" },
                    { mData: "DateCreated", sTitle: "Utworzono", "sWidth": "10%" },
        
                ],

                "language":
                {
                    "sProcessing": "<p class=\"text-center\"><img src=\"../Content/img/loader-pacman.gif\"><br />Przetwarzanie...</p>",
                    "sLengthMenu": "Pokaż _MENU_ pozycji",
                    "sZeroRecords": "Nie znaleziono pasujących pozycji",
                    "sInfoThousands": " ",
                    "sInfo": "Pozycje od _START_ do _END_ z _TOTAL_ łącznie",
                    "sInfoEmpty": "Pozycji 0 z 0 dostępnych",
                    "sInfoFiltered": "(filtrowanie spośród _MAX_ dostępnych pozycji)",
                    "sInfoPostFix": "",
                    "sSearch": "Szukaj:",
                    "sLoadingRecords": "<p class=\"text-center\"><img src=\"../Content/img/loader-pacman.gif\"><br />Trwa ładowanie</p>",
                    "sUrl": "",
                    "oPaginate":
                        {
                            "sFirst": "Pierwsza",
                            "sPrevious": "Poprzednia",
                            "sNext": "Następna",
                            "sLast": "Ostatnia"
                        }
                }
            });
        }

      /*      MVCDataTable.returnDataTable = function ()
            {
                return table;
            }*/
        },

    
};

$(function ()
{
    MVCDataTable.init();
});

function openModal(id) {
    var link = "./DeleteAccount/?id=" + id;
    var user_link = "./GetAccountName/?id=" + id;
    $.get(user_link, function(data) {
        $('#accountName').text(data);
    });
    $('#accountLink').attr('href',link);
    $('#myModal').modal('show');
}

function getIdAndAdditional(data, type, dataToSet) {
    return [data.Id, data.Additional];
}

function removeAccount(id) {
    var link = "./DeleteAccount/?id=" + id;
    var user_link = "./GetAccountName/?id=" + id;
    $.get(user_link, function (data) {
        var d = confirm("Czy na pewno chcesz skasować konto " + data + "?");
        if (d) {
            window.location.assign(link);
        }
    });  
}

function unban(id) {
    var link = "./Unban/" + id;
    var user_link = "./GetAccountName/?id=" + id;
    $.get(user_link, function (data) {
        var d = confirm("Czy na pewno chcesz odbanować konto " + data + "?");
        if (d) {
            window.location.assign(link);
        }
    });
}



/*  when we use client-side then use below code
   /*  "ajax": 
                    {
                        "url": "/Administration/UserListData?view="+ $.urlParam('view'),
                        "type": "POST",
                    },*/

/*     "columns":
     [
         { "data": "Id" },
         { "data": "Login" },
         { "data": "Email" },
         { "data": "FirstName" },
         { "data": "LastName" },
         { "data": "CityName" },
         { "data": "GenderName" },
         { "data": "UserRoleName" },
         { "data": "Additional" },
         {
             "bSortable": false,
             "bSearchable": false,
             "data": "Id", "render": function (data, type, full, meta)
             {
                 /*return '<a class="btn btn-xs btn-info" href="EditAccount?id=' + data + '">Edytuj </a>'
                     + '   '
                     + '<a class="btn btn-xs btn-info" href="UserList?id=' + data + '">Usun </a>';*/



/*         return '<a class="btn btn-xs btn-info" href="EditAccount?id=' + data + '">Edytuj </a>' +
          "    " +
         '<a href="#" id="btnRemove" class="btn btn-xs btn-info" ' +
              'onclick="openModal(' + data + ')">Usuń</a>';
         
     },
 }

],*/