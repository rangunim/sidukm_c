﻿using System.Web.Optimization;

namespace SIDUKM.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region jquery/bootstrap/core
            /*bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));*/

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            /*bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));*/


            bundles.Add(new StyleBundle("~/Content/dashgum").Include(
                    "~/Content-dashgum/css/bootstrap.css",
                    "~/Content/bootstrap.min.css",
                    "~/Content/Site.css",
                    "~/Content-dashgum/font-awesome/css/font-awesome.css",
                    "~/Content-dashgum/css/style.css",
                    "~/Content-dashgum/css/style-responsive.css"                                       
                    ));

            bundles.Add(new ScriptBundle("~/bundles/jquery-bootstrap").Include(   
                "~/Scripts/jquery-2.1.3.js",
                    "~/Content-dashgum/js/bootstrap.min.js",
                    "~/Content-dashgum/js/jquery-ui-1.9.2.custom.min.js",
                    "~/Content-dashgum/js/jquery.ui.youch-punch.min.js",
                    "~/Content-dashgum/js/jquery.dcjqaccordion.2.7.js",
                    "~/Content-dashgum/js/jquery.scrollTo.min.js",
                   // "~/Content-dashgum/js/jquery.nicescroll.js",
                    "~/Content-dashgum/js/common-scripts.js",
                    "~/Scripts/DropDownPanel.js",            
                    "~/Scripts/jquery-additional.js"
                    ));

             bundles.Add(new ScriptBundle("~/bundles/jquery-dataTable2").Include(
                 
                 //"~/Scripts/DataTables/jquery.dataTables.js",
                 "~/Scripts/DataTables/jquery.dataTables.min.js",
                 "~/Scripts/DataTables/dataTables.bootstrap.js",
                 "~/Scripts/DataTables/dataTables.responsive.min.js",
                 "~/Scripts/DataTables/MVCDataTable.js"
                 ));

             bundles.Add(new ScriptBundle("~/bundles/jquery-dataTable").Include(

                  //"~/Scripts/DataTables/jquery.dataTables.js",
                  "~/Scripts/DataTables/jquery.dataTables.min.js",
                  "~/Scripts/DataTables/dataTables.bootstrap.js",
                  "~/Scripts/DataTables/dataTables.responsive.min.js"
                  ));

            /*bundles.Add(new StyleBundle("~/Content/css-dataTable").IncludeDirectory(
                "~/Content/DataTables/css", "*.css", true
                ));*/
            bundles.Add(new StyleBundle("~/Content/css-dataTable").Include(
                "~/Content/DataTables/css/dataTables.bootstrap.css",
                "~/Content/DataTables/css/dataTables.responsive.css"
                ));

            bundles.Add(new ScriptBundle("~/bundles/validation").Include(
                "~/Scripts/jquery.unobtrusive-ajax.min.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/jquery.fixes.js"
                ));
            bundles.Add(new StyleBundle("~/Content/timepicker").Include(
                "~/Content/bootstrap-timepicker.min.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/timepicker").Include(
                "~/Scripts/bootstrap-timepicker.min.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/combobox").Include(
                "~/Scripts/bootstrap-combobox.js"
                ));
            bundles.Add(new StyleBundle("~/Content/combobox").Include(
                "~/Content/bootstrap-combobox.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                "~/Scripts/bootstrap-datepicker.min.js",
                "~/Scripts/bootstrap-datepicker.pl.min.js"
                ));
            bundles.Add(new StyleBundle("~/Content/datepicker").Include(
                "~/Content/bootstrap-datepicker3.min.css"
                ));
            #endregion
            #region our scripts/css
            bundles.Add(new StyleBundle("~/Content/signin").Include(
                        "~/Content/signin.css"));

            bundles.Add(new StyleBundle("~/Content/DropDownPanel").Include(
                        "~/Content/DropDownPanel.css"
                        ));

            bundles.Add(new StyleBundle("~/Content/RSS").Include(
                        "~/Content/RSS.css"
              ));
            bundles.Add(new ScriptBundle("~/bundles/route").Include(
                        "~/Scripts/Sites/routeplanning.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/avatar").Include(
                        "~/Scripts/Sites/avatar.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/spotted").Include(
                        "~/Scripts/Sites/spotted.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/messages").Include(
                        "~/Scripts/Sites/messages.js"
                        )); 
            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                        "~/Scripts/Sites/spotted_home.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/stats").Include(
                        "~/Scripts/Sites/stats.js"
                        ));
            #endregion
        }
    }
}
