﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SIDUKM.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            routes.MapRoute("EditAccount", "{controller}/{action}/{id}", new { controller = "Administration", action = "EditAccount", id = UrlParameter.Optional });
            routes.MapRoute("DownloadTimetable", "{controller}/{action}/{id}", new { controller = "Administration", action = "DownloadTimetable", id = UrlParameter.Optional });
            routes.MapRoute("UpdateCoordinates", "{controller}/{action}/{id}", new { controller = "Administration", action = "DownloadTimetable", id = UrlParameter.Optional });
          
        }
    }
}
