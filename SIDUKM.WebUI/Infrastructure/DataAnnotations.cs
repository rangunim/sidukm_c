﻿using System.ComponentModel.DataAnnotations;

namespace SIDUKM.WebUI.Infrastructure
{
    public class IsTrueAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool)value;
        }
    }
}