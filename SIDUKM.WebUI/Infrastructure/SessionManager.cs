﻿using System;
using System.Web;

namespace SIDUKM.WebUI.Infrastructure
{
    public class SessionManager :ISessionManager
    {
        public void Clear()
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
            }
        }

        public void Put(string key, object value)
        {
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session.Remove(key);
                HttpContext.Current.Session.Add(key, value);
            }
        }

        public object Get(string key)
        {
            return HttpContext.Current != null && HttpContext.Current.Session != null
                ? HttpContext.Current.Session[key]
                : null;
        }

        public int? GetInt(string key)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                object value = HttpContext.Current.Session[key];
                if (value != null)
                {
                    return Convert.ToInt32(value);
                }
            }
            return null;
        }
    }
}