﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using Ninject.Modules;
using SIDUKM.Logic;
using SIDUKM.Logic.Repository;

namespace SIDUKM.WebUI.Infrastructure
{

    public class NinjectResolver : IDependencyResolver
    {
        public IKernel Kernel { get; private set; }
        public NinjectResolver(params NinjectModule[] modules)
        {
            Kernel = new StandardKernel(modules);
        }

        public object GetService(Type serviceType)
        {
            return Kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return Kernel.GetAll(serviceType);
        }
    }


    public class NinjectModules
    {
        //Return Lists of Modules in the Application
        public static NinjectModule[] Modules
        {
            get
            {
                //Return Modules you want to use for DI
                return new[] { new MainModule() };
            }
        }

        public class MainModule : NinjectModule
        {
            public override void Load()
            {
                Kernel.Bind<ISessionManager>().To<SessionManager>();
                Kernel.Bind<IAccountService>().To<AccountService>();
                Kernel.Bind<IMailService>().To<MailService>();
                Kernel.Bind<IRssService>().To<RssService>();
                Kernel.Bind<IBusService>().To<BusService>();
                Kernel.Bind<ISpottedService>().To<SpottedService>();
                Kernel.Bind<IMessageService>().To<MessageService>();
                Kernel.Bind<IStatService>().To<StatService>();
                Bind(typeof(IRepository<>)).To(typeof(EFRepository<>));
            }
        }
    }

    public class NinjectContainer
    {
        private static NinjectResolver _resolver;

        public static void RegisterModules(NinjectModule[] modules)
        {
            _resolver = new NinjectResolver(modules);
            DependencyResolver.SetResolver(_resolver);
        }

        //Manually Resolve Dependencies
        public static T Resolve<T>()
        {
            return _resolver.Kernel.Get<T>();
        }
    }

}