﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http.Filters;
using System.Web.Http.Controllers;

namespace SIDUKM.WebUI.Infrastructure
{
    public class BasicAuthorizeAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            var authHeader = filterContext.Request.Headers.Authorization;
            if (authHeader != null)
            {
                if (authHeader.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) &&
                    !string.IsNullOrWhiteSpace(authHeader.Parameter))
                {
                    try
                    {
                        var credentials = GetCredentials(authHeader);
                        var user = ConfigurationManager.AppSettings["EndPointUser"];
                        var pass = ConfigurationManager.AppSettings["EndPointPass"];
                        if (credentials.Length == 2 && credentials[0] == user && credentials[1] == pass)
                        {
                            return;
                        }
                    }
                    catch (Exception)
                    {
                        HandleUnauthorizedRequest(filterContext);
                    }
                }
            }
            HandleUnauthorizedRequest(filterContext);
        }

        protected void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
        }

        private string[] GetCredentials(AuthenticationHeaderValue authHeader)
        {
            var raw = authHeader.Parameter;
            var encoding = Encoding.ASCII;
            var credentials = encoding.GetString(Convert.FromBase64String(raw));
            return credentials.Split(':');
        }
    }
}