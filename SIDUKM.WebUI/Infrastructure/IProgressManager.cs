﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIDUKM.WebUI.Infrastructure
{
    public interface IProgressManager
    {
        void ClearAll();
        string LoadUpdateDataInDatabase(string id);
        string LoadUpdateCoordinates(string id);
        void Add(string id);
        void Remove(string id);
        int GetStatus(string id);
    }
}
