﻿using System;

namespace SIDUKM.WebUI.Infrastructure
{
    public interface ISessionManager
    {
        void Clear();
        void Put(String key, object value);
        object Get(String key);
        int? GetInt(String key);
    }
}
