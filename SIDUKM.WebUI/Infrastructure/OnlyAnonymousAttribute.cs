﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace SIDUKM.WebUI.Infrastructure
{
    public class OnlyAnonymousAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            // TODO doprowadzić do działania
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }
            var authHeader = filterContext.Request.Headers.Authorization;
            if (authHeader != null)
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }

        protected void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
        }
    }
}