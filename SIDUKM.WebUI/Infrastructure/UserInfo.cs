﻿namespace SIDUKM.WebUI.Infrastructure
{
    public class UserInfo
    {
        public string Login { get; set; }
        public int UserRoleId { get; set; }
    }
}