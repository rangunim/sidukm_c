﻿using SIDUKM.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Xml;

namespace SIDUKM.WebUI.Infrastructure
{
    public class ProgressManager : IProgressManager
    {
        private static object syncRoot = new object();
        private static IDictionary<string, int> ProcessStatus { get; set; }

        public List<Line> Lines { get; set; }
        public List<Stop> Stops { get; set; }
        public List<Route> Routes { get; set; }
        public List<Timetable> Timetables { get; set; }
        public List<string> ChoosedFiles { get; set; }
        public Tuple<List<Line>, List<Stop>, List<Timetable>> Tuple { get; set; }
       
        public ZipArchive Zip { get; set; }
        SIDUKM.Logic.IBusService BS { get; set; }

        public ProgressManager(SIDUKM.Logic.IBusService bs)
        {
            if(ProcessStatus == null)
            {
                ProcessStatus = new Dictionary<string, int>();
            }
            this.BS = bs;
        }

        public void ClearAll()
        {
            if(Lines != null && Stops != null && Timetables != null)
            {
                Lines.Clear();
                Stops.Clear();
                Timetables.Clear();
               // ProcessStatus.Clear();
            }
        }

        public string LoadUpdateDataInDatabase(string id)
        {
            List<Line> resultLine = new List<Line>();
            List<Stop> resultStop = new List<Stop>();
            List<Route> resultRoute = new List<Route>();
            Tuple<List<Line>, List<Stop>, List<Timetable>> tupleLocal = new Tuple<List<Line>, List<Stop>, List<Timetable>>(new List<Line>(), new List<Stop>(), new List<Timetable>());

            XmlReader xml;
            while(ChoosedFiles.Count != 0)
            {
                xml = BS.GetXmlFromZip(Zip, ChoosedFiles [0]);
                ChoosedFiles.RemoveAt(0);
                BS.UpdateDataFromXml(xml, ref resultLine, ref resultStop, ref tupleLocal, ref resultRoute);
            }
            xml = null;

            Lines = resultLine.Distinct(new LineComparer()).ToList();
            Stops = resultStop.Distinct(new StopComparer()).ToList();
            Routes = resultRoute.ToList();
            Tuple = tupleLocal;
            Timetables = tupleLocal.Item3.ToList();
            resultLine = null;
            resultStop = null;
            resultRoute = null;

            int countObjectToSave = Lines.Count + Stops.Count + Routes.Count + Timetables.Count;
            Line tempLine;
            Stop tempStop;
            Timetable tempTimetable;
            Route tempRoute;

            bool result = BS.AddLineAll(Lines.ToArray());
            if(!result)
            {
                while(Lines.Count > 0)
                {
                    tempLine = Lines.ElementAt(0);
                    BS.AddOrUpdateLine(tempLine);
                    Lines.Remove(tempLine);
                    Thread.Sleep(100);
                    SetProgress(id, ref countObjectToSave);
                }
            }
            Lines.Clear();
            SetProgress(id, ref countObjectToSave);

            result = BS.AddStopAll(Stops.ToArray());
            if(!result)
            {
                RepairStops(); //poprawia coordynaty z (0,0) na te co były.
                while(Stops.Count > 0)
                {
                    tempStop = Stops.ElementAt(0);
                    BS.AddOrUpdateStop(tempStop);
                    Stops.Remove(tempStop);
                    Thread.Sleep(100);
                    SetProgress(id, ref countObjectToSave);
                }
            }
            Stops.Clear();
            SetProgress(id, ref countObjectToSave);

            RepairTimetableAndRoute();
            result = BS.AddRouteAll(Routes.ToArray());
            if(!result)
            {
                while(Routes.Count > 0)
                {
                    tempRoute = Routes.ElementAt(0);
                    BS.AddOrUpdateRoute(tempRoute);
                    Routes.Remove(tempRoute);
                    Thread.Sleep(100);
                    SetProgress(id, ref countObjectToSave);
                }
            }
            Routes.Clear();
            SetProgress(id, ref countObjectToSave);


            result = BS.AddTimetableAll(Timetables.ToArray());
            if(!result)
            {
                while(Timetables.Count > 0)
                {
                    tempTimetable = Timetables.ElementAt(0);
                    BS.AddOrUpdateTimetable(tempTimetable);
                    Timetables.Remove(tempTimetable);
                    Thread.Sleep(100);
                    SetProgress(id, ref countObjectToSave);
                }
            }
            SetProgress(id, ref countObjectToSave);
            return id;
        }

        private void RepairStops()
        {
            List<Stop> stopsDB = BS.GetOryginalStops(1);
            Stop temp;
            for(int i = 0; i < Stops.Count; i++)
            {
                Stop stop = Stops.ElementAt(i);
                temp = stopsDB.Find(x => x.StopNumber.Equals(stop.StopNumber) && x.StopName.Equals(stop.StopName,StringComparison.OrdinalIgnoreCase) && x.CityId == stop.CityId);
                if(temp != null)
                {
                    Stops.ElementAt(i).CoordinateX = temp.CoordinateX;
                    Stops.ElementAt(i).CoordinateY = temp.CoordinateY;
                }
            }
        }

        private void RepairTimetableAndRoute()
        {
            List<Timetable> result = new List<Timetable>();
            List<Route> resultRoute = new List<Route>();
            List<Line> lines = Tuple.Item1;
            List<Stop> stops = Tuple.Item2;
            int count = Tuple.Item3.Count;
            Timetable timetable;
            Line line;
            Stop stop;
            Route route;

            List<Line> linesDB = BS.GetLines(1);
            List<Stop> stopsDB = BS.GetOryginalStops(1);

            for(int i = 0; i < count; i++)
            {
                timetable = Tuple.Item3.ElementAt(i);
                line = lines.ElementAt(i);
                stop = stops.ElementAt(i);
                timetable.LineId = (linesDB.Find(x => x.LineName.Contains(line.LineName + " kierunek: " + line.Direction) && x.Direction.Equals(line.Direction, StringComparison.OrdinalIgnoreCase) && x.CityId == line.CityId)).LineId;
                timetable.StopId = (stopsDB.Find(x => x.StopNumber.Equals(stop.StopNumber) && x.StopName.Equals(stop.StopName,StringComparison.OrdinalIgnoreCase) && x.CityId == stop.CityId)).StopId;
                Timetable t = new Timetable
                {
                    LineId = timetable.LineId,
                    StopId = timetable.StopId,
                    Table = timetable.Table,
                    Hash = timetable.Hash
                };

                if(!timetable.Table.Equals("<Timetable />"))
                { 
                        result.Add(t);
                }

                route = Routes.ElementAt(i);
                route.LineId = t.LineId;
                route.StopId = t.StopId;
                Route r = new Route
                {
                    LineId = route.LineId,
                    StopId = route.StopId,
                    Order = route.Order
                };
                resultRoute.Add(r);
            }
            Tuple = null;
            Timetables = result.Distinct(new TimetableComparer()).ToList();
            Routes = resultRoute.Distinct(new RouteComparer()).ToList();
        }

        private void SetProgress(string id, ref int countObjectToSave)
        {
            lock(syncRoot)
            {
                try
                {
                    ProcessStatus [id] = Convert.ToInt32(((countObjectToSave - (Lines.Count + Stops.Count + Timetables.Count + Routes.Count)) * 100) / countObjectToSave);
                }
                catch(DivideByZeroException)
                {
                    ProcessStatus [id] = 100;
                }
            }
        }

        public string LoadUpdateCoordinates(string id)
        {
            UpdateCoordinatesFromWeb("http://geoportal.wroclaw.pl/www/pliki/KomunikacjaZbiorowa/SlupkiWspolrzedne.txt", ref id);
            return id;
        }
        private bool UpdateCoordinatesFromWeb(string link, ref string id)
        {
            List<int> notFoundStopNumber = new List<int>();
            WebClient web = new WebClient();
            Stream stream = web.OpenRead(link);
            if(stream == null) return false;
            int counterObjectsToSave = CountObjectsToSave(stream);
            if(counterObjectsToSave == 0) return true;
            int countSavedObjects = 0;

            stream = web.OpenRead(link);
            if(stream == null) return false;
            List<Stop> stopDB = BS.GetOryginalStops(1);
            using(System.IO.StreamReader reader = new System.IO.StreamReader(stream))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    string [] split = line.Split(';');
                    //double coorX = Convert.ToDouble(split [0]);
                    // double coorY = Convert.ToDouble(split [1]);
                    int stopNumber = Convert.ToInt32(split [2]);
                    Stop s = stopDB.Find(x => x.StopNumber.Equals(stopNumber));
                    if(s != null)
                    {
                        s.CoordinateY = split [0].Replace(',', '.');
                        s.CoordinateX = split [1].Replace(',', '.');
                        BS.AddOrUpdateStop(s);
                        //System.Threading.Thread.Sleep(5000);
                    }
                    else
                    {
                        notFoundStopNumber.Add(stopNumber); //TODO wyswietlanie nr przystanku ktorego nie ma w bazie a ma wspolrzedne
                    }
                    countSavedObjects++;
                    lock(syncRoot)
                    {
                        ProcessStatus[id] = Convert.ToInt32((countSavedObjects * 100) / counterObjectsToSave);
                    }
                }
                reader.Close();
                stream.Close();
            }
            return true;
        }

        private int CountObjectsToSave(Stream stream)
        {
            int result = 0;
            using(System.IO.StreamReader reader = new System.IO.StreamReader(stream))
            {
                while(reader.ReadLine() != null)
                {
                    result++;
                }
                reader.Close();
                stream.Close();
            }
            return result;
        }

        public void Add(string id)
        {
            lock(syncRoot)
            {
                ProcessStatus.Add(id, 0);
            }
        }

        public void Remove(string id)
        {
            lock(syncRoot)
            {
                ProcessStatus.Remove(id);
            }
        }

        public int GetStatus(string id)
        {
            lock(syncRoot)
            {
                if(ProcessStatus.Keys.Count(x => x == id) == 1)
                {
                    return ProcessStatus [id];
                }
                else
                {
                    return 100;
                }
            }
        }

        //================COMPARATORS OF ENITIES============================================
        private class LineComparer : IEqualityComparer<Line>
        {
            public bool Equals(Line x, Line y)
            {
                return x.CityId == y.CityId && x.Direction.Equals(y.Direction, System.StringComparison.OrdinalIgnoreCase) && x.LineName.Equals(y.LineName, System.StringComparison.OrdinalIgnoreCase);
            }
            public int GetHashCode(Line obj)
            {
                return 31 * obj.CityId.GetHashCode() + obj.LineName.GetHashCode() + obj.Direction.GetHashCode();
            }
        }
        private class StopComparer : IEqualityComparer<Stop>
        {
            public bool Equals(Stop x, Stop y)
            {
                return x.CityId == y.CityId && x.StopNumber == y.StopNumber && x.StopName.Equals(y.StopName, System.StringComparison.OrdinalIgnoreCase);
            }
            public int GetHashCode(Stop obj)
            {
                return 31 * obj.CityId.GetHashCode() + obj.StopNumber.GetHashCode() + obj.StopName.GetHashCode();
            }
        }
        private class TimetableComparer : IEqualityComparer<Timetable>
        {
            public bool Equals(Timetable x, Timetable y)
            {
                return x.LineId.Equals(y.LineId) && x.StopId.Equals(y.StopId);
            }
            public int GetHashCode(Timetable obj)
            {
                return 31 * obj.LineId.GetHashCode() + obj.StopId.GetHashCode();
            }
        }
        private class RouteComparer : IEqualityComparer<Route>
        {
            public bool Equals(Route x, Route y)
            {
                return x.LineId.Equals(y.LineId) && x.StopId.Equals(y.StopId);
            }
            public int GetHashCode(Route obj)
            {
                return 31 * obj.LineId.GetHashCode() + obj.StopId.GetHashCode();
            }
        }

    }
}