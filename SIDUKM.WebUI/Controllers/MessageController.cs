﻿using System;
using System.Web.Mvc;
using SIDUKM.Domain;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;
using SIDUKM.WebUI.Models.Message;

namespace SIDUKM.WebUI.Controllers
{
    public class MessageController : AbstractController
    {

        private IMessageService MessageService;
        private ISpottedService SpottedService;

        public MessageController(ISessionManager sessionManager, IAccountService accountService, 
            IMailService mailService, IMessageService messageService, ISpottedService spottedService) 
            : base(sessionManager, accountService, mailService)
        {
            MessageService = messageService;
            SpottedService = spottedService;
        }

        public ActionResult Send(int? original, int? topic, int? user)
        {
            var model = new SendViewModel()
            {
                ReturnLink = Request.ServerVariables["HTTP_REFERER"]
            };
            if (user.HasValue)
            {
                model.AccountId = user.Value.ToString();
                model.AccountName = AccountService.GetAccountName(user.Value, true);
            }
            else if (original.HasValue)
            {
                var msg = MessageService.GetRawMessage(original.Value);
                if (msg.Recipient_AccountID != AccountService.GetAccountId(HttpContext.User.Identity.Name))
                {
                    return HttpNotFound();
                }
                model.AccountId = msg.Sender_AccountID.ToString();
                model.AccountName = AccountService.GetAccountName(msg.Sender_AccountID, true);
                model.Subject = string.Format("Re: {0}", msg.Subject);
                model.Content = string.Format("\n-- Oryginalna wiadomość: --\n{0}", msg.Content);
            }
            else if (topic.HasValue)
            {
                var source = SpottedService.GetRawTopic(topic.Value);
                model.AccountId = "-1";
                model.AnonymousSourceId = source.TopicId;
                model.AccountName = source.IsAnonymous
                    ? "<< anonimowy >>"
                    : AccountService.GetAccountName(source.AccountId, true);
            }
            else
            {
                model.AccountList = MessageService.GetAccountsList();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Send(SendViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            Message msg = new Message
            {
                Content = model.Content,
                Subject = model.Subject,
                Sender_AccountID = AccountService.GetAccountId(HttpContext.User.Identity.Name),
                IsAnonymousRecipient = model.AnonymousSourceId > 0,
                Recipient_AccountID =
                    model.AnonymousSourceId > 0
                        ? SpottedService.GetRawTopic(model.AnonymousSourceId).AccountId
                        : Int32.Parse(model.AccountId)
            };
            bool result = MessageService.SendMessage(msg);
            if (result) 
            {
                //return Content("Wysłano"); // TODO: redirect na listę wiadomości 
                return RedirectToAction("List", new {folder = 1, sent = 1});
            }
            ViewBag.Error = "Wystąpił niezidentyfikowany błąd. Przepraszamy!";
            if (model.AnonymousSourceId == 0 && model.AccountName != null)
            {
                model.AccountList = MessageService.GetAccountsList();
            }
            return View(model);
        }

        public ContentResult GetUnread()
        {
            return
                Content(
                    MessageService.GetUnreadCount(AccountService.GetAccountId(HttpContext.User.Identity.Name))
                        .ToString());
        }

        public ActionResult List(int? folder = 0, int? sent = 0)
        {
            ViewBag.Site = folder;
            ViewBag.Sent = sent.HasValue ? sent : 0;
            return View();
        }

        public JsonResult GetInbox()
        {
            var collection = MessageService.GetInbox(AccountService.GetAccountId(HttpContext.User.Identity.Name));
            var parser = new DataTablesParser.DataTablesParser<MessageData>(Request, collection);
            return Json(parser.Parse());
        }

        public JsonResult GetSent()
        {
            var collection = MessageService.GetSent(AccountService.GetAccountId(HttpContext.User.Identity.Name));
            var parser = new DataTablesParser.DataTablesParser<MessageData>(Request, collection);
            return Json(parser.Parse());
        }

        public JsonResult GetTrash()
        {
            var collection = MessageService.GetDeleted(AccountService.GetAccountId(HttpContext.User.Identity.Name));
            var parser = new DataTablesParser.DataTablesParser<MessageData>(Request, collection);
            return Json(parser.Parse());
        }

        public ActionResult View(int id)
        {
            if (id > 0)
            {
                int account = AccountService.GetAccountId(HttpContext.User.Identity.Name);
                var msg = MessageService.GetMessage(id, account);
                if (msg != null)
                {
                    ViewBag.link = Request.ServerVariables["HTTP_REFERER"];
                    //int account = AccountService.GetAccountId(HttpContext.User.Identity.Name);
                    if (msg.SenderId == account || msg.RecipientId == account)
                    {
                        if (msg.RecipientId == account)
                        {
                            MessageService.CheckAsRead(id);
                        }
                        return View(msg);
                    }
                }
            }
            return HttpNotFound();
        }

        public ActionResult Delete(int id)
        {
            var referer = Request.ServerVariables["HTTP_REFERER"];
            var result = MessageService.RemoveMessage(id, AccountService.GetAccountId(HttpContext.User.Identity.Name));
            return Redirect(referer);
        }

        public ActionResult Undelete(int id)
        {
            var referer = Request.ServerVariables["HTTP_REFERER"];
            var result = MessageService.UnremoveMessage(id, AccountService.GetAccountId(HttpContext.User.Identity.Name));
            return Redirect(referer);
        }

        public ActionResult DeleteFromTrash(int id)
        {
            var result = MessageService.RemoveFromTrash(id, AccountService.GetAccountId(HttpContext.User.Identity.Name));
            return RedirectToAction("List", new { folder = 2 });
        }
    }
}