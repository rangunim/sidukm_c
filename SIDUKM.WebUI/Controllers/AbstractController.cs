﻿using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Security;
using SIDUKM.Domain;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;

namespace SIDUKM.WebUI.Controllers
{
    public abstract class AbstractController : Controller
    {
        protected IAccountService AccountService;
        protected ISessionManager SessionManager;
        protected IMailService MailService;

        //public AbstractController() { }

        public AbstractController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService)
        {
            SessionManager = sessionManager;
            AccountService = accountService;
            MailService = mailService;
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (HttpContext.User != null)
            {
                string[] rolesArray = new string[1];
                UserRole role = AccountService.GetUserRole(HttpContext.User.Identity.Name);
                bool isBanned = AccountService.IsUserBanned(AccountService.GetAccountId(HttpContext.User.Identity.Name));
                if (role != null)
                {
                    rolesArray[0] = role.RoleName;
                    GenericPrincipal principal = new GenericPrincipal(HttpContext.User.Identity,rolesArray);
                    HttpContext.User = principal;
                }
                if (isBanned)
                {
                    FormsAuthentication.SignOut();
                }
            }
        }

        [ChildActionOnly]
        public string CurrentUserRole()
        {
            if (HttpContext.User != null)
            {
                UserRole role = AccountService.GetUserRole(HttpContext.User.Identity.Name);
                if (role != null)
                {
                    return role.RoleName;
                }
            }
            return "Gosc";
        }

        [ChildActionOnly]
        public string UserRole(string login)
        {
            UserRole role = AccountService.GetUserRole(login);
            return role != null ? role.RoleName : "Gosc";
        }

    }
}