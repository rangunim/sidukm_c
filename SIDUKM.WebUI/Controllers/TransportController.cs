﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Rotativa;
using Rotativa.Options;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;
using SIDUKM.WebUI.Models.Transport;

namespace SIDUKM.WebUI.Controllers
{
    public class TransportController : AbstractController
    {
        IBusService BusService { get; set; }

        public TransportController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService,
            IBusService busService) : 
            base(sessionManager, accountService, mailService)
        {
            BusService = busService;
        }

        [AllowAnonymous]
        public ActionResult FindRoute()
        {
            // TODO przerobić żeby nie korzystało z jakdojade
            // TODO obsługa wielu miast
            var model = new FindRouteViewModel { Stops = BusService.GetStopsDistinct(1) };
            return View(model);
        }

        [AllowAnonymous]
        public JsonResult GetCoords(int? start, int? stop)
        {
            if (start != null && stop != null)
                return Json(new {start = BusService.GetCoordinates(start.Value), stop = BusService.GetCoordinates(stop.Value)},
                    JsonRequestBehavior.AllowGet);
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult LinesList()
        {
            //var model = BusService.GetLines(1);
            LineViewModel model = new LineViewModel()
            {
                LineNames = BusService.GetLineNames(1)
            };

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult DirectionsList(string lineName)
        {
            ViewBag.Return = Request.ServerVariables["HTTP_REFERER"];
            DirectionsViewModel model = new DirectionsViewModel()
            {
                DirectionsName = BusService.GetDirectionByName(lineName),
                LineName = lineName
            };
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult StopList(int? id)
        {
            
            if (id.HasValue)
            {
                //ViewBag.Return = Request.ServerVariables["HTTP_REFERER"];
                var name = BusService.GetLineName(id.Value);
                ViewBag.Return = Url.Action("DirectionsList", new {lineName = name});
                //var model = BusService.GetStopsByLine(id.Value);
                var model = BusService.GetStopsOrder(id.Value);

                return View(model);
            }

            return RedirectToAction("LinesList");
        }

        [AllowAnonymous]
        public ActionResult Timetable(int? lineId, int? stopId)
        {
            if (lineId.HasValue && stopId.HasValue)
            {
                ViewBag.Return = Request.ServerVariables["HTTP_REFERER"];
                var lineName = BusService.GetLineNameWithStopName(lineId.Value, stopId.Value);
                var model =
                    new Tuple<Dictionary<string, List<string>>, string>(
                        BusService.GetTimetableParsed(lineId.Value, stopId.Value), lineName);
                ViewBag.lineId = lineId.Value;
                ViewBag.stopId = stopId.Value;
                return View(model);
            }

            return RedirectToAction("LinesList");
        }

        [AllowAnonymous]
        public ActionResult TimetableToPrint(int? lineId, int? stopId)
        {
            if (!lineId.HasValue || !stopId.HasValue)
            {
                return Content("<h1>Błąd!</h1>");
            }

            var lineName = BusService.GetLineNameWithStopName(lineId.Value, stopId.Value);
            var model =
                new Tuple<Dictionary<string, List<string>>, string>(
                    BusService.GetTimetableParsed(lineId.Value, stopId.Value), lineName);

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Print(int? lineId, int? stopId)
        {
            return new ActionAsPdf("TimetableToPrint", new {lineId, stopId})
            {
                FileName = "rozklad.pdf",
                PageOrientation = Orientation.Landscape
            };
        }

    }
}