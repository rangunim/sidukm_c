﻿using System.Linq;
using System.Web.Mvc;
using SIDUKM.Domain;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;
using SIDUKM.WebUI.Models.Administration;
using System.Globalization;
using System.Collections.Generic;
using System.IO.Compression;
using System;
using Newtonsoft.Json;

namespace SIDUKM.WebUI.Controllers
{
    public class AdministrationController : AbstractController
    {
        IBusService BusService { get; set; }
         delegate string ProcessTask(string id);
         ProgressManager Progress { get; set; }

        public AdministrationController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService, IBusService busService) :
            base(sessionManager, accountService, mailService)
        {
            BusService = busService;
            Progress = new ProgressManager(busService);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Menu()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult UserList(string view)// by zwrocil jakis widok.
        {
            switch(view)
            {
                case "DefaultView": ViewBag.TitleLocal = "Lista aktywnych użytkowników"; break;
                case "ArchiveView": ViewBag.TitleLocal = "Archiwum kont użytkowników"; break;
                case "GuidView": ViewBag.TitleLocal = "Konta użytkowników z aktywnym przypomnieniem hasła"; break;
                case "BannedView": ViewBag.TitleLocal = "Lista zbanowych użytkowników"; break;
                default: ViewBag.TitleLocal = "Lista aktywnych użytkowników"; break;
            }
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult UserListData()  // jquery datatble && json wypelnia tabelke
        {
            string selectedView = "DefaultView";
            if(Request.UrlReferrer != null && Request.UrlReferrer.Query != "")
            {
                selectedView = Request.UrlReferrer.Query.Substring("view".Length + 2);
            }
            //ViewBag.SelectedView = selectedView;
            var collection = AccountService.GetAccountsData(selectedView);
            var parser = new DataTablesParser.DataTablesParser<AccountData>(Request, collection);
            return Json(parser.Parse());//,"application/json; charset=utf-8", JsonRequestBehavior.AllowGet);
            /* JsonResult r =  new JsonResult() { JsonRequestBehavior = JsonRequestBehavior.DenyGet, Data = new { data = collection } };
                   return r; }*/
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult EditAccount(int id, string view)
        {
            Account acc = AccountService.GetAccountById(id, false);
            if (acc == null)
            {
                return View();
            }
            var model = new EditAccountViewModel()
            {
                ReturnLink = Request.ServerVariables["HTTP_REFERER"],
                AccountId = id,
                UserRole = AccountService.GetUserRoles().ToList(),
                UserRoleId = acc.UserRoleId ?? 1
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult EditAccount(EditAccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                /*Account account = AccountService.GetAccountById(model.AccountId); //tylko po to by wyciagnac login do result
                if(account == null)
                {
                    ViewBag.isEdited = 2;
                    return View(model);
                }*/
                /*Account result = new Account
                {
                    AccountId = acc.AccountId,
                    AccountLogin = account.AccountLogin,
                    AccountPassword = acc.NewPassword,
                    CityId = (short) acc.CityID,
                    Email = acc.Email,
                    FirstName = acc.Name,
                    GenderId = acc.SexID,
                    LastName = acc.SurName,
                    UserRoleId = (short) acc.UserRoleID
                };

                AccountAvatar newAccountAvatar = null;
                if(avatar != null)
                {
                    newAccountAvatar = new AccountAvatar
                    {
                        AccountId = acc.AccountId,
                        MimeType = avatar.ContentType,
                        Avatar = new byte [avatar.ContentLength]
                    };
                    avatar.InputStream.Read(newAccountAvatar.Avatar, 0, avatar.ContentLength);
                }

                acc.Sex = AccountService.GetGenders().ToList();
                acc.City = AccountService.GetCities().ToList();
                acc.UserRole = AccountService.GetUserRoles().ToList();

                bool isEdited = AccountService.EditAccount(result, newAccountAvatar);
                ViewBag.isEdited = isEdited ? 1 : 2;

                if(isEdited) return RedirectToAction("UserList");*/
                bool isEdited = AccountService.ChangeUserRole(model.AccountId, model.UserRoleId);
                ViewBag.isEdited = isEdited ? 1 : 2;
                if (isEdited)
                {
                    //return RedirectToAction("UserList");
                    return Redirect(model.ReturnLink);
                }
                return View(model);
            }
            /*acc.Sex = AccountService.GetGenders().ToList();
            acc.City = AccountService.GetCities().ToList();*/
            model.UserRole = AccountService.GetUserRoles().ToList();
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult DeleteAccount(int id)
        {
            var result = AccountService.DeleteAccount(id);
            return RedirectToAction("UserList");
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult DeleteAvatar(int id)
        {
            var result = AccountService.DeleteAvatar(id);
            return RedirectToAction("EditAccount");
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult GetAccountName(int id)
        {
            var result = AccountService.GetAccountById(id);
            return Content(result.AccountLogin);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AddStop()
        {
            var model = new SIDUKM.WebUI.Models.Administration.StopViewModel();
            model.City = AccountService.GetCities().ToList();
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddStop(StopViewModel model)
        {
            if(ModelState.IsValid)
            {
                var newStop = new Stop()
                {
                    CityId = (short) model.CityID,
                    CoordinateX = model.CoordinateX.ToString(CultureInfo.InvariantCulture),
                    CoordinateY = model.CoordinateY.ToString(CultureInfo.InvariantCulture),
                    StopName = model.Name,
                    StopNumber = model.Number
                };
                bool isAdded = BusService.AddOrUpdateStop(newStop);
                ViewBag.completed = isAdded;
                if(isAdded)
                {
                    model = new StopViewModel();
                    ModelState.Clear();
                }
                model.City = AccountService.GetCities().ToList();
                return View(model);
            }
            else
            {
                model.City = AccountService.GetCities().ToList();
                return View(model);
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AddLine()
        {
            var model = new SIDUKM.WebUI.Models.Administration.AddLineViewModel();
            model.City = AccountService.GetCities().ToList();
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddLine(AddLineViewModel model)
        {
            if(ModelState.IsValid)
            {
                var newLine = new Line()
                {
                    CityId = (short) model.CityID,
                    Direction = model.Direction,
                    LineName = model.Name
                };
                bool isSuccess = BusService.AddOrUpdateLine(newLine);
                ViewBag.comp = isSuccess;
                if(isSuccess)
                {
                    model = new AddLineViewModel();
                    ModelState.Clear();
                }
                model.City = AccountService.GetCities().ToList();
                return View(model);
            }
            else
            {
                model.City = AccountService.GetCities().ToList();
                return View(model);
            }
        }

       /* [Authorize(Roles = "Administrator")]
        public ActionResult AddTimetable()
        {
            AddTimetableViewModel model = new AddTimetableViewModel();
            // TODO podział w zależności od miasta
            model.Stops = BusService.GetStops(1).ToList();
            model.Lines = BusService.GetLines(1).ToList();
            return View(model);
        }*/

        [Authorize(Roles = "Administrator")]
        public ActionResult AddTimetable(int lineId=1, int stopId=2)
        {
            
            AddTimetableViewModel model = new AddTimetableViewModel()
            {
                //Stops = BusService.GetStops(1).ToList(),
                Lines = BusService.GetLines(1).ToList(),
                Stops = BusService.GetSortedStops(lineId),
                LineNumberID = lineId,
                StopID = stopId,          
                TimetableNormal = BusService.GetListTimetable(lineId, stopId, 0),
                TimetableSaturday = BusService.GetListTimetable(lineId, stopId, 1),
                TimetableSunday = BusService.GetListTimetable(lineId, stopId, 2)
            };
            if (stopId < 0)
            {
                model.StopID = model.Stops[0].StopId;
                model.TimetableNormal = BusService.GetListTimetable(lineId, model.StopID, 0);
                model.TimetableSaturday = BusService.GetListTimetable(lineId, model.StopID, 1);
                model.TimetableSunday = BusService.GetListTimetable(lineId, model.StopID, 2);
                ModelState.Clear();
            }
            
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        private List<string> SplitTable(List<string> timetable)
        {
            List<string> result = new List<string>(240);
            int i = 0;
            foreach(string line in timetable)
            {
                string [] lines = line.Split(new char [] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
                result.AddRange(lines.Select(x => i + ":" + x));
                i++;
            }
            return result;
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddTimetable(AddTimetableViewModel model)
        {
        
                if (ModelState.IsValid)
            {
                Dictionary<string, List<string>> timetable = new Dictionary<string, List<string>>();
                // TODO ręcznie definiowane nazwy kolumn
                    int lineId = model.LineNumberID;
                    int stopId = model.StopID;
                timetable.Add("Dni robocze", SplitTable(model.TimetableNormal));
                timetable.Add("Soboty", SplitTable(model.TimetableSaturday));
                timetable.Add("Niedziele", SplitTable(model.TimetableSunday));
                bool isSuccess = BusService.AddTimetable(model.StopID, model.LineNumberID, timetable);
                ViewBag.comp = isSuccess;
                    if (isSuccess)
                {
                    model = new AddTimetableViewModel();
                    ModelState.Clear();
                }
                
                model.Lines = BusService.GetLines(1).ToList();
                model.Stops = BusService.GetStops(1).ToList();
                //model.Stops = BusService.GetSortedStops(lineId);
                    model.TimetableNormal = BusService.GetListTimetable(lineId, stopId, 0);
                    model.TimetableSaturday = BusService.GetListTimetable(lineId, stopId, 1);
                    model.TimetableSunday = BusService.GetListTimetable(lineId, stopId, 2);
                return View(model);
            }
            else
            {
                model.Stops = BusService.GetStops(1).ToList();
                model.Lines = BusService.GetLines(1).ToList();
                    
                return View(model);
            }
            
            //return View(model);
            
        }

        public JsonResult GetTimetableJson(int lineId, int stopId, int timetableId)
        {
            List<string> list = BusService.GetListTimetable(lineId, stopId, timetableId);
            return Json(JsonConvert.SerializeObject(list));
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet]
        public ActionResult DownloadTimetable()
        {
            ZipArchive zip = BusService.GetZipArchiveFromWeb("http://www.um.wroc.pl/zdikzip/rozklady_xml.zip", ZipArchiveMode.Read);
            List<string> xmlFileNames = GetXmlNamesFromZip(zip);
            xmlFileNames = GetLinesNamesFromXmlFileNames(xmlFileNames);
            var model = new DownloadTimetableViewModel
            {
                Lines = xmlFileNames,
                LastUpdatedInAllLines =  BusService.GetLastUpdateDateInAllLines(),
                LastUpdated = GetLastUpdateLines(xmlFileNames)
            };       
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult DownloadTimetable(string id, List<bool> list)
        {
            ZipArchive zip = BusService.GetZipArchiveFromWeb("http://www.um.wroc.pl/zdikzip/rozklady_xml.zip", ZipArchiveMode.Read);
            DownloadTimetableViewModel model = new DownloadTimetableViewModel();
            model.ChoosedStop = list;
           
            if(zip != null)
            {
                model.Lines = GetLinesNamesFromXmlFileNames(GetXmlNamesFromZip(zip));
                model.LastUpdated = GetLastUpdateLines(model.Lines);
                List<string> xmlFilesName = GetXmlNamesFromZip(zip);
                List<string> choosedFiles = new List<string>();
                if(list != null)
                {
                    if (list.Count == 1 && list[0] == false)
                    {
                        List<bool> l = new List<bool>();
                        for (int i = 0; i < xmlFilesName.Count; i++)
                        {
                            l.Add(false);
                        }
                        model.ChoosedStop = l;
                        Progress.ClearAll();
                        UpdateCoordinates(id);
                    }
                    else
                    {
                        for (int i = 0; i < list.Count; i++)
                        {
                            if (list[i])
                            {
                                string temp = model.Lines[i].ToLower();
                                temp = String.Format("{0000}", temp);
                                temp = xmlFilesName.Find(x => x.Contains(temp));
                                if (temp != null)
                                {
                                    choosedFiles.Add(temp);
                                }
                            }
                        }
                        Progress.ClearAll();
                        Progress.ChoosedFiles = choosedFiles;
                        Progress.Zip = zip;
                        StartUpdateDataInDatabase(id);
                    }
                    
                }
               
                ViewBag.Downloaded = true;
                return View(model);
            }

            ViewBag.Downloaded = false;
            return View(model);
        }

        public void UpdateCoordinates(string id)
        {
            Progress.Add(id);
            ProcessTask processTask = new ProcessTask(Progress.LoadUpdateCoordinates);
            processTask.BeginInvoke(id, new AsyncCallback(EndUpdateDataInDatabase), processTask);

            //ZipArchive zip = BusService.GetZipArchiveFromWeb("http://www.um.wroc.pl/zdikzip/rozklady_xml.zip", ZipArchiveMode.Read);
            //DownloadTimetableViewModel model = new DownloadTimetableViewModel();
            //model.Lines = GetLinesNamesFromXmlFileNames(GetXmlNamesFromZip(zip));
            //return RedirectToAction("DownloadTimetable", new RouteValueDictionary(new {id=id, list=new List<bool>()}));
            //TODO NAPRAWIC by sie nie pzeladowywalla strona!!!
        }

        private List<string> GetLinesNamesFromXmlFileNames(List<string> xmlFileNames)
        {
            List<string> result = new List<string>();
            foreach(String s in xmlFileNames)
            {
                string t = s.TrimStart('0');
                t = t.Substring(0, t.LastIndexOf('.'));
                if(t.Length == 1)
                {
                    t = t.ToUpper();
                    if(t.Equals("P") || t.Equals("L")) //dla lini 0l i 0p
                    {
                        t = "0" + t;
                    }
                }
                result.Add(t);
            }
            return result;
        }
        private List<string> GetLastUpdateLines(List<string> lines)
        {
            List<string> result = new List<string>();
            string element = "";
            DateTime? date;
            for(int i = 0; i < lines.Count; i++)
            {
                element = " (";
                date = BusService.GetLastUpdate(lines.ElementAt(i), 1);
                if(date == null) element += " Nigdy)";
                else
                {
                    int days = (DateTime.Today - date.Value).Days;
                    if(days == 0) element += "Dziś)";
                    else element += days + " dni temu)";
                }
                result.Add(element);
            }
            return result;
        }

        private List<string> GetXmlNamesFromZip(ZipArchive zip)
        {
            List<string> xmlFilesName = new List<string>();
            foreach(ZipArchiveEntry entry in zip.Entries)
            {
                xmlFilesName.Add(entry.Name);
            }
            xmlFilesName.RemoveAt(xmlFilesName.Count - 1);
            xmlFilesName.RemoveAt(xmlFilesName.Count - 1);
            return xmlFilesName;
        }

        private void StartUpdateDataInDatabase(string id)
        {
            Progress.Add(id);
            ProcessTask processTask = new ProcessTask(Progress.LoadUpdateDataInDatabase);
            processTask.BeginInvoke(id, new AsyncCallback(EndUpdateDataInDatabase), processTask);
        }

        private void EndUpdateDataInDatabase(IAsyncResult result)
        {
            ProcessTask processTask = (ProcessTask) result.AsyncState;
            string id = processTask.EndInvoke(result);
            Progress.Remove(id);
            ViewBag.Downloaded = true;
        }

        public ContentResult GetCurrentProgress(string id)
        {
            this.ControllerContext.HttpContext.Response.AddHeader("cache-control", "no-cache");
            var currentProgress = Progress.GetStatus(id).ToString();
            return Content(currentProgress);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Ban(int id)
        {
            var existing = AccountService.GetBan(id);
            var model = new BanViewModel()
            {
                AccountId = id,
                StartDate =
                    existing != null
                        ? (existing.StartDate).Date.ToString("yyyy-MM-dd")
                        : (DateTime.Now).Date.ToString("yyyy-MM-dd"),
                StopDate =
                    existing != null
                        ? (existing.EndDate).Date.ToString("yyyy-MM-dd")
                        : (DateTime.Now).Date.ToString("yyyy-MM-dd"),
                Username = AccountService.GetAccountById(id, false).AccountLogin,
                ReturnLink = Request.ServerVariables["HTTP_REFERER"]
            };
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult Ban(BanViewModel model, int id)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Error = !AccountService.SetBan(model.AccountId, DateTime.Parse(model.StartDate), DateTime.Parse(model.StopDate));
            }
            return View(model);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Unban(int id)
        {
            var referer = Request.ServerVariables["HTTP_REFERER"];
            var result = AccountService.Unban(id);
            return Redirect(referer);
        }
    }
}
