﻿using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Security;
using SIDUKM.Domain;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;
using SIDUKM.WebUI.Models.Account;
using System;
using System.Web;
using System.Web.Helpers;

namespace SIDUKM.WebUI.Controllers
{
    public class AccountController : AbstractController
    {

        public AccountController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService) : 
            base(sessionManager, accountService, mailService) { }

        //
        // GET: /Account/
        /*public ActionResult Index()
        {
            return View();
        }*/

        public interface IAuthorizationFilter
        {
            void OnAuthorization(AuthorizationContext filterContext);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            if (HttpContext.Request.Cookies.AllKeys.Contains("Ban"))
            {
                var cookie = HttpContext.Request.Cookies.Get("Ban");
                if (cookie != null)
                {
                    int tmp = Convert.ToInt32(cookie.Values["date"]);
                    DateTime date = new DateTime(tmp / 10000, (tmp / 100) % 100, tmp % 100);
                    if (date > DateTime.Now)
                        return RedirectToAction("Banned", new { until = date });
                }
            }
            var model = new RegisterViewModel
            {
                Gender = AccountService.GetGenders().ToList(),
                City = AccountService.GetCities().ToList()
            };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            model.Gender = AccountService.GetGenders().ToList();
            model.City = AccountService.GetCities().ToList();
            if(ModelState.IsValid)
            {
                // tu akcja rejestrowania uzytkownika
                Account result = new Account
                {
                    AccountLogin = model.Login,
                    AccountPassword = model.Password,
                    CityId = (short)model.CityId,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    GenderId = (short)model.GenderId,
                    LastName = model.LastName,
                    UserRoleId = 3
                };

                bool isRegistered = AccountService.RegisterAccount(result);
                ViewBag.register = isRegistered ? 1 : 2;
                return View(model);
            }
            // tu co sie dzieje jak sie nie rejestruje go, zwroci razem z walidacja
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            if (HttpContext.Request.Cookies.AllKeys.Contains("Ban"))
            {
                var cookie = HttpContext.Request.Cookies.Get("Ban");
                if (cookie != null)
                {
                    int tmp = Convert.ToInt32(cookie.Values["date"]);
                    DateTime date = new DateTime(tmp/10000, (tmp/100)%100, tmp%100);
                    if (date > DateTime.Now)
                        return RedirectToAction("Banned", new { until = date });
                }
            }
            IPrincipal gp = HttpContext.User;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                var account = AccountService.GetAccountByUsername(model.Login, model.Password);
                ViewBag.Logged = account;
                ViewBag.isError = ViewBag.Logged == null;
                if (!ViewBag.isError)
                {
                    bool isBanned = AccountService.IsUserBanned(account.AccountId);
                    if (isBanned)
                    {
                        DateTime banEnd = AccountService.GetDateOfBanEnd(account.AccountId);
                        HttpCookie cookie = new HttpCookie("Ban");
                        cookie.Values.Add("login", account.AccountLogin);
                        cookie.Values.Add("id", account.AccountId.ToString());
                        cookie.Values.Add("date", String.Format("{0:yyyyMMdd}", banEnd));
                        cookie.Expires = banEnd;
                        System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
                        return RedirectToAction("Banned", new {until = banEnd});
                    }
                    if (model.IsAuto)
                    {
                        // użycie czasu 30 dni
                        var time = DateTime.Now.AddDays(30);
                        FormsAuthentication.Initialize();
                        var cookie = FormsAuthentication.GetAuthCookie(account.AccountLogin, model.IsAuto);
                        cookie.Expires = time;
                        var cookieValue = FormsAuthentication.Decrypt(cookie.Value);
                        var ticket = new FormsAuthenticationTicket(cookieValue.Version, cookieValue.Name,
                            cookieValue.IssueDate, time, true, cookieValue.UserData);
                        cookie.Value = FormsAuthentication.Encrypt(ticket);
                        System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        // użycie domyślnego czasu 30 minut
                        FormsAuthentication.SetAuthCookie(account.AccountLogin, model.IsAuto);    
                    }
                    UserInfo logged = new UserInfo()
                    {
                        Login = account.AccountLogin,
                        UserRoleId = (int) account.UserRoleId
                    };
                    SessionManager.Put("CurrentUser",logged);
                    if (Url.IsLocalUrl(ReturnUrl))
                    {
                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Banned(DateTime until)
        {
            return View(until);
        }

        public ActionResult Logout()
        {
            HttpContext.User = new GenericPrincipal(new GenericIdentity(""), null);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            ViewBag.link = Request.ServerVariables["HTTP_REFERER"];
            if (HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {             
            Guid? resetPassword = AccountService.CreateResetLink(model.Email);
            if (resetPassword != null)
            {
                // TODO można się pobawić w zrobienie metody renderującej cshtml do stringa i trzymać wtedy treść maila jako widok
                ViewBag.successSend = true;
                string link = Url.Action("ResetPassword", "Account", new {code = resetPassword}, "http");
                string resetLink = "<a href=\"" + link + "\">" + link + "</a>";
                MailService.Send(model.Email, "Przypomnienie hasła", 
                    "<h1>Przypomnienie hasła</h1><hr>" +
                    "<p>Otrzymujesz tę wiadomość, ponieważ ktos próbował zalogować się na konto, do którego przypisany jest ten adres e-mail, prosząc o przypomnienie hasła.</p>" +
                    "<p>Jeśli rzeczywiście wykonałeś/aś taką czynność i chcesz wyczyścić hasło, kliknij poniższy odnośnik do strony WWW. Jeśli nie możesz tego zrobic, skopiuj go i wklej do paska adresowego swojej przeglądarki.</p>" + 
                    resetLink +
                    "<p>Jesli masz jakieś pytania lub potrzebujesz pomocy odnośnie Twojego konta, odwiedź nasze centrum pomocy technicznej, które jeszcze nie istnieje, ale pomóc pomoże.</p>" +
                    "<b>Z poważaniem,</b><br><b>Zespół SIDUKM</b>");
                return View();
            }
            if (ModelState.IsValid)
            {
                ViewBag.errorMessage = "Podany mail nie istnieje w naszej bazie!";
            }
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(Guid? code)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            if (code == null)
            {
                return RedirectToAction("ForgotPassword");
            }
            int accountId = AccountService.GetAccountIdByGuid(code.Value);
            if (accountId == 0)
            {
                return RedirectToAction("ForgotPassword");
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordViewModel model,Guid code)
        {
            int accountId = AccountService.GetAccountIdByGuid(code);
            if (accountId != 0)
            {
                if (ModelState.IsValid)
                {
                    AccountService.ChangePassword(model.Password, accountId);
                    ViewBag.successSend = true;
                }
            }
            else
            {
                ModelState.AddModelError("unknown", "Wystąpił nieznany błąd!");
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult EditAccount (string name)
        {
            Account account;
            ViewBag.SuccessulChange = false;
            string refer = Request.ServerVariables["HTTP_REFERER"];
            account = AccountService.GetAccountByName(HttpContext.User.Identity.Name);
            var model = new EditAccountViewModel()
            {
                AccountId = account.AccountId,
                Email = account.Email,
                FirstName = account.FirstName,
                LastName = account.LastName,
                Gender = AccountService.GetGenders().ToList(),
                City = AccountService.GetCities().ToList(),
                GenderId= account.GenderId,
                CityId=account.CityId,
                PreviousPage = refer
            };           
            return View(model);
        }

        [HttpPost]
        public ActionResult EditAccount(EditAccountViewModel model, HttpPostedFileBase avatar)
        {
            ViewBag.OldPasswordError = null;
            ViewBag.EmailError = null;
            ViewBag.SuccessulChange = false;
            model.Gender = AccountService.GetGenders().ToList();
            model.City = AccountService.GetCities().ToList();
            Account newAccount = new Account();
            Account account = AccountService.GetAccountByName(HttpContext.User.Identity.Name);
            string oldmail = account.Email;
            bool changeData = false; // czy zmieniono dane (imię, nazwisko, płeć lub miasto)
            if (ModelState.IsValid)
            {
                ModelState.Clear();
                if (model.Email == null)
                {
                    model.Email = account.Email;
                    ViewBag.EmailError = "Email nie może być pusty";
                }
                //zmiana hasła
                if (model.Password != null || model.NewPassword != null || model.RepeatPassword != null)
                {
                    if (model.Password == null)
                    {
                        ViewBag.OldPasswordError = "Wprowadź stare hasło!";
                    }
                    if (model.Password != null && model.NewPassword != null)
                    {
                        bool changed = AccountService.EditPassword(HttpContext.User.Identity.Name, model.Password, model.NewPassword);
                        if (!changed)
                        {
                            ViewBag.OldPasswordError = "Hasło jest błędne!";
                        }
                        else
                        {
                            ViewBag.OldPasswordError = "Pomyślnie zmieniono hasło!";
                        }
                    }
                    if (model.NewPassword != null && model.Password == null)
                    {
                        ViewBag.OldPasswordError = "Aby zmienić hasło, wprowadź stare hasło!";
                    }
                    if (model.Password != null && model.NewPassword == null)
                    {
                        ViewBag.OldPasswordError = "Aby zmienić hasło, wprowadź nowe hasło!";
                    }              
                }
                
                
                AccountAvatar newAccountAvatar = null;
                if(avatar != null)
                {
                    /*newAccountAvatar = new AccountAvatar
                    {
                        AccountId = account.AccountId,
                        MimeType = avatar.ContentType,
                        Avatar = new byte [avatar.ContentLength]
                    };
                    avatar.InputStream.Read(newAccountAvatar.Avatar,0,avatar.ContentLength);*/
                    WebImage img = new WebImage(avatar.InputStream);
                    double ratio = ((double) img.Width)/img.Height;
                    if (img.Width > 200 || img.Height > 200)
                    {
                        img.Resize(200, (int)(200*ratio));
                    }
                    newAccountAvatar = new AccountAvatar
                    {
                        AccountId = account.AccountId,
                        MimeType = avatar.ContentType,
                        Avatar = img.GetBytes()
                    };
                } 
                // Sprawdzanie imię, nazwisko, płeć lub miasto zostało zmienione
                if (account.FirstName != model.FirstName || account.LastName != model.LastName || account.GenderId != model.GenderId || account.CityId != model.CityId)
                {
                    changeData = true;
                }
                newAccount.AccountId = account.AccountId;
                newAccount.AccountLogin = account.AccountLogin;
                newAccount.Email = model.Email;
                newAccount.CityId = (short)model.CityId;
                newAccount.GenderId = model.GenderId;
                newAccount.FirstName = model.FirstName;
                newAccount.LastName = model.LastName;
                newAccount.UserRoleId = (short) account.UserRoleId;
                if (!AccountService.EditAccount(newAccount, newAccountAvatar))
                {
                    ViewBag.EmailError = "Podany email istnieje już w bazie!";
                }
                else
                {
                    if (!oldmail.Equals(model.Email, StringComparison.OrdinalIgnoreCase))
                        ViewBag.EmailError = "Pomyślnie zaktualizowano email!";
                    if(changeData)
                        ViewBag.SuccessulChange = true;
                }
            }
            
            return View(model);
        }

        [AllowAnonymous]
        public FileResult GetAvatar(int id)
        {
            AccountAvatar accAvatar = AccountService.GetAccountAvatar(id);
            if(accAvatar != null)
                return File(accAvatar.Avatar, accAvatar.MimeType);
            else
                return File("~/Content/img/default_avatar_large.png" ,"image/png");
        }

        [AllowAnonymous]
        public FileResult GetAvatarByName(string name)
        {
            Account acc = AccountService.GetAccountByName(name);
            return GetAvatar(acc.AccountId);
        }

        public ActionResult DeleteAvatar(int id)
        {
            var result = AccountService.DeleteAvatar(id);
            return RedirectToAction("EditAccount");
        }

        public ActionResult DeleteAccount()
        {
            var result = AccountService.DeleteAccount(AccountService.GetAccountId(HttpContext.User.Identity.Name));
            return RedirectToAction("Logout");
        }

        [AllowAnonymous]
        public ActionResult UserProfile(int? id, string login)
        {
            var result = login != null ? AccountService.GetAccountByName(login) : AccountService.GetAccountById(id.Value);

            var model = new ProfileViewModel()
            {
                AccountId = result.AccountId,
                AccountLogin = result.AccountLogin,
                City = AccountService.GetCities().FirstOrDefault(x => x.CityId == result.CityId).CityName,
                FirstName = result.FirstName,
                Gender = AccountService.GetGenders().FirstOrDefault(x => x.GenderId == result.GenderId).GenderName,
                LastName = result.LastName,
                UserRole = AccountService.GetUserRoles().FirstOrDefault(x => x.UserRoleId == result.UserRoleId).RoleName
            };

            ViewBag.link = Request.ServerVariables["HTTP_REFERER"];

            return View(model);
        }
	}
}