﻿using System.Web.Mvc;
using SIDUKM.Domain;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;
using SIDUKM.WebUI.Models.Spotted;

namespace SIDUKM.WebUI.Controllers
{
    public class SpottedController : AbstractController
    {
        private ISpottedService SpottedService;
        private IBusService BusService;

        public SpottedController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService,
            ISpottedService spottedService, IBusService busService)
            : base(sessionManager, accountService, mailService)
        {
            SpottedService = spottedService;
            BusService = busService;
        }

        [AllowAnonymous]
        public ActionResult Index(int? topic)
        {
            if (topic.HasValue && topic != 0)
            {
                return new RedirectResult(Url.Action("Index") + "#topic" + topic);
            }
            ViewBag.Logged = HttpContext.User.Identity.IsAuthenticated;
            var user = AccountService.GetAccountByName(HttpContext.User.Identity.Name);
            var ntModel = new NewTopicViewModel()
            {
                Line = BusService.GetLines(user == null? 1 : user.CityId),
                Category = SpottedService.GetTopicCategories()
            };
            return View(ntModel);
        }

        [HttpPost]
        public ActionResult NewTopic(NewTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                int id = 0;
                // TODO wybór miasta
                var user = AccountService.GetAccountByName(HttpContext.User.Identity.Name);
                var topic = new Topic()
                {
                    CityId = user.CityId,
                    AccountId = user.AccountId,
                    IsDeleted = false,
                    IsAnonymous = model.IsAnonymous,
                    LineId = model.LineId,
                    Message = model.Message,
                    TopicCategoryId = model.CategoryId
                };
                id = SpottedService.AddTopic(topic);
                if (id == 0)
                {
                    ViewBag.Error = "Nie udało się dodać tematu!";
                }
                //return new RedirectResult(Url.Action("Index") + "#" + id);
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public JsonResult GetTopics(int lastId = 0, int categoryId = 0, int count = 10, int cityId = 1)
        {
            return Json(SpottedService.GetTopicsToDisplay(count, lastId, categoryId, cityId), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ContentResult AreNewTopics(int firstId = 0, int categoryId = 0, int cityId = 1)
        {
            return Content(SpottedService.AreNewTopics(firstId, categoryId, cityId).ToString());
        }

        [AllowAnonymous]
        public ContentResult GetNewTopicsCount(int firstId = 0, int categoryId = 0, int cityId = 1)
        {
            return Content(SpottedService.GetNewTopicsCount(firstId, categoryId, cityId).ToString());
        }

        [AllowAnonymous]
        public JsonResult GetPosts(int? postId)
        {
            return postId.HasValue ? Json(SpottedService.GetPostsToDisplay(postId.Value), JsonRequestBehavior.AllowGet) : 
                Json(null, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ContentResult CurrentUser()
        {
            return Content(AccountService.GetAccountId(HttpContext.User.Identity.Name).ToString());
        }

        [AllowAnonymous]
        public JsonResult GetModeratorsList()
        {
            return Json(AccountService.GetModeratorsId(), JsonRequestBehavior.AllowGet);
        }

        public ContentResult TopicVote(int value = 0, int topicId = 0)
        {
            int val = 0;
            switch (value)
            {
                case 1:
                    val = SpottedService.AddTopicVote(AccountService.GetAccountId(HttpContext.User.Identity.Name), topicId, true);
                    break;
                case -1:
                    val = SpottedService.AddTopicVote(AccountService.GetAccountId(HttpContext.User.Identity.Name), topicId, false);
                    break;
                default:
                    val = SpottedService.CountVotes(topicId, true, true);
                    break;
            }
            return Content(val.ToString());
        }

        public ContentResult PostVote(int value = 0, long postId = 0)
        {
            int val = 0;
            switch (value)
            {
                case 1:
                    val = SpottedService.AddPostVote(AccountService.GetAccountId(HttpContext.User.Identity.Name), postId, true);
                    break;
                case -1:
                    val = SpottedService.AddPostVote(AccountService.GetAccountId(HttpContext.User.Identity.Name), postId, false);
                    break;
                default:
                    val = SpottedService.CountVotes(postId, false, true);
                    break;
            }
            return Content(val.ToString());
        }

        public JsonResult GetTopicVotes(int id)
        {
            return Json(new { up = SpottedService.CountVotes(id, true, true), down = SpottedService.CountVotes(id, true, false) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPostVotes(long id)
        {
            return Json(new { up = SpottedService.CountVotes(id, false, true), down = SpottedService.CountVotes(id, false, false) }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddPost(NewPostViewModel model)
        {
            if (model != null)
            {
                long result = SpottedService.AddPost(new Post
                {
                    AccountId = AccountService.GetAccountId(HttpContext.User.Identity.Name),
                    IsDeleted = false,
                    Message = model.Content,
                    TopicId = model.TopicId
                });
                return Json(new { success = true, id = result });
            }
            else
            {
                return Json(new { success = false });
            }
        }

        [HttpPost]
        public void DeleteTopic(int id)
        {
            //var referer = Request.ServerVariables["HTTP_REFERER"];
            SpottedService.DeleteTopic(id);
            //return Redirect(referer);
            //return null;
        }

        [HttpPost]
        public void DeletePost(int id)
        {
            SpottedService.DeletePost(id);
        }

        [HttpGet]
        public ActionResult EditTopic(int? topic)
        {
            ViewBag.Link = Request.ServerVariables["HTTP_REFERER"];
            if(!topic.HasValue)
            {
                return RedirectToAction("Index", "Spotted");
            }
            Topic top = SpottedService.GetTopicById(topic.Value);
            NewTopicViewModel model = new NewTopicViewModel()
            {
                TopicId = top.TopicId,
                Message = top.Message
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult EditTopic(NewTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                Topic result = new Topic()
                {
                    TopicId = model.TopicId,
                    Message = model.Message
                };
                SpottedService.EditTopic(result);
            }
            return RedirectToAction("Index", "Spotted");
        }

        [HttpGet]
        public ActionResult EditPost(int? post)
        {
            ViewBag.Link = Request.ServerVariables["HTTP_REFERER"];
            if(!post.HasValue)
            {
                return RedirectToAction("Index", "Spotted");
            }
            Post p = SpottedService.GetPostById(post.Value);
            NewPostViewModel model = new NewPostViewModel()
            {
                TopicId = (int)p.PostId,
                Content = p.Message
            };
            return View(model);

        }

        [HttpPost]
        public ActionResult EditPost(NewPostViewModel model)
        {
            ViewBag.Link = Request.ServerVariables["HTTP_REFERER"];
            if(ModelState.IsValid)
            {
                Post result = new Post()
                {
                    PostId = model.TopicId,
                    Message = model.Content
                };
                SpottedService.EditPost(result);
            }
            return RedirectToAction("Index", "Spotted");
        }
    }
}