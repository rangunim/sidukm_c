﻿using System.Collections.Generic;
using System.Web.Mvc;
using SIDUKM.Domain;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;
using SIDUKM.WebUI.Models.Home;

namespace SIDUKM.WebUI.Controllers
{
    public class HomeController : AbstractController
    {

        IRssService RssService;
        //public HomeController() { }

        public HomeController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService, IRssService rssService) : 
            base(sessionManager, accountService, mailService) 
        {
            RssService = rssService;
        }

        // TODO Zastąpić poniższe wygenerowane przez Visuala własnymi, niepotrzebne skasować
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.MPKNews = RssService.GetNews(RssService.GetRssLink("admin"));
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Authorize(Roles="Administrator")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        // poniższe jest tylko dla testu komunikacji z bazą danych, można spokojnie skasować później
        // też jest do prostego pokazania jak działa tutaj zapytanie POST
        // jak coś, to jest to pod adresem: /Home/TestCity
        [AllowAnonymous]
        public ActionResult TestCity()
        {
            TestCityViewModel model = new TestCityViewModel
            {
                Cities = AccountService.GetCities() as List<City>
            };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult TestCity(TestCityViewModel model)
        {
            // lista miast z bazy nie przesyła się w formularzu, więc tracimy ją w modelu, trzeba pobrać jeszcze raz
            model.Cities = AccountService.GetCities() as List<City>;
            return View(model);
        }

        // prosty spis użytkowników
        // /Home/UserList
        [AllowAnonymous]
        public ActionResult UserList()
        {
            var model = AccountService.GetAccounts();
            return View(model);
        }
    }
}