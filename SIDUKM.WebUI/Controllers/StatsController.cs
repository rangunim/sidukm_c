﻿using System.Linq;
using System.Web.Mvc;
using SIDUKM.Logic;
using SIDUKM.WebUI.Infrastructure;

namespace SIDUKM.WebUI.Controllers
{
    public class StatsController : AbstractController
    {
        IStatService StatService { get; set; }

        public StatsController(ISessionManager sessionManager, IAccountService accountService, IMailService mailService,
            IStatService statService) : base(sessionManager, accountService, mailService)
        {
            StatService = statService;
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetRegistrationsPerDay(int acc = 0)
        {
            var result = acc == 0
                ? StatService.RegistrationsPerDay().OrderBy(x => x.Key)
                : StatService.StatAccumulated(StatService.RegistrationsPerDay)
                    .OrderBy(x => x.Key);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetTopicsPerDay(int acc = 0)
        {
            var result = acc == 0
                ? StatService.SpottedTopicsPerDay().OrderBy(x => x.Key)
                : StatService.StatAccumulated(StatService.SpottedTopicsPerDay)
                    .OrderBy(x => x.Key);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetPostsPerDay(int acc = 0)
        {
            var result = acc == 0
                ? StatService.SpottedPostsPerDay().OrderBy(x => x.Key)
                : StatService.StatAccumulated(StatService.SpottedPostsPerDay)
                    .OrderBy(x => x.Key);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetMostCommented()
        {
            return Json(StatService.MostCommentedTopic(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetTopicVotes(int type = 1)
        {
            return type == -1
                ? Json(StatService.LowestVotesTopic(), JsonRequestBehavior.AllowGet)
                : Json(StatService.HighestVotesTopic(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetPostVotes(int type = 1)
        {
            return type == -1
                ? Json(StatService.LowestVotesPost(), JsonRequestBehavior.AllowGet)
                : Json(StatService.HighestVotesPost(), JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator")]
        public JsonResult GetActivity(int posts = 0, int count = 10)
        {
            return posts == 1
                ? Json(StatService.SpottedActivityByPosts(count).OrderByDescending(x => x.Value),
                    JsonRequestBehavior.AllowGet)
                : Json(StatService.SpottedActivityByTopics(count).OrderByDescending(x => x.Value),
                    JsonRequestBehavior.AllowGet);
        }
    }
}