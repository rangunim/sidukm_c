USE baza_zpi

CREATE TABLE [Route]
(
	LineId INT REFERENCES Line,
	StopId INT REFERENCES [Stop],
	[Order] INT NOT NULL,
	CONSTRAINT pk_Route PRIMARY KEY(LineId, StopId)
);
GO

CREATE TABLE [Message] 
(
	MessageID INT PRIMARY KEY IDENTITY,
	Sender_AccountID INT REFERENCES Account(AccountID) NOT NULL,
	Recipient_AccountID INT REFERENCES Account(AccountID) NOT NULL,
	[Subject] NVARCHAR(100) NOT NULL,
	Content NVARCHAR(MAX) NOT NULL,
	DateCreated DATETIME NOT NULL,
	IsReaded BIT NOT NULL,
	IsAnonymousRecipient BIT NOT NULL,
	IsDeleted BIT NOT NULL
);
GO

EXEC sp_rename 'Message.IsDeleted', 'IsDeletedBySender', 'COLUMN';
GO

ALTER TABLE [Message]
ADD IsDeletedByRecipient BIT NOT NULL;
GO

CREATE TABLE Profanity (
	ProfanityId INT PRIMARY KEY IDENTITY,
	Word NVARCHAR(50) UNIQUE NOT NULL,
	AccountId INT REFERENCES Account
);
GO