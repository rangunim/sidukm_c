USE baza_zpi

CREATE TABLE AccountBan
(
	AccountId INT PRIMARY KEY REFERENCES Account,
	StartDate DATETIME NOT NULL,
	EndDate DATETIME NOT NULL,
	CHECK(StartDate < EndDate)
);
GO

ALTER TABLE [Message]
ADD DateDeletedBySender DATETIME,
DateDeletedByRecipient DATETIME;
GO

UPDATE [dbo].[Message]
   SET [DateDeletedBySender] = GETDATE()
 WHERE [IsDeletedBySender] = 1
GO

UPDATE [dbo].[Message]
   SET [DateDeletedByRecipient] = GETDATE()
 WHERE [IsDeletedByRecipient] = 1
GO