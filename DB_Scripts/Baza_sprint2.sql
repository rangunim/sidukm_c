USE baza_zpi

CREATE TABLE PasswordReset (
	AccountId INT PRIMARY KEY REFERENCES Account,
	Link UNIQUEIDENTIFIER NOT NULL
);

CREATE TABLE AccountAvatar (
	AccountId INT PRIMARY KEY REFERENCES Account,
	Avatar VARBINARY(max) NOT NULL,
	MimeType VARCHAR(50) NOT NULL
);

ALTER TABLE City
ADD RssLink NVARCHAR(MAX);

UPDATE City
SET RssLink = 'http://pasazer.mpk.wroc.pl/rss-aktualnosci-i-zmiany.xml'
WHERE CityId = 1;