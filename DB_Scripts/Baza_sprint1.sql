USE baza_zpi

CREATE TABLE City (
	CityId SMALLINT CONSTRAINT pk_City PRIMARY KEY IDENTITY,
	CityName NVARCHAR(20) CONSTRAINT notnull_City_CityName NOT NULL 
);
INSERT INTO City (CityName) VALUES (
	'Wroc�aw'
);
GO

CREATE TABLE UserRole (
	UserRoleId SMALLINT CONSTRAINT pk_Role PRIMARY KEY,
	RoleName NVARCHAR(30) CONSTRAINT notnull_User_Role_TypeRole NOT NULL
);
INSERT INTO UserRole
VALUES (1, 'Administrator'), (2, 'Moderator'), (3, 'U�ytkownik');
GO

CREATE TABLE Gender (
	GenderId SMALLINT CONSTRAINT pk_Gender PRIMARY KEY,
	GenderName NVARCHAR(10) CONSTRAINT notnull_Gender_GemderName NOT NULL 
);
INSERT INTO Gender
VALUES (0, 'Wybierz...'), (1, 'Kobieta'), (2, 'M�czyzna');
GO

CREATE TABLE Account (
	AccountId INT CONSTRAINT pk_Account PRIMARY KEY IDENTITY,
	AccountLogin NVARCHAR(30) CONSTRAINT notnull_Account_AccountLogin NOT NULL CONSTRAINT unique_Account_Login UNIQUE,
	Email NVARCHAR(30) CONSTRAINT notnull_Account_Email NOT NULL CONSTRAINT unique_Account_Email UNIQUE,
	AccountPassword CHAR(32) CONSTRAINT notnull_Account_AccountPassword NOT NULL,
	FirstName NVARCHAR(20),
	LastName NVARCHAR(30),
	CityId SMALLINT NOT NULL REFERENCES City,
	GenderId SMALLINT REFERENCES Gender,
	UserRoleId SMALLINT REFERENCES UserRole
);
INSERT INTO [dbo].[Account]
           ([AccountLogin]
           ,[Email]
           ,[AccountPassword]
           ,[FirstName]
           ,[LastName]
           ,[CityId]
           ,[GenderId]
           ,[UserRoleId])
     VALUES
           ('Admin'
           ,'203226@student.pwr.edu.pl'
           ,'21232f297a57a5a743894a0e4a801fc3'
           ,NULL
           ,NULL
           ,1
           ,1
           ,1);
GO