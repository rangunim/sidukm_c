USE baza_zpi

ALTER TABLE Timetable
	ADD [Hash] CHAR(32),
	LastUpdated DATETIME;
GO

CREATE TABLE TopicCategory (
	TopicCategoryId INT CONSTRAINT pk_TopicCategory PRIMARY KEY IDENTITY,
	Name NVARCHAR(50) NOT NULL,
);
GO

INSERT INTO TopicCategory(Name)
VALUES ('Towarzyskie'), ('Utrudnienia i awarie'), ('Opinie o komunikacji'), ('Uwagi dotycz�ce serwisu'), ('Inne');
GO

CREATE TABLE Topic (
	TopicId INT CONSTRAINT pk_Topic PRIMARY KEY IDENTITY,
	[Message] NVARCHAR(max) NOT NULL,
	IsAnonymous BIT NOT NULL,
	IsDeleted BIT NOT NULL,
	DateCreated DATETIME NOT NULL,
	LastUpdated DATETIME,
	AccountId INT NOT NULL REFERENCES Account,
	TopicCategoryId INT NOT NULL REFERENCES TopicCategory,
	LineId INT REFERENCES Line,
	CityId SMALLINT NOT NULL REFERENCES City
);
GO

CREATE TABLE Post (
	PostId BIGINT CONSTRAINT pk_Post PRIMARY KEY IDENTITY,
	[Message] NVARCHAR(max) NOT NULL,
	DateCreated DATETIME NOT NULL,
	LastUpdated DATETIME,
	IsDeleted BIT NOT NULL,
	AccountId INT NOT NULL REFERENCES Account,
	TopicId INT NOT NULL REFERENCES Topic
);
GO

CREATE TABLE TopicVote (
	TopicId INT NOT NULL REFERENCES Topic,
	AccountId INT NOT NULL REFERENCES Account,
	VoteValue SMALLINT NOT NULL,
	CONSTRAINT pk_TopicVote PRIMARY KEY(TopicId, AccountId)		
);
GO

CREATE TABLE PostVote (
	PostId BIGINT NOT NULL REFERENCES Post,
	AccountId INT NOT NULL REFERENCES Account,
	VoteValue SMALLINT NOT NULL,
	CONSTRAINT pk_PostVote PRIMARY KEY(PostId, AccountId)		
);
GO